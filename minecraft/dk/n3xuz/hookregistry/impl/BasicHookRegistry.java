package dk.n3xuz.hookregistry.impl;

import dk.n3xuz.hookregistry.HookRegistry;
import dk.n3xuz.hookregistry.context.Context;
import dk.n3xuz.hookregistry.filters.Filter;
import dk.n3xuz.hookregistry.Hook;
import dk.n3xuz.hookregistry.utils.ArrayResizer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;
import java.util.function.Predicate;

public class BasicHookRegistry implements HookRegistry {
    protected final HashMap<Class<? extends Context>, Hook[]> registry = new HashMap<>();

    @Override
    public boolean register(Hook<?>... hooks) {
        Objects.requireNonNull(hooks);
        boolean added = false;
        for (Hook<?> hook : hooks) {
            hooks = registry.get(hook.getContextType());
            if (hooks == null)
                added = registry.put(hook.getContextType(), new Hook<?>[] {hook}) == null;
            else if (!Arrays.asList(hooks).contains(hook)) {
                int insert = 0;
                do {
                    if (hook.getPriority().ordinal() < hooks[insert].getPriority().ordinal())
                        break;
                    insert++;
                } while (insert < hooks.length);
                registry.put(hook.getContextType(), ArrayResizer.insert(hooks, insert, hook));
                added = true;
            }
        }

        return added;
    }

    public boolean unregister(Predicate<Hook<?>> filter) {
        boolean removed = false;
        for (Hook<?>[] hooks : registry.values())
            for (Hook<?> hook : hooks)
                if (filter.test(hook))
                    removed |= unregister(hook);
        return removed;
    }

    @Override
    public boolean unregister(Hook<?>... hooks) {
        Objects.requireNonNull(hooks);
        boolean removed = false;
        for (Hook<?> hook : hooks) {
            hooks = registry.get(hook.getContextType());
            if (hooks != null) {
                int index = Arrays.asList(hooks).indexOf(hook);
                if (index != -1) {
                    removed = true;
                    if (hooks.length == 1)
                        registry.remove(hook.getContextType());
                    else
                        registry.put(hook.getContextType(), ArrayResizer.remove(hooks, index));
                }
            }
        }

        return removed;
    }

    @Override
    public <T extends Context> T dispatch(T context) {
        @SuppressWarnings("unchecked")
        Hook<T>[] hooks = registry.get(context.getClass());
        if (hooks != null)
            for (Hook<T> hook : hooks)
                if (filter(hook, context))
                    if (hook.getPriority().monitor) {
                        context.lock();
                        hook.call(context);
                        context.unlock();
                    } else
                        hook.call(context);
        return context;
    }

    protected final <T extends Context> boolean filter(Hook<T> hook, T context) {
        Filter<T>[] filters = hook.getFilters();
        if (filters != null)
            for (Filter<T> filter : filters)
                if (!filter.test(hook, context))
                    return false;
        return true;
    }
}
