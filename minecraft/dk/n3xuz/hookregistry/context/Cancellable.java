package dk.n3xuz.hookregistry.context;

public interface Cancellable {
    void setCancelled(boolean flag);

    boolean isCancelled();

    void cancel();
}
