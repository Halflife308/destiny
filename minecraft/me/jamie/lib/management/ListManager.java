package me.jamie.lib.management;

import java.util.List;

public abstract class ListManager<T> 
{
	private List<T> contents;
	
	public abstract void load();
	
	public abstract void register(T t);
	
	public List<T> getContents() {
		return contents;
	}
	
	public void setContents(List<T> contents) {
		this.contents = contents;
	}
}
