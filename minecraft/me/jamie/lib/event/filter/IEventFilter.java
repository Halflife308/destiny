package me.jamie.lib.event.filter;

import me.jamie.lib.event.listen.ListenerContainer;

/**
 * @author Ddong
 * @since Mar 8, 2015
 */
public interface IEventFilter<T> {

    boolean shouldSend(final T hook, final ListenerContainer listener);
}
