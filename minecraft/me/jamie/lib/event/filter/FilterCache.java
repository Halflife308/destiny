package me.jamie.lib.event.filter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sc4re
 */
public class FilterCache {
    @SuppressWarnings("rawtypes")
    protected final Map<Class<? extends IEventFilter>, IEventFilter<?>> filters = new HashMap<>();

    public IEventFilter<?>[] getFilters() {
	return filters.values().toArray(new IEventFilter[filters.size()]);
    }

    @SuppressWarnings("rawtypes")
    public <T extends IEventFilter> void addFilter(final T filter) {
	filters.put(filter.getClass().asSubclass(IEventFilter.class), filter);
    }

    @SuppressWarnings("unchecked")
    public <T extends IEventFilter<?>> T getFilter(final Class<T> filterClass) {
	return (T) filters.get(filterClass);
    }
}
