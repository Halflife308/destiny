package me.jamie.lib.event.filter.filters;

import me.jamie.destiny.impl.events.packet.PacketSendEvent;
import me.jamie.lib.event.filter.IEventFilter;
import me.jamie.lib.event.listen.ListenerContainer;
import net.minecraft.network.Packet;

public class PacketFilter implements IEventFilter<PacketSendEvent>
{
public boolean shouldSend(PacketSendEvent event, ListenerContainer container)
{
  AcceptedPackets packets = (AcceptedPackets)container.getMethod().getAnnotation(AcceptedPackets.class);
  if (packets == null) {
    return true;
  }
  Class[] accepted = packets.packets();
  if (accepted == null) {
    return true;
  }
  if (accepted.length == 0) {
    return true;
  }
  Packet packet = event.getPacket();
  Class[] arrayOfClass1;
  int j = (arrayOfClass1 = accepted).length;
  for (int i = 0; i < j; i++)
  {
    Class<? extends Packet> packetClass = arrayOfClass1[i];
    if ((packet.getClass().isInstance(packetClass)) || (packet.getClass().isAssignableFrom(packetClass)) || (packet.getClass().equals(packetClass)) || (packet.getClass().getSuperclass().equals(packetClass))) {
      return true;
    }
  }
  return false;
}
}
