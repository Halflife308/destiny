package me.jamie.lib.event.listen;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import me.jamie.lib.event.filter.Filter;

/**
 * @author Ddong
 * @since Mar 8, 2015
 */
public class ListenerContainer {
    protected final Object object;
    protected final Method method;
    protected final Handler handler;
    protected final int priority;
    protected final Filter[] filters;
    protected final Map<Class<? extends Annotation>, Annotation> annotations = new HashMap<>();

    public ListenerContainer(final Object object, final Method method) {
	this.object = object;
	this.method = method;
	this.handler = method.getAnnotation(Handler.class);
	this.filters = handler.filters();
	this.priority = handler.priority();
	Arrays.asList(method.getDeclaredAnnotations()).forEach(annotation -> {
	    if (!(annotation instanceof Handler)) {
		this.annotations.put(annotation.getClass(), annotation);
	    }
	});
    }

    public Object getObject() {
	return object;
    }

    public Method getMethod() {
	return method;
    }

    public Handler getHandlerAnnotation() {
	return handler;
    }

    public Filter[] getFilters() {
	return filters;
    }

    @SuppressWarnings("unchecked")
    public <T extends Annotation> T getAnnotation(final Class<T> annotation) {
	return (T) this.annotations.get(annotation);
    }

    @Override
    public int hashCode() {
	return getObject().hashCode() ^ getMethod().hashCode();
    }
}
