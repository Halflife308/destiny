package me.jamie.lib.event.listen;

/**
 * @author Ddong
 * @since Mar 10, 2015
 */
@FunctionalInterface
public interface DirectListener<T> {

    public void call(final T event);
}
