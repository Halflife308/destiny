package me.jamie.lib.event;

/**
 * @author Ddong
 * @since 1/17/2015
 */
public class Event {
    private boolean cancelled;

    public boolean isCancelled() {
	return cancelled;
    }

    public void setCancelled(final boolean cancelled) {
	this.cancelled = cancelled;
    }
}
