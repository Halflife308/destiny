package me.jamie.lib.event;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import me.jamie.lib.event.dispatch.EventSender;
import me.jamie.lib.event.listen.DirectListener;
import me.jamie.lib.event.listen.Handler;
import me.jamie.lib.event.listen.ListenerContainer;

/**
 * @author Ddong
 * @since Mar 8, 2015
 */
public class EventManager {
    protected final Map<Class<? extends Event>, EventSender> senders = new ConcurrentHashMap<>();

    public synchronized void register(final Object object) {
	if (object instanceof DirectListener) {
	    
	}
	
	for (final Method method : object.getClass().getDeclaredMethods()) {
	    method.setAccessible(true);
	    final Handler annotation = method.getAnnotation(Handler.class);
	    if (annotation == null) {
		continue;
	    }

	    if (method.getParameterTypes().length != 1) {
		continue;
	    }

	    if (!Event.class.isAssignableFrom(method.getParameterTypes()[0])) {
		continue;
	    }

	    final ListenerContainer listenerContainer = new ListenerContainer(object, method);
	    final Class<? extends Event> eventClass = method.getParameterTypes()[0].asSubclass(Event.class);
	    if (this.senders.get(eventClass) == null) {
		final EventSender eventSender = new EventSender();
		eventSender.addListener(listenerContainer);
		this.senders.put(eventClass, eventSender);
	    } else {
		this.senders.get(eventClass).addListener(listenerContainer);
	    }
	}
    }

    public synchronized void unregister(final Object object) {
	for (final Method method : object.getClass().getDeclaredMethods()) {
	    method.setAccessible(true);
	    final Handler annotation = method.getAnnotation(Handler.class);
	    if (annotation == null) {
		continue;
	    }

	    if (method.getParameterTypes().length != 1) {
		continue;
	    }

	    if (!Event.class.isAssignableFrom(method.getParameterTypes()[0])) {
		continue;
	    }

	    final Class<? extends Event> eventClass = method.getParameterTypes()[0].asSubclass(Event.class);
	    if (this.senders.get(eventClass) != null) {
		this.senders.get(eventClass).removeListener(object);
	    }
	}
    }

    public void fire(final Event event) {
	final Class<? extends Event> eventClass = event.getClass().asSubclass(Event.class);
	if (this.senders.get(eventClass) == null) {
	    return;
	}

	this.senders.get(eventClass).send(event);
    }
}
