package me.jamie.lib.event.dispatch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;

import me.jamie.lib.event.filter.Filter;
import me.jamie.lib.event.filter.FilterCache;
import me.jamie.lib.event.filter.IEventFilter;
import me.jamie.lib.event.listen.ListenerContainer;

/**
 * @author Ddong
 * @since Mar 8, 2015
 */
public class EventSender {
    protected final List<ListenerContainer> listeners = new ArrayList<>();
    protected final FilterCache filterCache = new FilterCache();

    public synchronized void addListener(final ListenerContainer listener) {
	this.listeners.add(listener);
	Collections.sort(this.listeners, new ListenerComparator());
    }

    public synchronized void removeListener(final Object object) {
	Iterator<ListenerContainer> iterator = this.listeners.iterator();
	final int size = this.listeners.size();
	while (iterator.hasNext()) {
	    if (iterator.next().getObject().equals(object)) {
		iterator.remove();
	    }
	}

	if (this.listeners.size() != size) {
	    Collections.sort(this.listeners, new ListenerComparator());
	}
    }

    public FilterCache getFilterCache() {
	return filterCache;
    }

    @SuppressWarnings("unchecked")
    public synchronized <T> void send(final T event) {
	if (this.listeners.size() == 0) {
	    return;
	}

	try {
		for (final ListenerContainer listener : this.listeners) {
		    final Filter[] filters = listener.getFilters();
		    boolean failed = false;
		    for (final Filter filter : filters) {
			IEventFilter<T> eventFilter = this.filterCache.getFilter(filter.type());
			if (eventFilter == null) {
			    try {
				eventFilter = filter.type().newInstance();
			    } catch (Exception e) {
				e.printStackTrace();
				failed = true;
				break;
			    }
	
			    this.filterCache.addFilter(eventFilter);
			}
	
			if (!eventFilter.shouldSend(event, listener)) {
			    failed = true;
			    break;
			}
		    }
	
		    if (failed) {
			continue;
		    }
	
		    try {
			listener.getMethod().invoke(listener.getObject(), event);
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		}
	} catch (ConcurrentModificationException e) {
		e.printStackTrace();
	}
    }
}
