package me.jamie.lib.event.dispatch;

import java.util.Comparator;

import me.jamie.lib.event.listen.ListenerContainer;

/**
 * @author Ddong
 * @since Mar 8, 2015
 */
public class ListenerComparator implements Comparator<ListenerContainer> {

    @Override
    public int compare(ListenerContainer firstListener, ListenerContainer secondListener) {
	return firstListener.getHandlerAnnotation().priority() - secondListener.getHandlerAnnotation().priority();
    }
}
