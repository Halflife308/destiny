package me.jamie.lib.interfaces;

@FunctionalInterface
public abstract interface Labelled {
	abstract String getLabel();
}
