package me.jamie.lib.interfaces;

@FunctionalInterface
public abstract interface Toggleable {
	abstract void toggle();
}
