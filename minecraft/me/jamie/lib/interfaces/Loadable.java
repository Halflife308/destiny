package me.jamie.lib.interfaces;

import java.io.IOException;

@FunctionalInterface
public abstract interface Loadable {
	abstract void load() throws IOException;
}
