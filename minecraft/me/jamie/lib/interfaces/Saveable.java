package me.jamie.lib.interfaces;

import java.io.IOException;

@FunctionalInterface
public abstract interface Saveable {
	abstract void save() throws IOException;
}

