package me.jamie.destiny.impl.update;

import me.jamie.lib.interfaces.Labelled;

public class Update implements Labelled
{
	private String name = "Version", info;
	private double version;
	
	public Update(double version, String info) {
		this.version = version;
		this.info = info;
	}
	
	@Override
	public String getLabel() {
		return name + version;
	}
	
	public String getInfo() {
		return info;
	}
	
	public void setInfo(String info) {
		this.info = info;
	}
}
