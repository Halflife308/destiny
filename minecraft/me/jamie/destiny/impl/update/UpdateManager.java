package me.jamie.destiny.impl.update;

import java.util.LinkedList;
import java.util.Set;

import org.reflections.Reflections;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.update.Update;
import me.jamie.lib.management.ListManager;

public class UpdateManager extends ListManager<Update> 
{

	@Override
	public void load() {
		if(getContents() == null)
			setContents(new LinkedList<Update>());
		register(new Update(1.1, "Initial commit, bug fixes."));
	}

	@Override
	public void register(Update u) {
		if(getContents().contains(u)) return;
		getContents().add(u);
	}
	
}
