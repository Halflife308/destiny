package me.jamie.destiny.impl.font.manager;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.events.font.StringWidthEvent;
import me.jamie.destiny.util.font.MinecraftFontRenderer;
import me.jamie.lib.event.listen.Handler;
import me.jamie.lib.management.ListManager;

public class FontManager extends ListManager<MinecraftFontRenderer>
{	
	/**
	 * Add GUI screen to change the TTF.
	 */
	private MinecraftFontRenderer ttf;
	private boolean global;
	
	public MinecraftFontRenderer getTTF() {
		return ttf;
	}
	
	public void setTTF(MinecraftFontRenderer ttf) {
		this.ttf = ttf;
	}
	
	public boolean isGlobalTTF() {
		return global;
	}
	
	public void setGlobalTTF(boolean global) {
		this.global = global;
	}

	@Override
	public void load() {
		Destiny.getInstance().getEventManager().register(this);
	}
	
	@Handler
	protected void onStringWidth(final StringWidthEvent event) {
		if(Destiny.getInstance().getFontManager().isGlobalTTF()) 
    		event.setCancelled(true);
	}
	
	@Override
	public void register(MinecraftFontRenderer font) {}
}
