package me.jamie.destiny.impl.alt;

import me.jamie.lib.interfaces.Labelled;

public class Alt implements Labelled
{
	private String name, password;
	
	public Alt(String name, String password) {
		this.name = name;
		this.password = password;
	}
	
	public Alt(String name) {
		this.name = name;
	}
	
	@Override
	public String getLabel() {
		return this.name;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	public void setLabel(String name) {
		this.name = name;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
}
