package me.jamie.destiny.impl.alt.manager;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Set;

import org.reflections.Reflections;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.alt.Alt;
import me.jamie.lib.management.ListManager;

public class AltManager extends ListManager<Alt> 
{

	@Override
	public void load() {
		if(getContents() == null)
			setContents(new LinkedList<Alt>());
		Destiny.getInstance().getEventManager().register(this);
		final Reflections reflect = new Reflections(Alt.class);
		final Set<Class<? extends Alt>> alts = reflect.getSubTypesOf(Alt.class);
		for (final Class clazz : alts) {
			try {
				final Alt alt = (Alt) clazz.newInstance();
				register(alt);
			} catch (final InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void register(Alt a) {
		if(getContents().contains(a)) return;
		getContents().add(a);
	}
	
}
