package me.jamie.destiny.impl.module.manager;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Set;

import org.reflections.Reflections;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.command.Command;
import me.jamie.destiny.impl.events.key.KeyToggleEvent;
import me.jamie.destiny.impl.module.Module;
import me.jamie.lib.event.listen.Handler;
import me.jamie.lib.management.ListManager;

public final class ModuleManager extends ListManager<Module>
{

	@Override
	public void load() {
		if(getContents() == null)
			setContents(new LinkedList<Module>());
		Destiny.getInstance().getEventManager().register(this);
		final Reflections reflect = new Reflections(Module.class);
		final Set<Class<? extends Module>> mods = reflect.getSubTypesOf(Module.class);
		for (final Class clazz : mods) {
			try {
				final Module module = (Module) clazz.newInstance();
				register(module);
			} catch (final InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		
		Collections.sort(Destiny.getInstance().getModuleManager().getContents(), new Comparator<Module>() {
	        @Override
	        public int compare(Module m1, Module m2) {
	            return m1.getLabel().compareToIgnoreCase(m2.getLabel());
	        }
	    });
	}

	@Handler
	protected void onKeyToggle(final KeyToggleEvent event) {
		getContents().stream().filter(m -> m.getBind() == event.getKey()).forEach(m -> m.toggle());
	}
	
	@Override
	public void register(Module m) {
		if(getContents().contains(m)) return;
		getContents().add(m);
	}
	
	public Module getModuleByName(String name) {
		for(Module m : getContents()) {
			if(m == null || m.getLabel() != name) 
				continue;
			return m;
		}
		return null;
	}
}
