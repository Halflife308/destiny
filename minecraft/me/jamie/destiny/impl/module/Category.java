package me.jamie.destiny.impl.module;

public enum Category 
{
	Movement("Movement"),
	Combat("Combat"),
	Player("Player"),
	Render("Render"),
	Exploits("Exploits"),
	World("World");
	
	private String label;
	
	private Category(String label) {
		this.label = label;
	}
}
