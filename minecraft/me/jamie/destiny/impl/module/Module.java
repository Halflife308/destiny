package me.jamie.destiny.impl.module;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.property.Property;
import me.jamie.lib.interfaces.Labelled;
import me.jamie.lib.interfaces.Toggleable;

public abstract class Module implements Labelled, Toggleable
{
	private String label, extension;
	private String[] aliases;
	
	private int bind, color;
	private boolean enabled, visible;
	
	private Category category;
	protected List<Property> properties;
	
	public Module(String label, String[] aliases) {
		this.label = label;
		this.aliases = aliases;
		this.properties = new LinkedList<>();
		loadProperties();
	}
	
	public Module(String label) {
		this.label = label;
		this.properties = new LinkedList<>();
		loadProperties();
	}
	
	@Override
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getExtension() {
		return extension;
	}
	
	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	public String[] getAliases() {
		return aliases;
	}

	public void setAliases(String[] aliases) {
		this.aliases = aliases;
	}

	public int getBind() {
		return bind;
	}

	public void setBind(int bind) {
		this.bind = bind;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
		if(this.enabled)
			Destiny.getInstance().getEventManager().register(this);
		else
			Destiny.getInstance().getEventManager().unregister(this);
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
	public Category getCategory() {
		return category;
	}
	
	public void setCategory(Category category) {
		this.category = category;
	}
	
	public List<Property> getProperties() {
		return properties;
	}
	
	@Override
	public void toggle() {
		this.enabled = !this.enabled;
		
		if(isEnabled()) 
			onEnable();
		else
			onDisable();
	}
	
	public void onEnable() {
		Destiny.getInstance().getEventManager().register(this);
	}
	
	public void onDisable() {
		Destiny.getInstance().getEventManager().unregister(this);
	}
	
	public void loadProperties() {
		Collections.sort(this.getProperties(), new Comparator<Property>() {
	        @Override
	        public int compare(Property p1, Property p2) {
	            return p1.getLabel().compareToIgnoreCase(p2.getLabel());
	        }
	    });
	}
}


