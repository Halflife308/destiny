package me.jamie.destiny.impl.module.modules.render;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import me.jamie.destiny.impl.events.render.Render3DEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.EntitySlime;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayer;

public class Tracers extends Module
{
	private Property<String> mode, color;
	private Property<Boolean> animals, mobs, players, items;
	
	public Tracers() {
		super("Tracers");
	
		setBind(Keyboard.KEY_BACK);
		setCategory(Category.Render);
	}
	
	@Override
	public void loadProperties() 
	{
		this.properties.add(mode = new Property<String>("Tracers Mode", "Mode", "Line", new String[] { "mode" }) {
			public void addProperties() {
				getProperties().add("Line");
				getProperties().add("Stem");
			}
		});
		
		this.properties.add(color = new Property<String>("Tracers Color", "Color", "Health", new String[] { "color" }) {
			public void addProperties() {
				getProperties().add("Health");
				getProperties().add("Distance");
			}
		});
		
		this.properties.add(animals = new Property<Boolean>("Tracers Animals", "Animals", true, new String[] { "animals" }));
		this.properties.add(mobs = new Property<Boolean>("Tracers Mobs", "Mobs", true, new String[] { "mobs" }));	
		this.properties.add(players = new Property<Boolean>("Tracers Players", "Players", true, new String[] { "players" }));
		this.properties.add(items = new Property<Boolean>("Tracers Items", "Items", true, new String[] { "items" }));
	}
	
	@Handler
	protected void onRender3D(final Render3DEvent event) 
	{
		for(Object o : Wrapper.getWorld().loadedEntityList) 
		{
			if(o == null || (!(o instanceof Entity))) continue;
			final Entity entity = (Entity)o;
			
			if(entity.equals(Wrapper.getPlayer())) continue;
			if(!canRender(entity)) continue;
			
			double x = interpolate(entity.lastTickPosX, entity.posX) - Wrapper.getMinecraft().getRenderManager().renderPosX;
			double y = interpolate(entity.lastTickPosY, entity.posY) - Wrapper.getMinecraft().getRenderManager().renderPosY;
			double z = interpolate(entity.lastTickPosZ, entity.posZ) - Wrapper.getMinecraft().getRenderManager().renderPosZ;	
			float[] colors;
			
			if(entity instanceof EntityLivingBase)
				colors = getColor((EntityLivingBase)entity);
			else 
				colors = new float[] { 0.0F, 1.0F, 0.0F };
			
			GL11.glPushMatrix();
			GlStateManager.disableLighting();
			GlStateManager.enableBlend();
			GlStateManager.blendFunc(770, 771);
			GlStateManager.disableDepth();
			GlStateManager.depthMask(false);
			GlStateManager.disableTexture();
			GlStateManager.pushMatrix();
			GlStateManager.loadIdentity();
			
			GL11.glLineWidth(1.7F);
	        GL11.glEnable(GL11.GL_LINE_SMOOTH);
			
			boolean bobbing = Wrapper.getGameSettings().viewBobbing;
	        Wrapper.getGameSettings().viewBobbing = false;
	        Wrapper.getMinecraft().entityRenderer.orientCamera(event.getPartialTicks());
	        
	        GL11.glBegin(GL11.GL_LINES);
	        GlStateManager.color(colors[0], colors[1], colors[2], 0.5F);
            
	        switch(mode.getCurrentProperty()) 
	        {
	        	case "Line": {
	        		 GL11.glVertex3d(0, Wrapper.getPlayer().getEyeHeight(), 0);
	                 GL11.glVertex3d(x, y, z);
	                 break;
	        	}
	        	case "Stem": {
	        		GL11.glVertex3d(x, y, z);
                    GL11.glVertex3d(x, y + entity.height, z);
                    break;
	        	}
	        }
	        
	        GL11.glVertex3d(0, Wrapper.getPlayer().getEyeHeight(), 0);
            GL11.glVertex3d(x, y, z);
            GL11.glEnd();
            
            GL11.glTranslated(x, y, z);
            GL11.glTranslated(-x, -y, -z);
            
            Wrapper.getGameSettings().viewBobbing = bobbing;
            
            GlStateManager.popMatrix();
            
            GL11.glDisable(2848);
			GlStateManager.enableTexture();
			GlStateManager.depthMask(true);
			GlStateManager.enableDepth();
			GlStateManager.disableBlend();
			GlStateManager.enableLighting();
	        GL11.glPopMatrix();
		}
	}
	
	private double interpolate(double prev, double cur) 
	{
		return prev + (cur - prev);
	}
	
	private boolean canRender(final Entity entity) 
	{
		if(entity instanceof EntityPlayer && players.getProperty())
			return true;
		if(entity instanceof EntityAnimal && animals.getProperty())
			return true;
		if((entity instanceof EntityMob || entity instanceof EntitySlime) && mobs.getProperty())
			return true;
		if(entity instanceof EntityItem && items.getProperty())
			return true;
		return false;
	}
	
	private float[] getColor(final EntityLivingBase entity) 
	{
		switch(color.getCurrentProperty()) 
		{
			case "Health": {
				float health = entity.getHealth() / 2;
				
				if(health > 8) 
					return new float[] { 0.0F, 1.0F, 0.0F };
				else if(health > 6)
					return new float[] { 0.5F, 0.15F, 0.0F };
				else if(health > 4) 
					return new float[] { 0.7F, 0.15F, 0.0F };
				else if(health > 2)
					return new float[] { 0.95F, 0.0F, 0.0F };
				else if(health >= 0)
					return new float[] { 1.0F, 0.0F, 0.0F };
			
				break;
			}
			case "Distance": {
				double distance = Wrapper.getPlayer().getDistanceToEntity(entity);
				
				if(distance > 28)
					return new float[] { 0.0F, 0.9F, 0.0F };
				else if(distance > 10)
					return new float[] { 0.5F, 0.15F, 0.0F };
				else if(distance > 6)
					return new float[] { 0.7F, 0.15F, 0.0F };
				else if(distance > 3)
					return new float[] { 0.95F, 0.0F, 0.0F };
				else if(distance >= 0)
					return new float[] { 1.0F, 0.0F, 0.0F };
				else if(distance >= 28)
					return new float[] { 0.0F, 1.0F, 0.0F };
				break;
			}
		}
		return new float[] { 0.0F, 1.0F, 0.0F };
	}
}
