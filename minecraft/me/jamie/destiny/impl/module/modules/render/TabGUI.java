package me.jamie.destiny.impl.module.modules.render;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.events.key.KeyToggleEvent;
import me.jamie.destiny.impl.events.render.Render2DEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.destiny.util.gui.Draw;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.client.gui.Gui;

public class TabGUI extends Module
{	
	private Property<String> mode;
	
	private List<Category> categories;
	private List<Module> modules;
	private int selected, selectedMod = 0;
	private boolean inside;
	
	public TabGUI() 
	{
		super("Tab GUI", new String[] { "tabgui", "tabui" } );	
		
		setCategory(Category.Render);
		
		categories = new ArrayList<>();
		modules = new ArrayList<>();
	}
	
	@Override
	public void loadProperties() {
		this.properties.add(mode = new Property<String>("TabGUI Color", "Color", "Rainbow", new String[] { "Color" }) {
			public void addProperties() {
				getProperties().add("Rainbow");
				getProperties().add("Red");
				getProperties().add("Orange");
				getProperties().add("Green");
				getProperties().add("Cyan");
				getProperties().add("Blue");
				getProperties().add("Purple");
				getProperties().add("Grey");
			}
		});
	}
	
	@Override
	public void onEnable() {
		super.onEnable();
	}
	
	@Handler
	protected void onRender2D(final Render2DEvent event) 
	{
		if(categories.isEmpty())
			addCategories();
		
		for(Category category : Category.values()) {
			for(Module m : Destiny.getInstance().getModuleManager().getContents()) {
				if(m.getCategory().equals(category) && categories.indexOf(category) == selected && !modules.contains(m)) 
					modules.add(m);
				else if(modules.contains(m) && categories.indexOf(category) != selected && m.getCategory().equals(category))
					modules.remove(m);
				else if(modules.contains(m) && m.getLabel().equalsIgnoreCase("ClickGui"))
					modules.remove(m);
			}
		}
		
		categories.sort((c1, c2) -> c1.name().compareTo(c2.name()));
		modules.sort((m1, m2) -> m1.getLabel().compareTo(m2.getLabel()));
		
		int y = 12;
		
		Draw.drawHollowRect(2, 12, Wrapper.getFontRenderer().getStringWidth(getCategoryWidth()) + (Destiny.getInstance().getFontManager().isGlobalTTF() ? 18 : 14), getCategoryHeight() + 10, 1.0F, -872415232);
		
		for(Category category : categories) {
			if(categories.indexOf(category) == selected) {
				Draw.drawBorderedRect(2, y, Wrapper.getFontRenderer().getStringWidth(getCategoryWidth()) + (Destiny.getInstance().getFontManager().isGlobalTTF() ? 18 : 14), y + 12, 1.0F, -872415232, color());
			} else {
				Gui.drawRect(2, y, Wrapper.getFontRenderer().getStringWidth(getCategoryWidth()) + (Destiny.getInstance().getFontManager().isGlobalTTF() ? 18 : 14), y + 12, -1795162112);
			}
			Wrapper.getFontRenderer().drawStringWithShadow(category.name(), 4, y + 2, -1);
			y += 12;
		}
		
		if(!inside) return;
		int y1 = 12;
		
		Draw.drawHollowRect(Wrapper.getFontRenderer().getStringWidth(getCategoryWidth()) + (Destiny.getInstance().getFontManager().isGlobalTTF() ? 20 : 16), y1,  Wrapper.getFontRenderer().getStringWidth(getCategoryWidth()) + 10 + Wrapper.getFontRenderer().getStringWidth(getModuleWidth()) + (Destiny.getInstance().getFontManager().isGlobalTTF() ? 16 : 12), getModuleHeight() + 10, 1.0F, -872415232);
		for(Module module : modules) {
			if(modules.indexOf(module) == selectedMod) {
				Draw.drawBorderedRect(Wrapper.getFontRenderer().getStringWidth(getCategoryWidth()) + (Destiny.getInstance().getFontManager().isGlobalTTF() ? 20 : 16), y1,  Wrapper.getFontRenderer().getStringWidth(getCategoryWidth()) + 10 + Wrapper.getFontRenderer().getStringWidth(getModuleWidth()) + (Destiny.getInstance().getFontManager().isGlobalTTF() ? 16 : 12), y1 + 12, 1.0F, -872415232, color());
			} else {
				Gui.drawRect(Wrapper.getFontRenderer().getStringWidth(getCategoryWidth()) + (Destiny.getInstance().getFontManager().isGlobalTTF() ? 20 : 16), y1,  Wrapper.getFontRenderer().getStringWidth(getCategoryWidth()) + 10 + Wrapper.getFontRenderer().getStringWidth(getModuleWidth()) + (Destiny.getInstance().getFontManager().isGlobalTTF() ? 16 : 12), y1 + 12, -1795162112);
			}
			Wrapper.getFontRenderer().drawStringWithShadow(module.getLabel(), Wrapper.getFontRenderer().getStringWidth(getCategoryWidth()) + (Destiny.getInstance().getFontManager().isGlobalTTF() ? 22 : 18), y1 + 2, module.isEnabled() ? 0xFFFF00 : -1);
			y1 += 12;
		}
	}
	
	private String getCategoryWidth() {
		String width = " ";
		for(Category category : categories) {
			if(category.name().length() > width.length()) {
				width = category.name();
			}
		}
		return width;
	}
	
	private int getCategoryHeight() {
		int y = 2;
		for(Category category : categories) {
			y += 12;
		}
		return y;
	}
	
	private String getModuleWidth() {
		String width = " ";
		for(Module module : modules) {
			if(module.getLabel().length() > width.length()) {
				width = module.getLabel();
			} else {
				continue;
			}
		}
		return width;
	}
	
	private int getModuleHeight() {
		int y = 2;
		for(Module module : modules) {
			y += 12;
		}
		return y;
	}
	
	private int color() {
		switch(mode.getCurrentProperty()) {
			case "Rainbow": {
				return Draw.getRainbow(0.9F).getRGB();
			}
			case "Red": {
				return -244411;
			}
			case "Orange": {
				return -679854;
			}
			case "Green": {
				return -11672179;
			}
			case "Cyan": {
				return -16532806;
			}
			case "Blue": {
				return Color.BLUE.getRGB();
			}
			case "Purple": {
				Color purple = new Color(0.7F, 0.0F, 0.75F);
				return purple.getRGB();
			}
			case "Grey": {
				return Color.GRAY.getRGB();
			}
		}
		return 0;
	}
	
	private int getEnabledColor() {
		switch(mode.getCurrentProperty()) {
			case "Rainbow": {
				return 0xFFFF00;
			}
			case "Red": {
				return -244411;
			}
			case "Orange": {
				return Color.ORANGE.getRGB();
			}
			case "Green": {
				return Color.GREEN.getRGB();
			}
			case "Cyan": {
				return Color.CYAN.getRGB();
			}
			case "Blue": {
				return Color.BLUE.getRGB();
			}
			case "Purple": {
				Color purple = new Color(0.7F, 0.0F, 0.75F, 0.1F);
				return purple.getRGB();
			}
			case "Grey": {
				return Color.GRAY.getRGB();
			}
		}
		return 0;
	}
	
	private void addCategories() {
		for(Category category : Category.values()) {
			if(categories.contains(category)) continue;
			categories.add(category);
		}
	}
	
	private void onToggle() {
		for(Module m : modules) {
			if(modules.indexOf(m) == selectedMod) {
				System.out.println(modules.indexOf(m));
				m.toggle();
				break;
			}
		}
	}
	
	private boolean isModuleEnabled() {
		for(Module m : modules) {
			if(modules.indexOf(m) == selectedMod) {
				return m.isEnabled();
			}
		}
		return false;
	}
	
	@Handler
	protected void onKeyToggle(final KeyToggleEvent event) 
	{
		switch(event.getKey()) {
			case Keyboard.KEY_UP: {
				if(inside) 
					selectedMod--;
				else
					selected--;
				
				if(selected < 0)
					selected = categories.size() - 1;
				
				if(selectedMod < 0)
					selectedMod = modules.size() - 1;
				
				break;
			}
			case Keyboard.KEY_RIGHT: {
				if(!inside)
					inside = true;
				else if(inside)
					onToggle();
				break;
			}
			case Keyboard.KEY_RETURN: {
				if(inside)
					onToggle();
				break;
			}
			case Keyboard.KEY_LEFT: {
				if(inside)
					inside = false;
				selectedMod = 0;
				break;
			}
			case Keyboard.KEY_DOWN: {
				if(inside)
					selectedMod++;
				else
					selected++;
				
				if(selected > categories.size() - 1)
					selected = 0;
				
				if(selectedMod > modules.size() - 1)
					selectedMod = 0;
				break;
			}
		}
	}
}
