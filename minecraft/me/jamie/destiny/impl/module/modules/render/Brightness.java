package me.jamie.destiny.impl.module.modules.render;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.impl.events.render.GammaEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Timer;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.potion.PotionEffect;

public class Brightness extends Module
{
	private Property<Boolean> nightVision;
	private float prevGamma;
	
	private Timer timer;
	
	public Brightness() {
		super("Brightness", new String[] { "fullbright", "bright", "brightness" });
		
		setBind(Keyboard.KEY_L);
		setCategory(Category.Render);
		setColor(0xFFF5F93E);
		setVisible(true);
		
		this.timer = new Timer();
	}	
	
	@Override
	public void loadProperties() {
		this.properties.add(nightVision = new Property<Boolean>("Brightness Vision", "Night Vision", true, new String[] { "vision" }));
	}
	
	@Override
	public void onEnable() {
		super.onEnable();
		
		if(nightVision.getProperty())
			Wrapper.getPlayer().addPotionEffect(new PotionEffect(16, Integer.MAX_VALUE));
	
		this.prevGamma = Wrapper.getGameSettings().gammaSetting;
	}
	
	@Override
	public void onDisable() {
		super.onDisable();
		
		Wrapper.getPlayer().removePotionEffect(16);
		Wrapper.getGameSettings().gammaSetting = prevGamma;
	}
	
	@Handler
	protected void onGamma(final GammaEvent event) {
		event.setGamma(100.0F);
	}
}
