package me.jamie.destiny.impl.module.modules.render;

import org.lwjgl.opengl.GL11;

import me.jamie.destiny.impl.events.render.Render3DEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.destiny.util.gui.Draw;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.EntitySlime;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;

public class ESP extends Module
{
	private Property<String> mode;
	private Property<Boolean> animals, mobs, players, items;
	
	public ESP() {
		super("ESP", new String[] { "esp", "playeresp" });
		
		setCategory(Category.Render);
	}
		
	@Override
	public void loadProperties() 
	{
		this.properties.add(mode = new Property<String>("ESP Mode", "Mode", "Box", new String[] { "mode" }) {
			public void addProperties() {
				getProperties().add("Box");
			}
		});
		
		this.properties.add(animals = new Property<Boolean>("ESP Animals", "Animals", true, new String[] { "animals" }));
		this.properties.add(mobs = new Property<Boolean>("ESP Mobs", "Mobs", true, new String[] { "mobs" }));	
		this.properties.add(players = new Property<Boolean>("ESP Players", "Players", true, new String[] { "players" }));
		this.properties.add(items = new Property<Boolean>("ESP Items", "Items", true, new String[] { "items" }));
	}
	
	@Handler
	protected void onRender3D(final Render3DEvent event) 
	{
		for(Object o : Wrapper.getWorld().loadedEntityList) 
		{
			if(o == null || (!(o instanceof Entity))) continue;
			final Entity entity = (Entity)o;
			
			if(entity.equals(Wrapper.getPlayer())) continue;
			if(!canRender(entity)) continue;
			
			double x = interpolate(entity.lastTickPosX, entity.posX) - Wrapper.getMinecraft().getRenderManager().renderPosX;
			double y = interpolate(entity.lastTickPosY, entity.posY) - Wrapper.getMinecraft().getRenderManager().renderPosY;
			double z = interpolate(entity.lastTickPosZ, entity.posZ) - Wrapper.getMinecraft().getRenderManager().renderPosZ;
			float[] colors;
			
			AxisAlignedBB bb = new AxisAlignedBB(x - 0.4F, y, z - 0.4F, x + entity.width - 0.1, y + entity.height + (entity.isSneaking() ? -0.2 : 0.2), z + entity.width - 0.1);
			
			if(entity instanceof EntityLivingBase)
				colors = getColor((EntityLivingBase)entity);
			else 
				colors = new float[] { 0.0F, 1.0F, 0.0F };
			
			GL11.glPushMatrix();
			GlStateManager.disableLighting();
			GlStateManager.enableBlend();
			GlStateManager.blendFunc(770, 771);
			GlStateManager.disableDepth();
			GlStateManager.depthMask(false);
			GlStateManager.disableTexture();
			GL11.glEnable(2848);
			
			GL11.glLineWidth(3.2F);
			GlStateManager.pushMatrix();
            GlStateManager.translate(x, y, z);
            GlStateManager.rotate(-entity.rotationYaw, 0.0F, entity.height, 0.0F);
            GlStateManager.translate(-x, -y, -z);
			GlStateManager.color(colors[0], colors[1], colors[2], 0.5F);
			Draw.drawOutlinedBox(bb);
			GlStateManager.popMatrix();
			
			GL11.glDisable(2848);
			GlStateManager.enableTexture();
			GlStateManager.depthMask(true);
			GlStateManager.enableDepth();
			GlStateManager.disableBlend();
			GlStateManager.enableLighting();
	        GL11.glPopMatrix();
		}
	}
	
	private double interpolate(double prev, double cur) 
	{
		return prev + (cur - prev);
	}
	
	private boolean canRender(final Entity entity) 
	{
		if(entity instanceof EntityPlayer && players.getProperty())
			return true;
		if(entity instanceof EntityAnimal && animals.getProperty())
			return true;
		if((entity instanceof EntityMob || entity instanceof EntitySlime) && mobs.getProperty())
			return true;
		if(entity instanceof EntityItem && items.getProperty())
			return true;
		return false;
	}
	
	private float[] getColor(final EntityLivingBase entity) {
		float health = entity.getHealth() / 2;
		if(health > 8) 
			return new float[] { 0.0F, 1.0F, 0.0F };
		else if(health > 6)
			return new float[] { 0.5F, 0.15F, 0.0F };
		else if(health > 4) 
			return new float[] { 0.7F, 0.15F, 0.0F };
		else if(health > 2)
			return new float[] { 0.95F, 0.0F, 0.0F };
		else if(health >= 0)
			return new float[] { 1.0F, 0.0F, 0.0F };
		return new float[] { 0.0F, 1.0F, 0.0F };
	}
	
}
