package me.jamie.destiny.impl.module.modules.render;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import me.jamie.destiny.impl.events.render.NametagEvent;
import me.jamie.destiny.impl.events.render.Render3DEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.destiny.util.gui.Draw;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagList;

public class Nametags extends Module {

	private Property<Boolean> armor, health;
	
	public Nametags() {
		super("Nametags");

		setCategory(Category.Render);
	}
	
	@Override
	public void loadProperties() 
	{
		this.properties.add(armor = new Property<Boolean>("Nametags Armor", "Armor", true, new String[] { "armor" }));
		this.properties.add(health = new Property<Boolean>("Nametags Health", "Health", true, new String[] { "health" }));
	}

	// RendererLivingEntity
	@Handler
	protected void render(final NametagEvent event) {
		event.setCancelled(true);
	}

	@Handler
	protected void onRender3D(final Render3DEvent event) {
		for (Object o : Wrapper.getWorld().loadedEntityList) {
			if (o == null)
				continue;
			if (!(o instanceof EntityPlayer))
				continue;
			final EntityPlayer player = (EntityPlayer) o;

			if (player.equals(Wrapper.getPlayer()))
				continue;

			double x = player.lastTickPosX + (player.posX - player.lastTickPosX) * event.getPartialTicks()
					- Wrapper.getMinecraft().getRenderManager().renderPosX;
			double y = player.lastTickPosY + (player.posY - player.lastTickPosY) * event.getPartialTicks()
					- Wrapper.getMinecraft().getRenderManager().renderPosY;
			double z = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * event.getPartialTicks()
					- Wrapper.getMinecraft().getRenderManager().renderPosZ;

			renderNametag(player, x, y, z);
		}
	}

	private void renderNametag(EntityLivingBase entity, double x, double y, double z) {
		FontRenderer var12 = Wrapper.getFontRenderer();
		float var13 = getNametagSize(entity);
		float var14 = 0.016666668F * var13;
		GlStateManager.pushMatrix();
		RenderHelper.enableStandardItemLighting();
		GlStateManager.translate((float) x + 0.0F, (float) y + entity.height + 0.5F, (float) z);
		GL11.glNormal3f(0.0F, 1.0F, 0.0F);
		GlStateManager.rotate(-Wrapper.getMinecraft().getRenderManager().playerViewY, 0.0F, 1.0F, 0.0F);
		GlStateManager.rotate(Wrapper.getMinecraft().getRenderManager().playerViewX, 1.0F, 0.0F, 0.0F);
		GlStateManager.scale(-var14, -var14, var14);
		GlStateManager.disableLighting();
		GlStateManager.depthMask(false);
		GlStateManager.disableDepth();
		GlStateManager.enableBlend();
		GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
		// Tessellator var15 = Tessellator.getInstance();
		// WorldRenderer var16 = var15.getWorldRenderer();
		byte var17 = 0;

		if (Wrapper.getPlayer().getDistanceToEntity(entity) < 29) {
			var17 = (byte) ((byte) -Wrapper.getPlayer().getDistanceToEntity(entity) / 3 - 3);
		} else {
			var17 = -12;
		}

		GlStateManager.disableTexture();
		GL11.glColor4f(0.0F, 0.0F, 0.0F, 0.5F);
		// var16.startDrawingQuads();
		int var18 = var12.getStringWidth(entity.getDisplayName().getFormattedText() + " " + getHealth(entity)) / 2;
		// var16.func_178960_a(0.0F, 0.0F, 0.0F, 0.5F);
		// var16.addVertex((double)(-var18 - 1), (double) (-1 + var17), 0.0D);
		// var16.addVertex((double)(-var18 - 1), (double) (8 + var17), 0.0D);
		// var16.addVertex((double)(var18 + 1), (double) (8 + var17), 0.0D);
		// var16.addVertex((double)(var18 + 1), (double) (-1 + var17), 0.0D);
		Color color = new Color(0.0F, 0.0F, 0.0F, 0.5F);
		GL11.glEnable(GL11.GL_POLYGON_OFFSET_FILL);
		GL11.glPolygonOffset(1.0F, -2000000F);
		// -2 w/o health
		Draw.drawBorderedRect((double) (-var18 + (health.getProperty() ? - 2 : 6)), (double) (-2 + var17), (double) (var18 + (health.getProperty() ? 2 : -7)), (double) (9 + var17),
				1.0F, 0xFF010300, color.getRGB());
		// var15.draw();
		GlStateManager.enableTexture();
		// var12.drawString(entity.getName(),
		// -var12.getStringWidth(entity.getName()) / 2, var17, 553648127);
		GlStateManager.enableDepth();
		// GlStateManager.depthMask(true);
		String label = entity.getDisplayName().getFormattedText() + (health.getProperty() ? " " + getHealth(entity) : "");
		var12.drawStringWithShadow(label,
				-var12.getStringWidth(label) / 2, var17,
				-1);
		GL11.glPolygonOffset(1.0F, 2000000F);
		GL11.glDisable(GL11.GL_POLYGON_OFFSET_FILL);
		if(armor.getProperty())
			renderArmor(entity);
		GlStateManager.enableLighting();
		GlStateManager.depthMask(true);
		GlStateManager.disableBlend();
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		RenderHelper.disableStandardItemLighting();
		GlStateManager.popMatrix();
	}

	private String getHealth(EntityLivingBase entity) {
		float health = entity.getHealth() / 2;
		String color = "";
		String tag = "";
		if (health > 8)
			color = "�2";
		else if (health > 6)
			color = "�a";
		else if (health > 4)
			color = "�e";
		else if (health > 2)
			color = "�6";
		else if (health > 1)
			color = "�c";
		else if (health >= 0)
			color = "�4";
		tag = color + String.format("%.1f", health);
		if(tag.endsWith(".0"))
			tag = tag.substring(0, tag.length() - 2);
		return  tag;
	}

	/*
	 * Credits to Latematt for most of this.
	 */
	private void renderArmor(final EntityLivingBase entity) {
		List<ItemStack> stacks = new ArrayList<>();

		final EntityPlayer player = (EntityPlayer)entity;
		
		if (player.getCurrentEquippedItem() != null)
			stacks.add(player.getCurrentEquippedItem());
		for (int i = 3; i >= 0; i--) {
			final ItemStack armor = player.inventory.armorInventory[i];
			if (armor == null)
				continue;
			stacks.add(armor);
		}

		int var18 = Wrapper.getFontRenderer().getStringWidth(entity.getDisplayName().getFormattedText()) / 2;

		int offset = var18 - (stacks.size() - 1) * 9 - 9;
		int xPos = 0;

		byte var17 = 0;

		if (Wrapper.getPlayer().getDistanceToEntity(entity) < 29) {
			var17 = (byte) ((byte) -Wrapper.getPlayer().getDistanceToEntity(entity) / 3 - 3);
		} else {
			var17 = -12;
		}

		for (ItemStack stack : stacks) {
			GlStateManager.pushMatrix();
	        GlStateManager.depthMask(true);
	        GlStateManager.clear(GL11.GL_ACCUM);

	        RenderHelper.enableStandardItemLighting();
	        Wrapper.getMinecraft().getRenderItem().zLevel = -150.0F;

	        GlStateManager.disableLighting();
	        GlStateManager.disableDepth();
	        GlStateManager.disableBlend();
	        GlStateManager.enableLighting();
	        GlStateManager.enableDepth();
	        GlStateManager.disableLighting();
	        GlStateManager.disableDepth();
	        GlStateManager.disableAlpha();
	        GlStateManager.disableAlpha();
	        GlStateManager.disableBlend();
	        GlStateManager.enableBlend();
	        GlStateManager.enableAlpha();
	        GlStateManager.enableAlpha();
	        GlStateManager.enableLighting();
	        GlStateManager.enableDepth();
			Wrapper.getMinecraft().getRenderItem().renderItemAboveHead(stack, -var18 + offset + xPos, var17 - 20);
			Wrapper.getMinecraft().getRenderItem().renderItemOverlayIntoGUI(Wrapper.getMinecraft().fontRendererObj,
					stack, -var18 + offset + xPos, var17 - 20);
			Wrapper.getMinecraft().getRenderItem().zLevel = 0.0F;
	        RenderHelper.disableStandardItemLighting();

	        GlStateManager.disableCull();
	        GlStateManager.enableAlpha();
	        GlStateManager.disableBlend();
	        GlStateManager.disableLighting();
	        GlStateManager.scale(0.5F, 0.5F, 0.5F);
	        GlStateManager.disableDepth();
			if (stack.getItem() == Items.golden_apple && stack.hasEffect()) {
				Wrapper.getMinecraft().fontRendererObj.drawStringWithShadow("god", (-var18 + offset + xPos) * 2,
						(var17 - 20) * 2, 0xFFFF0000);
			} else {
				NBTTagList enchants = stack.getEnchantmentTagList();

				if (enchants != null) {
					int encY = 0;
					Enchantment[] important = new Enchantment[] { Enchantment.field_180310_c, Enchantment.unbreaking,
							Enchantment.field_180314_l, Enchantment.fireAspect, Enchantment.efficiency,
							Enchantment.field_180309_e, Enchantment.power, Enchantment.flame, Enchantment.punch,
							Enchantment.fortune, Enchantment.infinity, Enchantment.thorns };
					if (enchants.tagCount() >= 6) {
						Wrapper.getMinecraft().fontRendererObj.drawStringWithShadow("god", (-var18 + offset + xPos) * 2,
								(var17 - 20) * 2, 0xFFFF0000);
					} else {
						for (int index = 0; index < enchants.tagCount(); ++index) {
							short id = enchants.getCompoundTagAt(index).getShort("id");
							short level = enchants.getCompoundTagAt(index).getShort("lvl");
							Enchantment enc = Enchantment.func_180306_c(id);
							if (enc != null) {
								for (Enchantment importantEnchantment : important) {
									if (enc == importantEnchantment) {
										String encName = enc.getTranslatedName(level).substring(0, 1).toLowerCase();
										if (level > 99)
											encName = encName + "99+";
										else
											encName = encName + level;
										Wrapper.getMinecraft().fontRendererObj.drawStringWithShadow("�b" + encName,
												(-var18 + offset + xPos) * 2, (var17 - 20 + encY) * 2, 0xFFAAAAAA);
										encY += 5;
										break;
									}
								}
							}
						}
					}
				}
			}
	        GlStateManager.enableDepth();
	        GlStateManager.scale(2.0F, 2.0F, 2.0F);
			xPos += 18;
			GlStateManager.enableLighting();
			GlStateManager.popMatrix();
		}

	}

	// RenderItem
	private float getNametagSize(final EntityLivingBase entity) {
		float distance = Wrapper.getPlayer().getDistanceToEntity(entity) / 3;
		return distance <= 3 ? 3 : distance;
	}

	private float getNametagHeight(final EntityLivingBase entity) {
		return (Wrapper.getPlayer().getDistanceToEntity(entity) / 8) - 0.5F;
	}
}
