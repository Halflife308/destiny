package me.jamie.destiny.impl.module.modules.render;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.impl.events.block.BlockLightEvent;
import me.jamie.destiny.impl.events.render.CullingEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.block.Block;

public class Wallhack extends Module
{
	private static List<Integer> blocks = new ArrayList<>();
	private Integer[] ids = new Integer[] { 14, 15, 16, 46, 56, 57, 58, 61, 62, 73, 74 };
	
	private float gamma;
	
	public Wallhack() 
	{
		super("Wallhack", new String[] { "wallhack", "xray" });
		
		setBind(Keyboard.KEY_X);
		setCategory(Category.Render);
		setColor(0xFFADA7A7);
		setVisible(true);
	}
	
	@Override
	public void onEnable() {
		blocks.addAll(Arrays.asList(ids));
		gamma = Wrapper.getGameSettings().gammaSetting;
		Wrapper.getMinecraft().renderGlobal.loadRenderers();
		Wrapper.getGameSettings().gammaSetting = 100.0F;
		Wrapper.getGameSettings().ambientOcclusion = 0;
	}
	
	@Override
	public void onDisable() {
		blocks.clear();
		Wrapper.getMinecraft().renderGlobal.loadRenderers();
		Wrapper.getGameSettings().gammaSetting = gamma;
		Wrapper.getGameSettings().ambientOcclusion = 1;
	}
	
	@Handler
	protected void onBlockLight(final BlockLightEvent event) {
		event.setLight(0.0F);
	}
	
	@Handler
	protected void onCull(final CullingEvent event) {
		event.setCancelled(true);
	}
	
	//BlockModelRenderer
	public static boolean hasBlock(final int id) 
	{
		return blocks.contains(id);
	}
}
