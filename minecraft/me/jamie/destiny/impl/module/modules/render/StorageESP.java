package me.jamie.destiny.impl.module.modules.render;

import org.lwjgl.opengl.GL11;

import me.jamie.destiny.impl.events.render.Render3DEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.destiny.util.gui.Draw;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.tileentity.TileEntityDispenser;
import net.minecraft.tileentity.TileEntityEnchantmentTable;
import net.minecraft.tileentity.TileEntityEnderChest;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.tileentity.TileEntityHopper;
import net.minecraft.util.AxisAlignedBB;

public class StorageESP extends Module
{
	private Property<Boolean> chest, ender, furnace, dispenser, hopper, enchantment;
	
	public StorageESP() {
		super("StorageESP", new String[] { "storageesp", "chestesp" });
		
		setCategory(Category.Render);
	}
	
	@Override
	public void loadProperties() 
	{
		this.properties.add(chest = new Property<Boolean>("StorageESP Chest", "Chests", true, new String[] { "chests" }));
		this.properties.add(ender = new Property<Boolean>("StorageESP Ender", "Ender Chests", true, new String[] { "echests", "enderchests" }));	
		this.properties.add(furnace = new Property<Boolean>("StorageESP Furnace", "Furnaces", true, new String[] { "furnaces" }));
		this.properties.add(dispenser = new Property<Boolean>("StorageESP Dispenser", "Dispensers", true, new String[] { "dispensers" }));	
		this.properties.add(hopper = new Property<Boolean>("StorageESP Hopper", "Hoppers", true, new String[] { "hoppers" }));
		this.properties.add(enchantment = new Property<Boolean>("StorageESP Enchantment", "Enchantment Tables", true, new String[] { "etables", "enchantmenttables", "enchantmenttable" }));	
	}
	
	@Handler
	protected void onRender3D(final Render3DEvent event) 
	{
		for(Object o : Wrapper.getWorld().loadedTileEntityList) {
			if(o == null || (!(o instanceof TileEntity))) continue;
			final TileEntity tileEntity = (TileEntity)o;
			if(!canRender(tileEntity)) continue;
			
			float[] colors = getColor(tileEntity);
			
			double x = tileEntity.getPos().getX() - Wrapper.getMinecraft().getRenderManager().renderPosX;
			double y = tileEntity.getPos().getY() - Wrapper.getMinecraft().getRenderManager().renderPosY;
			double z = tileEntity.getPos().getZ() - Wrapper.getMinecraft().getRenderManager().renderPosZ;
			
			AxisAlignedBB bb = new AxisAlignedBB(x, y, z, x + 1, y + 1, z + 1);
			

            if (tileEntity instanceof TileEntityChest) {
                TileEntityChest chest = (TileEntityChest)tileEntity;
                if (chest.adjacentChestZPos != null) {
                    bb = new AxisAlignedBB(x + 0.0625, y, z + 0.0625, x + 0.9375, y + 0.875, z + 1.9375);
                } else if (chest.adjacentChestXPos != null) {
                    bb = new AxisAlignedBB(x + 0.0625, y, z + 0.0625, x + 1.9375, y + 0.875, z + 0.9375);
                } else if (chest.adjacentChestZPos == null && chest.adjacentChestXPos == null
                        && chest.adjacentChestZNeg == null && chest.adjacentChestXNeg == null) {
                    bb = new AxisAlignedBB(x + 0.0625, y, z + 0.0625, x + 0.9375, y + 0.875, z + 0.9375);
                } else {
                    continue;
                }
            } else if (tileEntity instanceof TileEntityEnderChest) {
                bb = new AxisAlignedBB(x + 0.0625, y, z + 0.0625, x + 0.9375, y + 0.875, z + 0.9375);
            }
			
			GL11.glPushMatrix();
			GlStateManager.disableLighting();
			GlStateManager.enableBlend();
			GlStateManager.blendFunc(770, 771);
			GlStateManager.disableDepth();
			GlStateManager.depthMask(false);
			GlStateManager.disableTexture();
			GL11.glEnable(2848);
			
			GL11.glLineWidth(1.2F);
			GL11.glColor4f(colors[0], colors[1], colors[2], 0.25F);
			Draw.drawBox(bb);
			GL11.glColor4f(colors[0], colors[1], colors[2], 0.5F);
			Draw.drawOutlinedBox(bb);
			
			GL11.glDisable(2848);
			GlStateManager.enableTexture();
			GlStateManager.depthMask(true);
			GlStateManager.enableDepth();
			GlStateManager.disableBlend();
			GlStateManager.enableLighting();
	        GL11.glPopMatrix();
		}
	}
	
	private boolean canRender(final TileEntity tileEntity) 
	{
		if(tileEntity instanceof TileEntityChest && chest.getProperty())
			return true;
		if(tileEntity instanceof TileEntityEnderChest && ender.getProperty())
			return true;
		if(tileEntity instanceof TileEntityFurnace && furnace.getProperty()) 
			return true;
		if(tileEntity instanceof TileEntityHopper && hopper.getProperty())
			return true;
		if(tileEntity instanceof TileEntityDispenser && dispenser.getProperty())
			return true;
		if(tileEntity instanceof TileEntityEnchantmentTable)
			return true;
		return false;
	}
	
	private float[] getColor(final TileEntity tileEntity)
	{
		if(tileEntity instanceof TileEntityChest)
			return new float[] { 1.0F, 0.9F, 0.0F };
		else if(tileEntity instanceof TileEntityEnderChest)
			return new float[] { 1.0F, 0.0F, 1.0F };
		else if(tileEntity instanceof TileEntityFurnace || tileEntity instanceof TileEntityHopper || tileEntity instanceof TileEntityDispenser)
			return new float[] { 0.75F, 0.75F, 0.75F };
		else if(tileEntity instanceof TileEntityEnchantmentTable)
			return new float[] { 0.7F, 0.0F, 0.75F };
		return new float[] { 0.0F, 0.0F, 0.0F };
	}
}
