package me.jamie.destiny.impl.module.modules.world;

import me.jamie.destiny.impl.events.motion.UpdateEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;

public class FastPlace extends Module
{
	private int prev;
	
	public FastPlace() {
		super("FastPlace", new String[] { "Fast Place" } );
		
		setCategory(Category.World);
		setColor(0xFFF38644);
		setVisible(true);
	}
	
	@Override
	public void onEnable() {
		super.onEnable();
		
		prev = Wrapper.getMinecraft().rightClickDelayTimer;
	}
	
	@Override
	public void onDisable() 
	{
		super.onDisable();
		
		Wrapper.getMinecraft().rightClickDelayTimer = prev;
	}
	
	@Handler
	protected void onUpdate(final UpdateEvent event) {
		Wrapper.getMinecraft().rightClickDelayTimer = 0;
	}
}
