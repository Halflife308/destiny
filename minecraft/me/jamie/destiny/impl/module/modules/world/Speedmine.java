package me.jamie.destiny.impl.module.modules.world;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.impl.events.block.BreakBlockEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.util.EnumFacing;

public class Speedmine extends Module
{
	private Property<String> mode;
	private Property<Float> multiplier;
	
	public Speedmine() 
	{
		super("Speed Mine", new String[] { "fastmine", "fastbreak", "speedmine" } );
		
		setBind(Keyboard.KEY_O);
		setCategory(Category.World);
		setColor(0xFFE7B86D);
		setVisible(true);
	}

	@Override
	public void loadProperties() 
	{
		this.properties.add(mode = new Property<String>("Speedmine Mode", "Mode", "Normal", new String[] { "mode" }) {
			public void addProperties() {
				getProperties().add("Normal");
				getProperties().add("Packet");
			}
		});
		
		this.properties.add(multiplier = new Property<Float>("Speedmine Multiplier", "Multiplier", (Float)1.25F, (Float)1.0F, (Float)5.0F, (Float)0.1F, new String[] { "multiplier" }));
	}
	
	@Handler
	protected void onBreakBlock(final BreakBlockEvent event) 
	{
		if(mode.getCurrentProperty().equals("Normal") || mode.getCurrentProperty().equals("Packet")) {
			event.setDelay(0);
			event.setMultiplier(multiplier.getFloatProperty());
			
			if(mode.getCurrentProperty().equals("Packet")) {
				if(event.getBlockDamage() > 0.65F)
					event.setBlockDamage(1.0F);
				Wrapper.getPlayer().sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.STOP_DESTROY_BLOCK, event.getBlockPos(), EnumFacing.DOWN));
			}
		}
	}
}
