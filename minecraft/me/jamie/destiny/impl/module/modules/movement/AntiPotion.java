package me.jamie.destiny.impl.module.modules.movement;

import me.jamie.destiny.impl.events.motion.UpdateEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.potion.PotionEffect;

public class AntiPotion extends Module
{

	public AntiPotion() {
		super("Anti Potion", new String[] { "zoot", "antipotion" });
		
		setCategory(Category.Player);
	}
	
	@Handler
	protected void onUpdate(final UpdateEvent event) {
		if(Wrapper.getPlayer().getActivePotionEffects().isEmpty()) return;
		if(isGoodPotion()) return;
		for(int i = 0; i < 60; i++) {
			Wrapper.getPlayer().sendQueue.addToSendQueue(new C03PacketPlayer(true));
		}
	}
	
	/*
	 * Potions that have desirable long-term effects (e.g. haste, resistance), which should not be removed.
	 */
	private boolean isGoodPotion() {
		for(Object o : Wrapper.getPlayer().getActivePotionEffects()) {
			PotionEffect pe = (PotionEffect)o;
			if(pe.getEffectName().equals("fire_resistance") || pe.getEffectName().equals("haste") || pe.getEffectName().equals("resistance"))
				return true;
		}
		return false;
	}
}
