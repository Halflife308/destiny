package me.jamie.destiny.impl.module.modules.movement;

import me.jamie.destiny.impl.events.item.UseItemEvent;
import me.jamie.destiny.impl.events.motion.PostMotionUpdateEvent;
import me.jamie.destiny.impl.events.motion.PreMotionUpdateEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C07PacketPlayerDigging.Action;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;

public class NoSlowdown extends Module
{

	public NoSlowdown() {
		super("No Slowdown", new String[] { "noslowdown", "noslow" });
		
		setCategory(Category.Movement);
	}

	@Handler
	protected void onUseItem(final UseItemEvent event) {
		event.setCancelled(true);
	}

	@Handler
	protected void onPreMotionUpdate(final PreMotionUpdateEvent event) {
		if(!Wrapper.getPlayer().isBlocking()) return;
		Wrapper.getPlayer().sendQueue.addToSendQueue(new C07PacketPlayerDigging(Action.RELEASE_USE_ITEM, BlockPos.ORIGIN, EnumFacing.DOWN));
	}
	
	@Handler
	protected void onPostMotionUpdate(final PostMotionUpdateEvent event) {
		if(!Wrapper.getPlayer().isBlocking()) return;
		Wrapper.getPlayer().sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(Wrapper.getPlayer().getCurrentEquippedItem()));
	}
}
