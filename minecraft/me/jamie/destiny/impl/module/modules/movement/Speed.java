package me.jamie.destiny.impl.module.modules.movement;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.impl.events.motion.MoveEntityEvent;
import me.jamie.destiny.impl.events.motion.UpdateEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.PlayerUtils;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.potion.Potion;
import net.minecraft.util.MovementInput;

public class Speed extends Module 
{
	private Property<String> mode;
	private int state, cooldown;
	private double speed, prev;

	public Speed() {
		super("Speed");

		setBind(Keyboard.KEY_G);
		setCategory(Category.Movement);
		setColor(0xFF68F186);
		setVisible(true);
	}

	@Override
	public void loadProperties() {
		this.properties.add(mode = new Property<String>("Speed Mode", "Mode", "Bhop", new String[] { "mode" }) {
			public void addProperties() {
				getProperties().add("Bhop");
				getProperties().add("YPort");
			}
		});
	}
	
	@Override
	public void onDisable() {
		super.onDisable();
		
		state = 0;
		cooldown = 0;
	}

	@Handler
	protected void onMoveEntity(final MoveEntityEvent event) 
	{
        if ((Wrapper.getPlayer().moveForward == 0F) && (Wrapper.getPlayer().moveStrafing == 0F) || PlayerUtils.isOnLiquid())
            return;
        if (Wrapper.getPlayer().onGround) {
            state = 2;
            Wrapper.getMinecraft().timer.timerSpeed = 1.0F;
        }
        if (round(Wrapper.getPlayer().posY - (int) Wrapper.getPlayer().posY, 3) == round(0.138D, 3)) {
            Wrapper.getPlayer().motionY -= 0.08D;
            event.setY(event.getY() - 0.09316090325960147D);
            Wrapper.getPlayer().posY -= 0.09316090325960147D;
        }
        if ((state == 1) && ((Wrapper.getPlayer().moveForward != 0.0F) || (Wrapper.getPlayer().moveStrafing != 0.0F))) {
            state = 2;
            speed = (1.35D * getSpeed() - 0.01D);
        } else if (state == 2) {
            state = 3;
            if ((Wrapper.getPlayer().moveForward != 0.0F) || (Wrapper.getPlayer().moveStrafing != 0.0F)) {
                Wrapper.getPlayer().motionY = 0.4D;
                event.setY(0.4D);
                if (cooldown > 0)
                    cooldown -= 1;
                speed *= 2.149D;
            }
        } else if (state == 3) {
            state = 4;
            double difference = 0.66D * (prev - getSpeed());
            speed = (prev - difference);
        } else {
            if ((Wrapper.getWorld().getCollidingBoundingBoxes(Wrapper.getPlayer(), Wrapper.getPlayer().getEntityBoundingBox().offset(0.0D, Wrapper.getPlayer().motionY, 0.0D)).size() > 0) || (Wrapper.getPlayer().isCollidedVertically))
                state = 1;
            speed = (prev - prev / 159.0D);
        }
        speed = Math.max(speed, getSpeed());
        MovementInput movementInput = Wrapper.getPlayer().movementInput;
        float forward = movementInput.moveForward;
        float strafe = movementInput.moveStrafe;
        float yaw = Wrapper.getPlayer().rotationYaw;
        if ((forward == 0.0F) && (strafe == 0.0F)) {
            event.setX(0D);
            event.setZ(0D);
            speed = 0D;
        } else if (forward != 0.0F) {
            if (strafe >= 1.0F) {
                yaw += (forward > 0.0F ? -45 : 45);
                strafe = 0.0F;
            } else if (strafe <= -1.0F) {
                yaw += (forward > 0.0F ? 45 : -45);
                strafe = 0.0F;
            }
            if (forward > 0.0F) {
                forward = 1.0F;
            } else if (forward < 0.0F) {
                forward = -1.0F;
            }
        }
        double mx = Math.cos(Math.toRadians(yaw + 90.0F));
        double mz = Math.sin(Math.toRadians(yaw + 90.0F));
        if (cooldown == 0) {
            event.setX(forward * speed * mx + strafe * speed * mz);
            event.setZ(forward * speed * mz - strafe * speed * mx);
        }
        Wrapper.getPlayer().stepHeight = 0.6F;
        if ((forward == 0.0F) && (strafe == 0.0F)) {
            event.setX(0D);
            event.setZ(0D);
        }
	}

	@Handler
	protected void onUpdate(final UpdateEvent event) 
	{
		setExtension("�7" + mode.getCurrentProperty());
		if ((Wrapper.getPlayer().moveForward == 0.0F) && (Wrapper.getPlayer().moveStrafing == 0.0F)
				&& (Wrapper.getPlayer().onGround)) {
			cooldown = 2;
			speed *= 1.0800000429153442D;
			state = 2;
		}
		double xDist = Wrapper.getPlayer().posX - Wrapper.getPlayer().prevPosX;
		double zDist = Wrapper.getPlayer().posZ - Wrapper.getPlayer().prevPosZ;
		prev = Math.sqrt(xDist * xDist + zDist * zDist);
	}
	
	private double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();
		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
	
	private double getSpeed() {
		double baseSpeed = 0.2873D;
		if (Wrapper.getPlayer().isPotionActive(Potion.moveSpeed)) {
			int amplifier = Wrapper.getPlayer().getActivePotionEffect(Potion.moveSpeed).getAmplifier();
			baseSpeed *= (1.0D + 0.2D * (amplifier + 1));
		}
		return baseSpeed;
	}
	
	public void setSpeed(double speed) {
		this.speed = speed;
	}
}
