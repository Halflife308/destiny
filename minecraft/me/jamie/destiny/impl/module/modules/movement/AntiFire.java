package me.jamie.destiny.impl.module.modules.movement;

import me.jamie.destiny.impl.events.motion.UpdateEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.network.play.client.C03PacketPlayer;

public class AntiFire extends Module
{

	public AntiFire() {
		super("Anti Fire", new String[] { "noburn", "antifire" });
		
		setCategory(Category.Player);
	}
	
	@Handler
	protected void onUpdate(final UpdateEvent event) {
		if(!Wrapper.getPlayer().isBurning()) return;
		for(int i = 0; i < 60; i++) {
			Wrapper.getPlayer().sendQueue.addToSendQueue(new C03PacketPlayer(true));
		}
	}
}
