package me.jamie.destiny.impl.module.modules.movement;

import me.jamie.destiny.impl.events.motion.UpdateEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;

/**
 * 
 * @author Jamie
 * @date 11/8/16
 * Sprinting without having to hold the CTRL key.
 */
public class Sprint extends Module
{
	private Property<Boolean> backwards;
	
	public Sprint() 
	{
		super("Sprint", new String[] { "sprint", "autosprint" });
		
		setCategory(Category.Movement);
		setColor(0xFF39ED60);
		setVisible(true);
	}
	
	@Override
	public void loadProperties() {
		this.properties.add(backwards = new Property<Boolean>("Sprint Backwards", "Backwards", true, new String[] { "backwards" }));
	}
	
	@Handler
	protected void onUpdate(final UpdateEvent event) {
		boolean checks = (backwards.getProperty() ? Wrapper.getPlayer().movementInput.moveForward != 0.0F
				: Wrapper.getPlayer().movementInput.moveForward > 0.0F)
				&& !Wrapper.getPlayer().isCollidedHorizontally
				&& Wrapper.getPlayer().getFoodStats().getFoodLevel() > 6;
	   Wrapper.getPlayer().setSprinting(checks);
	}
}
