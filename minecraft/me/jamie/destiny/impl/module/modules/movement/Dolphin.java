package me.jamie.destiny.impl.module.modules.movement;

import me.jamie.destiny.impl.events.block.BoundingBoxEvent;
import me.jamie.destiny.impl.events.motion.MoveEntityEvent;
import me.jamie.destiny.impl.events.motion.UpdateEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.util.game.Timer;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.BlockHopper;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.material.Material;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;

public class Dolphin extends Module
{
	private Timer timer;
	
	public Dolphin() {
		super("Dolphin");
		
		setCategory(Category.Movement);
		setColor(0xFF45B6F0);
		setVisible(true);
		
		this.timer = new Timer();
	}
	
	@Handler
	protected void onUpdate(final UpdateEvent event) {
		if(!Wrapper.getPlayer().isInWater() || Wrapper.getPlayer().isSneaking()) return;
		if(timer.check(250)) {
			Wrapper.getPlayer().motionY = Wrapper.getPlayer().isSprinting() ? 0.08 : 0.1;
			timer.reset();
		}
	}
}
