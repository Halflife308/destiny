package me.jamie.destiny.impl.module.modules.movement;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import me.jamie.destiny.impl.events.motion.UpdateEvent;
import me.jamie.destiny.impl.events.packet.PacketSendEvent;
import me.jamie.destiny.impl.events.render.Render3DEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.filter.Filter;
import me.jamie.lib.event.filter.filters.AcceptedPackets;
import me.jamie.lib.event.filter.filters.PacketFilter;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C09PacketHeldItemChange;
import net.minecraft.network.play.client.C0BPacketEntityAction;

public class Blink extends Module {
	private Queue<Packet> packets;
	private List<double[]> positions;

	public Blink() {
		super("Blink", new String[] { "blink", "lagswitch" });

		setBind(Keyboard.KEY_B);
		setCategory(Category.Player);
		setColor(0xFFC443EA);
		setVisible(true);

		packets = new ConcurrentLinkedQueue<>();
		positions = new ArrayList<>();
	}

	@Override
	public void onDisable() {
		super.onDisable();

		while (!packets.isEmpty())
			Wrapper.getPlayer().sendQueue.addToSendQueue(packets.poll());
		positions.clear();
	}

	@Handler
	protected void onUpdate(final UpdateEvent event) {
		setExtension("�7" + packets.size());
	}
	
	@Handler
	protected void onRender3D(final Render3DEvent event) {
		GlStateManager.pushMatrix();
		RenderHelper.enableStandardItemLighting();
		GlStateManager.disableLighting();
		GlStateManager.enableBlend();
		GlStateManager.blendFunc(770, 771);
		GlStateManager.disableDepth();
		GlStateManager.depthMask(false);
		GlStateManager.disableTexture();
		GL11.glEnable(2848);
		GL11.glLineWidth(2.0f);
        GlStateManager.color(Color.CYAN.getRed(), Color.CYAN.getGreen(), Color.CYAN.getBlue(), Color.CYAN.getAlpha());
        GL11.glBegin(3);
		for (double[] position : positions) {
			double x = position[0] - Wrapper.getMinecraft().getRenderManager().renderPosX;
			double y = position[1] - Wrapper.getPlayer().height - Wrapper.getMinecraft().getRenderManager().renderPosY;
			double z = position[2] - Wrapper.getMinecraft().getRenderManager().renderPosZ;
			GL11.glVertex3d(x, y, z);
		}
		GL11.glEnd();
		GL11.glDisable(2848);
		GlStateManager.enableTexture();
		GlStateManager.depthMask(true);
		GlStateManager.enableDepth();
		GlStateManager.disableBlend();
		GlStateManager.enableLighting();
		GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
		RenderHelper.disableStandardItemLighting();
		GlStateManager.popMatrix();
	}

	@AcceptedPackets(packets = { C0BPacketEntityAction.class, C03PacketPlayer.class, C07PacketPlayerDigging.class,
			C08PacketPlayerBlockPlacement.class, C09PacketHeldItemChange.class })
	@Handler(priority = 500, filters = { @Filter(type = PacketFilter.class) })
	protected void onPacketSend(final PacketSendEvent event) {
		boolean add = true;

		event.setCancelled(true);
		if (Wrapper.getPlayer().motionX == 0 && Wrapper.getPlayer().motionZ == 0) {
			add = false;
		} else {
			add = true;
			positions.add(new double[] { Wrapper.getPlayer().posX, Wrapper.getPlayer().posY, Wrapper.getPlayer().posZ });
		}

		if (add)
			packets.add(event.getPacket());
	}
}
