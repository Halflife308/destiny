package me.jamie.destiny.impl.module.modules.movement;

import me.jamie.destiny.impl.events.motion.PostStepEvent;
import me.jamie.destiny.impl.events.motion.PreStepEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;

public class Step extends Module
{
	private Property<Float> height;
	
	public Step() {
		super("Step");
	
		setCategory(Category.Movement); 
		setColor(0xFFF7DA34);
		setVisible(true);
	}
	
	@Override
	public void loadProperties() {
		this.properties.add(height = new Property<Float>("Step Height", "Height", (Float)1.0F, (Float)0.0F, (Float)10.0F, (Float)0.1F, new String[] { "height" }));
	}
	
	@Handler
	protected void onPreStep(final PreStepEvent event) {
		Wrapper.getPlayer().stepHeight = canStep() ? height.getFloatProperty() : 0.5F;
	}
	
	@Handler
	protected void onPostStep(final PostStepEvent event) {
		if(!canStep()) return;
		Wrapper.getPlayer().sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(Wrapper.getPlayer().posX, Wrapper.getPlayer().posY + 0.42, Wrapper.getPlayer().posZ, Wrapper.getPlayer().onGround));
		Wrapper.getPlayer().sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(Wrapper.getPlayer().posX, Wrapper.getPlayer().posY + 0.75, Wrapper.getPlayer().posZ, Wrapper.getPlayer().onGround));
	}
	
	private boolean canStep() {
		return !Wrapper.getGameSettings().keyBindJump.isPressed()
				&& Wrapper.getPlayer().fallDistance < 0.2F
				&& !Wrapper.getPlayer().isInWater()
				&& !Wrapper.getPlayer().isInsideOfMaterial(Material.lava)
				&& !Wrapper.getPlayer().isOnLadder()
				&& !Wrapper.getWorld().getCollidingBoundingBoxes(Wrapper.getPlayer(),
                        Wrapper.getPlayer().getEntityBoundingBox().addCoord(0.0, -0.1, 0.0)).isEmpty();
	}
}
