package me.jamie.destiny.impl.module.modules.movement;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.impl.events.packet.PacketSendEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.filter.Filter;
import me.jamie.lib.event.filter.filters.AcceptedPackets;
import me.jamie.lib.event.filter.filters.PacketFilter;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.network.play.client.C02PacketUseEntity;
import net.minecraft.network.play.client.C03PacketPlayer;

/**
 * 
 * @author Jamie
 * @date 11/8/16
 * Stops player from taking fall damage on vanilla.
 */
public class NoFall extends Module
{
	public NoFall() 
	{
		super("No Fall");
		
		setBind(Keyboard.KEY_N);
		setCategory(Category.Movement);
		setColor(0xFF7EF2B7);
		setVisible(true);
	}
	
	@AcceptedPackets(packets={C03PacketPlayer.class })
	@Handler(priority = 500, filters={@Filter(type=PacketFilter.class)})
	protected void onPacketSend(final PacketSendEvent event) {
		event.setCancelled(Wrapper.getPlayer().fallDistance > 3.0F);
	}
	
}
