package me.jamie.destiny.impl.module.modules.movement;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.impl.events.motion.MoveEntityEvent;
import me.jamie.destiny.impl.events.motion.UpdateEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.PlayerUtils;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.util.MathHelper;

public class Flight extends Module
{
	private Property<Boolean> damage;
	private Property<Float> speed;
	private Property<String> mode;
	
	private boolean flying;
	
	public Flight() {
		super("Flight", new String[] { "flight", "fly" });
		
		setBind(Keyboard.KEY_F);
		setCategory(Category.Movement);
		setColor(0xFF51BBFC);
		setVisible(true);
	}
	
	@Override
	public void loadProperties() 
	{
		this.properties.add(mode = new Property<String>("Flight Mode", "Mode", "Normal", new String[] { "mode" }) {
			public void addProperties() {
				getProperties().add("Normal");
				getProperties().add("Slowfall");
				getProperties().add("AAC");
			}
		});
		
		this.properties.add(damage = new Property<Boolean>("Flight Damage", "Damage", true, new String[] { "damage" }));
		
		this.properties.add(speed = new Property<Float>("Flight Speed", "Speed", (Float)1.0F, (Float)0.0F, (Float)10.0F, (Float)0.1F, new String[] { "speed" }));
	}
	
	@Override
	public void onEnable() {
		super.onEnable();
		
		flying = Wrapper.getPlayer().capabilities.isFlying;
		if(Wrapper.getWorld() == null) return;
		if(damage.getProperty())
			PlayerUtils.damage(1);
	}
	
	@Override
	public void onDisable() {
		super.onDisable();
		Wrapper.getPlayer().capabilities.isFlying = flying;
	}
	
	@Handler
	protected void onUpdate(final UpdateEvent event) {
		setExtension("�7" + mode.getCurrentProperty());
	}
	
	@Handler
	protected void onMoveEntity(final MoveEntityEvent event) 
	{	
		switch(mode.getCurrentProperty()) 
		{
			case "Normal": {
				Wrapper.getPlayer().capabilities.isFlying = true;
				
				float forward = Wrapper.getPlayer().moveForward;
				float strafe = Wrapper.getPlayer().moveStrafing;
				float sin = MathHelper.sin(Wrapper.getPlayer().rotationYaw * (float)Math.PI / 180.0F);
				float cos = MathHelper.cos(Wrapper.getPlayer().rotationYaw * (float)Math.PI / 180.0F);

				event.setX(event.getX() + (strafe * cos - forward * sin) * (speed.getFloatProperty() / 10));
				event.setZ(event.getZ() + (forward * cos + strafe * sin) * (speed.getFloatProperty() / 10));
				
				break;
			}
			case "Slowfall":
			case "AAC": {
				event.setY(-0.02D);
				break;
			}
		}
		
		if(Wrapper.getPlayer().movementInput.jump)
			event.setY(event.getY() + (mode.getCurrentProperty().equals("Slowfall") ? 0.05 : 0.3));
		else if(Wrapper.getPlayer().movementInput.sneak)
			event.setY(event.getY() - (mode.getCurrentProperty().equals("Slowfall") ? 0.05 : 0.3));
	}
}
