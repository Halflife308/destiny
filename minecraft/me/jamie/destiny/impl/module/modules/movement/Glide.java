package me.jamie.destiny.impl.module.modules.movement;

import me.jamie.destiny.impl.events.motion.MoveEntityEvent;
import me.jamie.destiny.impl.events.packet.PacketSendEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.PlayerUtils;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.filter.Filter;
import me.jamie.lib.event.filter.filters.AcceptedPackets;
import me.jamie.lib.event.filter.filters.PacketFilter;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.network.play.client.C03PacketPlayer;

public class Glide extends Module 
{
	private Property<Boolean> damage;
	private Property<Float> speed;
	
	public Glide() {
		super("Glide", new String[] { "glide" });
		
		setCategory(Category.Movement);
		setColor(0xFF1FA7FB);
		setVisible(true);
	}
	
	@Override
	public void loadProperties() {
		this.properties.add(damage = new Property<Boolean>("Glide Damage", "Damage", true, new String[] { "damage" }));
		this.properties.add(speed = new Property<Float>("Glide Speed", "Speed", (Float)0.05F, (Float)0.0F, (Float)1.0F, (Float)0.01F, new String[] { "speed" }));
	}
	
	@Override
	public void onEnable() {
		super.onEnable();

		if(Wrapper.getWorld() == null) return;
		if(damage.getProperty())
			PlayerUtils.damage(1);
	}
	
	@Handler
	protected void onMoveEntity(final MoveEntityEvent event) {
		if(Wrapper.getPlayer().fallDistance > 0 && !Wrapper.getPlayer().isCollidedHorizontally)
			event.setY(-0.01);
		System.out.println(Wrapper.getPlayer().rotationYaw);
	}
	
	@AcceptedPackets(packets={ C03PacketPlayer.class })
	@Handler(priority = 500, filters={@Filter(type=PacketFilter.class)})
	protected void onPacketSend(final PacketSendEvent event) 
	{
		
	}
}
