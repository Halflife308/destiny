package me.jamie.destiny.impl.module.modules.movement;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.impl.events.block.BoundingBoxEvent;
import me.jamie.destiny.impl.events.motion.UpdateEvent;
import me.jamie.destiny.impl.events.packet.PacketSendEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.util.game.PlayerUtils;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.filter.Filter;
import me.jamie.lib.event.filter.filters.AcceptedPackets;
import me.jamie.lib.event.filter.filters.PacketFilter;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.AxisAlignedBB;

public class Jesus extends Module
{
	private boolean next;
		
	public Jesus() {
		super("Jesus", new String[] { "jesus", "waterwalk" });
	
		setBind(Keyboard.KEY_J);
		setCategory(Category.Player);
		setColor(0xFF33F1FF);
		setVisible(true);
	}
	
	@Handler
	protected void onBoundingBox(final BoundingBoxEvent event) {
		if(Wrapper.getPlayer().fallDistance < 3.0F && !Wrapper.getPlayer().isSneaking() && Wrapper.getPlayer() != null && Wrapper.getWorld() != null) 
			event.setAABB(new AxisAlignedBB(event.getX(), event.getY(), event.getZ(), event.getX() + 1, event.getY() + 1, event.getZ() + 1));
		else
			event.setAABB(null);
	}
	
	@Handler
	protected void onUpdate(final UpdateEvent event) {
		if(PlayerUtils.isOnLiquid())
			Wrapper.getPlayer().movementInput.jump = false;
	}
	
	@AcceptedPackets(packets={ C03PacketPlayer.class })
	@Handler(priority = 500, filters={@Filter(type=PacketFilter.class)})
	protected void onPacketSend(final PacketSendEvent event) {
		final C03PacketPlayer packet = (C03PacketPlayer)event.getPacket();
		
		next = !next;
		if(next && PlayerUtils.isOnLiquid())
			packet.setY(packet.getY() - 0.01D);
		
		if(PlayerUtils.isInLiquid() && !Wrapper.getPlayer().isSneaking())
			Wrapper.getPlayer().motionY = 0.08D;
	}
}
