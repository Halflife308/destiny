package me.jamie.destiny.impl.module.modules.combat;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.impl.events.packet.PacketSendEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.filter.Filter;
import me.jamie.lib.event.filter.filters.AcceptedPackets;
import me.jamie.lib.event.filter.filters.PacketFilter;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.entity.Entity;
import net.minecraft.network.play.client.C02PacketUseEntity;
import net.minecraft.network.play.client.C02PacketUseEntity.Action;
import net.minecraft.util.MathHelper;
import net.minecraft.network.play.client.C03PacketPlayer;

public class Criticals extends Module
{

	public Criticals() {
		super("Criticals", new String[] { "criticals", "crits" });
		
		setBind(Keyboard.KEY_C);
		setCategory(Category.Combat);
		setColor(0xFFA898ED);
		setVisible(true);
	}

	@AcceptedPackets(packets={ C02PacketUseEntity.class })
	@Handler(priority = 500, filters={@Filter(type=PacketFilter.class)})
	protected void onPacketSend(final PacketSendEvent event) 
	{
		int cooldown = 0;
		if(event.getPacket() instanceof C02PacketUseEntity) {
			final C02PacketUseEntity action = (C02PacketUseEntity)event.getPacket();
			final Entity target = action.getEntityFromWorld(Wrapper.getWorld());
			
			if(!action.getAction().equals(Action.ATTACK)) return;
			if(Wrapper.getPlayer().getDistanceToEntity(target) > 5) return;
			event.sendPacket(new C03PacketPlayer.C04PacketPlayerPosition(Wrapper.getPlayer().posX, Wrapper.getPlayer().posY + 0.05, Wrapper.getPlayer().posZ, false));
			event.sendPacket(new C03PacketPlayer.C04PacketPlayerPosition(Wrapper.getPlayer().posX, Wrapper.getPlayer().posY, Wrapper.getPlayer().posZ, false));
			event.sendPacket(new C03PacketPlayer.C04PacketPlayerPosition(Wrapper.getPlayer().posX, Wrapper.getPlayer().posY + 0.012511, Wrapper.getPlayer().posZ, false));
			event.sendPacket(new C03PacketPlayer.C04PacketPlayerPosition(Wrapper.getPlayer().posX, Wrapper.getPlayer().posY, Wrapper.getPlayer().posZ, false));
		}
	}
}
