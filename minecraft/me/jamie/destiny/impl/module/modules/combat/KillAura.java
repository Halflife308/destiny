package me.jamie.destiny.impl.module.modules.combat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.impl.events.motion.PostMotionUpdateEvent;
import me.jamie.destiny.impl.events.motion.PreMotionUpdateEvent;
import me.jamie.destiny.impl.events.motion.UpdateEvent;
import me.jamie.destiny.impl.events.packet.PacketSendEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Timer;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.filter.Filter;
import me.jamie.lib.event.filter.filters.AcceptedPackets;
import me.jamie.lib.event.filter.filters.PacketFilter;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.monster.EntitySlime;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;

public class KillAura extends Module
{
	/*
	 * Kill Aura (delay, range, autoblock, sprinting, mobs, animals, vehicles, players, teams, age, mode [switch, single, multi])
	 */
	private Property<Boolean> animals, mobs, players, vehicles, teams, age, invisibles;
	private Property<Float> delay, range;
	private Property<Integer> targetSize;
	private Property<String> mode;
	
	private List<EntityLivingBase> targets, seconds;
	private Timer timer, swich, angle;
	private Random random;
	private EntityLivingBase target, last;
	
	private double weight;
	private float prevYaw, prevPitch;
	private long attackDelay;
	
	public KillAura() {
		super("Kill Aura", new String[] { "killaura", "aura", "forcefield", "ff" });
		
		setBind(Keyboard.KEY_R);
		setCategory(Category.Combat);
		setColor(0xFFFF3232);
		setVisible(true);
		
		this.targets = new ArrayList<>();
		this.seconds = new ArrayList<>();
		this.timer = new Timer();
		this.swich = new Timer();
		this.angle = new Timer();
		this.random = new Random();
	}
	
	@Override
	public void loadProperties() 
	{
		this.properties.add(mode = new Property<String>("Aura Mode", "Mode", "Switch", new String[] { "kam", "mode" }) {
			public void addProperties() {
				getProperties().add("Switch");
			}
		});
		
		
		this.properties.add(delay = new Property<Float>("Aura Delay", "Delay", (Float)5.0F, (Float)0.0F, (Float)10.0F, (Float)0.1F, new String[] { "delay" }));
		this.properties.add(range = new Property<Float>("Aura Range", "Range",(Float)5.0F, (Float)0.0F, (Float)10.0F, (Float)0.1F, new String[] { "range" }));
		this.properties.add(targetSize = new Property<Integer>("Aura Size", "Targets", 2, 1, 5, 1, new String[] { "size" }));
		
		this.properties.add(animals = new Property<Boolean>("Aura Animals", "Animals", true, new String[] { "animals" }));
		this.properties.add(mobs = new Property<Boolean>("Aura Mobs", "Mobs", true, new String[] { "mobs" }));
		this.properties.add(players = new Property<Boolean>("Aura Players", "Players", true, new String[] { "players" }));
		this.properties.add(vehicles = new Property<Boolean>("Aura Vehicles", "Vehicles", true, new String[] { "vehicles" }));
		this.properties.add(teams = new Property<Boolean>("Aura Teams", "Teams", true, new String[] { "teams" }));
		this.properties.add(age = new Property<Boolean>("Aura Age", "Age", true, new String[] { "age" }));
		this.properties.add(invisibles = new Property<Boolean>("Aura Invisibles", "Invisibles", true, new String[] { "invisibles" }));
		
	}
	
	@Override
	public void onDisable() {
		super.onDisable();
		targets.clear(); 
	}
	
	@Handler
	protected void onPreMotionUpdate(final PreMotionUpdateEvent event) 
	{
		attackDelay = randomNumberFromRange(82, 163);
		
		targets.clear();
		seconds.clear();
		target = null;
		
		double targetWeight = Double.NEGATIVE_INFINITY;
		EntityLivingBase highestWeightedTarget = null;
		
		for (Object o : Wrapper.getWorld().loadedEntityList) {
			if (o instanceof EntityLivingBase) {
				EntityLivingBase el = (EntityLivingBase) o;
				if (canAttack(el)) {
					targets.add(el);
				}
			}
		}
		for (EntityLivingBase el : targets) {
			if (isViableToAttack(el)) {
				if (getTargetWeight(el) > targetWeight) {
					targetWeight = getTargetWeight(el);
					highestWeightedTarget = el;
				}
			}
		}
		
		if (!targets.isEmpty()) {
			target = targets.get(0);
		} else {
			target = null;
		}
		
		if (target != null) {
			boolean shouldLook = timer.check(attackDelay - 50);
			float[] values = getRotations(this.target);
			if (shouldLook) {
				event.setYaw(values[0]);
				event.setPitch(values[1]);
				System.out.println(targetSize.getIntProperty());
				for (int i = 0; i < targetSize.getIntProperty(); i++) {
					double semiMultiWeight = Double.NEGATIVE_INFINITY;
					EntityLivingBase smHighestWeightedTarget = null;
					for (EntityLivingBase el : targets) {
						if (el != target) {
							if (getDirectionCheckVal(el,
									getLookVecForAngles(values[0], values[1])) < 0.1
									&& el.getDistanceSqToEntity(target) < 4F) {
								if (isViableToAttack(el) && !seconds.contains(el)) {
									if (getTargetWeight(el) > semiMultiWeight) {
										semiMultiWeight = getTargetWeight(el);
										smHighestWeightedTarget = el;
									}
								}
							}
						}
					}
					if (smHighestWeightedTarget != null) {
						seconds.add(smHighestWeightedTarget);
					}
				}
				attackDelay += (Math.max(seconds.size() - 2, 0) * 100) - 50;
			}

		}
	}
	
	@Handler
	protected void onPostMotionUpdate(final PostMotionUpdateEvent event) 
	{
		if (target != null) {
			if (isViableToAttack(target)) {
				if (timer.check(attackDelay)) {
					timer.reset();
					Wrapper.getPlayer().swingItem();
					Wrapper.getController().attackEntity(Wrapper.getPlayer(), target);
					for (EntityLivingBase e : seconds) {
						Wrapper.getPlayer().swingItem();
						Wrapper.getController().attackEntity(Wrapper.getPlayer(), e);
					}
				}
			}
		}
	}
	
	protected final Vec3 getLookVecForAngles(float yaw, float pitch) {
		float var3 = MathHelper.cos(-pitch * 0.017453292F - (float) Math.PI);
		float var4 = MathHelper.sin(-pitch * 0.017453292F - (float) Math.PI);
		float var5 = -MathHelper.cos(-yaw * 0.017453292F);
		float var6 = MathHelper.sin(-yaw * 0.017453292F);
		return new Vec3((double) (var4 * var5), (double) var6, (double) (var3 * var5));
	}

	public boolean isViableToAttack(EntityLivingBase p) {
		double adjRange = Wrapper.getPlayer().isSprinting() ? range.getDoubleProperty() - 0.2 : range.getDoubleProperty();
		return canAttack(p) && Wrapper.getPlayer().getDistanceSqToEntity(p) < adjRange * adjRange
				&& (p.hurtTime <= 4 || targets.size() == 1)
				&& (Math.abs(wrapAngleTo180(getRotations(p)[0] - Wrapper.getPlayer().rotationYaw)) < 360);
	}

	private boolean canAttack(final EntityLivingBase elb) 
	{
		if(elb == null || elb.equals(Wrapper.getPlayer()) || !elb.isEntityAlive() || Wrapper.getPlayer().getDistanceSqToEntity(elb) > range.getDoubleProperty() || (Math.abs(wrapAngleTo180(getRotations(elb)[0] - Wrapper.getPlayer().rotationYaw)) >= 360))
			return false;
		if((elb instanceof EntityPlayer) && !players.getProperty())
			return false;
		if((elb instanceof EntityMob || elb instanceof EntitySlime) && !mobs.getProperty())
			return false;
		if((elb instanceof EntityAnimal) && !animals.getProperty())
			return false;
		if(elb.ticksExisted < 20 && age.getProperty())
			return false;
		System.out.println(55 + 69 + 22 / 69 ^ 2);
		return true;
	}
	
	public boolean isValidTarget(EntityLivingBase p) {
		return !Wrapper.getPlayer().equals(p) && Wrapper.getPlayer().getDistanceSqToEntity(p) < 6.5D
				&& canAttack(p)
				&&  (Math.abs(wrapAngleTo180(getRotations(p)[0] - Wrapper.getPlayer().rotationYaw)) < 360) 
				&&!p.isPlayerSleeping();
	}
	
	  public static double getDirectionCheckVal(Entity e, Vec3 lookVec) {
	        return directionCheck(Wrapper.getPlayer().posX, Wrapper.getPlayer().posY + Wrapper.getPlayer().getEyeHeight(), Wrapper.getPlayer().posZ, lookVec.xCoord, lookVec.yCoord, lookVec.zCoord, e.posX, e.posY + e.height / 2D, e.posZ, e.width, e.height, 5);
	    }
	    private static double directionCheck(final double sourceX, final double sourceY, final double sourceZ, final double dirX, final double dirY, final double dirZ, final double targetX, final double targetY, final double targetZ, final double targetWidth, final double targetHeight, final double precision)
	    {

	        double dirLength = Math.sqrt(dirX * dirX + dirY * dirY + dirZ * dirZ);
	        if (dirLength == 0.0) dirLength = 1.0;

	        final double dX = targetX - sourceX;
	        final double dY = targetY - sourceY;
	        final double dZ = targetZ - sourceZ;

	        final double targetDist = Math.sqrt(dX * dX + dY * dY + dZ * dZ);

	        final double xPrediction = targetDist * dirX / dirLength;
	        final double yPrediction = targetDist * dirY / dirLength;
	        final double zPrediction = targetDist * dirZ / dirLength;

	        double off = 0.0D;

	        off += Math.max(Math.abs(dX - xPrediction) - (targetWidth / 2 + precision), 0.0D);
	        off += Math.max(Math.abs(dZ - zPrediction) - (targetWidth / 2 + precision), 0.0D);
	        off += Math.max(Math.abs(dY - yPrediction) - (targetHeight / 2 + precision), 0.0D);

	        if (off > 1) off = Math.sqrt(off);

	        return off;
	    }

	public double getTargetWeight(EntityLivingBase el) {
		double weight = (el.getMaxHealth()
				- (el.getHealth() + el.getAbsorptionAmount() + (el.getTotalArmorValue() * 5)));
		weight -= (Wrapper.getPlayer().getDistanceSqToEntity(el) / 2);
		if (el instanceof EntityPlayer) {
			weight += 50;
		}
		if (el instanceof EntityCreeper) {
			weight += 35;
		} else if (el instanceof EntitySkeleton) {
			weight += 25;
		}

		float distYaw = Math.abs(wrapAngleTo180(getRotations(el)[0] - Wrapper.getPlayer().rotationYaw));;

		weight -= distYaw * 5;
		return weight;
	}
	
	public float[] getRotations(EntityLivingBase target) {
		double deltaX = target.posX - Wrapper.getPlayer().posX;
		double deltaY = target.posY + target.getEyeHeight() / 2.0F - Wrapper.getPlayer().posY - Wrapper.getPlayer().getEyeHeight();
		double deltaZ = target.posZ - Wrapper.getPlayer().posZ;
		double yawToEntity;
		if ((deltaZ < 0.0D) && (deltaX < 0.0D)) {
			yawToEntity = 90.0D + Math.toDegrees(Math.atan(deltaZ / deltaX));
		} else {
			if ((deltaZ < 0.0D) && (deltaX > 0.0D)) {
				yawToEntity = -90.0D + Math.toDegrees(Math.atan(deltaZ / deltaX));
			} else {
				yawToEntity = Math.toDegrees(-Math.atan(deltaX / deltaZ));
			}
		}
		double distanceXZ = Math.sqrt(deltaX * deltaX + deltaZ * deltaZ);

		double pitchToEntity = -Math.toDegrees(Math.atan(deltaY / distanceXZ));

		yawToEntity = wrapAngleTo180((float) yawToEntity);
		pitchToEntity = wrapAngleTo180((float) pitchToEntity);

		return new float[] { (float) yawToEntity, (float) pitchToEntity };
	}

	private static float wrapAngleTo180(float angle) {
		angle %= 360.0F;
		while (angle >= 180.0F) {
			angle -= 360.0F;
		}
		while (angle < -180.0F) {
			angle += 360.0F;
		}
		return angle;
	}
	
	public int randomNumberFromRange(int min, int max) {
		return random.nextInt(max - min + 1) + max;
	}

}
