package me.jamie.destiny.impl.module.modules.combat;

import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.util.game.Timer;

public class Triggerbot extends Module
{
	private Timer timer;
	
	public Triggerbot() {
		super("Trigger Bot", new String[] { "triggerbot", "trigger", "autoclick" });
		
		setCategory(Category.Combat);
		setColor(0xFF3FEC53);
		
		this.timer = new Timer();
	}	
}
