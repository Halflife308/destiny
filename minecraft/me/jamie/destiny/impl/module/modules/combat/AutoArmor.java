package me.jamie.destiny.impl.module.modules.combat;

import me.jamie.destiny.impl.events.motion.UpdateEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Timer;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class AutoArmor extends Module
{
	private Timer timer;
	
	private Property<Float> delay;
	
	public AutoArmor() {
		super("Auto Armor", new String[] { "autoarmor" });
		
		setCategory(Category.Combat);
		setColor(0xFF8FF3C3);
		setVisible(true);
		
		this.timer = new Timer();
	}
	
	@Override
	public void loadProperties() {
		this.properties.add(delay = new Property<Float>("AutoArmor Delay", "Delay", (Float)5.0F, (Float)0.0F, (Float)10.0F, (Float)0.1F, new String[] { "delay" }));
	}
	
	@Handler
	protected void onUpdate(final UpdateEvent event) 
	{
		if(Wrapper.getMinecraft().currentScreen instanceof GuiInventory) return;
		if(timer.check(delay.getFloatProperty())) {
			int best = 0, slot = -1;
			for(int i = 9; i < 45; i++) {
				final ItemStack stack = Wrapper.getPlayer().inventoryContainer.getSlot(i).getStack();
				if(stack == null || (!(stack.getItem() instanceof ItemArmor))) continue;
				final ItemArmor armor = (ItemArmor)stack.getItem();
				if(hasHelmet(armor) || hasChestplate(armor) || hasLeggings(armor) || hasBoots(armor)) {
					if(getArmorStrength(stack) > best) {
						best = getArmorStrength(stack);
						slot = i;
					}
				}
			}
			if(slot != -1) {
				Wrapper.getController().windowClick(0, slot, 0, 1, Wrapper.getPlayer());
			}
			timer.reset();
		}
	}
	
	private boolean hasHelmet(ItemArmor armor) {
		return armor.armorType == 0 && Wrapper.getPlayer().inventory.armorInventory[3] == null;
	}
	
	private boolean hasChestplate(ItemArmor armor) {
		return armor.armorType == 1 && Wrapper.getPlayer().inventory.armorInventory[2] == null;
	}
	
	private boolean hasLeggings(ItemArmor armor) {
		return armor.armorType == 2 && Wrapper.getPlayer().inventory.armorInventory[1] == null;
	}
	
	private boolean hasBoots(ItemArmor armor) {
		return armor.armorType == 3 && Wrapper.getPlayer().inventory.armorInventory[0] == null;
	}
	
	private int getArmorStrength(final ItemStack stack) {
		if(stack == null || (!(stack.getItem() instanceof ItemArmor))) return 0;
		final ItemArmor armor = (ItemArmor)stack.getItem();
		return armor.damageReduceAmount + EnchantmentHelper.getEnchantmentLevel(Enchantment.field_180310_c.effectId, stack);
	}
}
