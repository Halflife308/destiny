package me.jamie.destiny.impl.module.modules.combat;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.impl.events.motion.PostMotionUpdateEvent;
import me.jamie.destiny.impl.events.motion.PreMotionUpdateEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Timer;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.monster.EntitySlime;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;

public class TestAura extends Module
{
	/*
	 * Kill Aura (delay, range, autoblock, sprinting, mobs, animals, vehicles, players, teams, age, mode [switch, single, multi])
	 */
	private Property<Boolean> animals, mobs, players, vehicles, teams, age, invisibles;
	private Property<Float> delay, range;
	private Property<String> mode;
	
	private List<EntityLivingBase> targets, seconds;
	private Timer timer, swich, angle;
	private Random random;
	private EntityLivingBase target, last;
	
	private float prevYaw, prevPitch;
	private long attackDelay;
	
	public TestAura() {
		super("Test Aura", new String[] { "testar", "arra", "fft", "tff" });
		
		setBind(Keyboard.KEY_R);
		setCategory(Category.Combat);
		setColor(0xFFFF3232);
		setVisible(true);
		
		this.targets = new ArrayList<>();
		this.seconds = new ArrayList<>();
		this.timer = new Timer();
		this.swich = new Timer();
		this.angle = new Timer();
		this.random = new Random();
	}
	
	@Override
	public void loadProperties() 
	{
		this.properties.add(mode = new Property<String>("Test Aura Mode", "Mode", "Switch", new String[] { "kam", "mode" }) {
			public void addProperties() {
				getProperties().add("Switch");
				getProperties().add("Multi");
				getProperties().add("Poop");
				getProperties().add("Jasdas");
			}
		});
		
		this.properties.add(animals = new Property<Boolean>("Test Aura Animals", "Animals", true, new String[] { "animals" }));
		this.properties.add(mobs = new Property<Boolean>("Test Aura Mobs", "Mobs", true, new String[] { "mobs" }));
		this.properties.add(players = new Property<Boolean>("Test Aura Players", "Players", true, new String[] { "players" }));
		this.properties.add(vehicles = new Property<Boolean>("Test Aura Vehicles", "Vehicles", true, new String[] { "vehicles" }));
		this.properties.add(teams = new Property<Boolean>("Test Aura Teams", "Teams", true, new String[] { "teams" }));
		this.properties.add(age = new Property<Boolean>("Test Aura Age", "Age", true, new String[] { "age" }));
		this.properties.add(invisibles = new Property<Boolean>("Test Aura Invisibles", "Invisibles", true, new String[] { "invisibles" }));
		
		this.properties.add(delay = new Property<Float>("Test Aura Delay", "Delay", (Float)5.0F, (Float)0.0F, (Float)10.0F, (Float)0.1F, new String[] { "delay" }));
		this.properties.add(range = new Property<Float>("Test Aura Range", "Range",(Float)5.0F, (Float)0.0F, (Float)10.0F, (Float)0.1F, new String[] { "range" }));
	}
	
	@Handler
	protected void onPreMotionUpdate(final PreMotionUpdateEvent event)
	{
		attackDelay = randomNumberFromRange(82, 163);
		double targetWeight = Double.NEGATIVE_INFINITY;
		EntityLivingBase highestWeightedTarget = null;
		targets.clear();
		seconds.clear();
		target = null;
		for (Object o : Wrapper.getWorld().loadedEntityList) {
			if (o instanceof EntityLivingBase) {
				EntityLivingBase el = (EntityLivingBase) o;
				if (isValidTarget(el)) {
					targets.add(el);
				}
			}
		}
		for (EntityLivingBase el : targets) {
			if (isViableToAttack(el)) {
				if (getTargetWeight(el) > targetWeight) {
					targetWeight = getTargetWeight(el);
					highestWeightedTarget = el;
				}
			}
		}
		if (highestWeightedTarget != null) {
			this.target = highestWeightedTarget;
		}
		if (!isValidTarget(target)) {
			target = null;
		}
		if (target != null) {
			boolean shouldLook = timer.check(attackDelay - 50);
			float[] values = getAngles(target);
			if (shouldLook) {
				event.setYaw(values[0]);
				event.setPitch(values[1]);
				for (int i = 0; i < 3; i++) {
					double semiMultiWeight = Double.NEGATIVE_INFINITY;
					EntityLivingBase smHighestWeightedTarget = null;
					for (EntityLivingBase el : targets) {
						if (el != target) {
							if (el.getDistanceSqToEntity(target) < 4F) {
								if (isViableToAttack(el) && !seconds.contains(el)) {
									if (getTargetWeight(el) > semiMultiWeight) {
										semiMultiWeight = getTargetWeight(el);
										smHighestWeightedTarget = el;
									}
								}
							}
						}
					}
					if (smHighestWeightedTarget != null) {
						seconds.add(smHighestWeightedTarget);
					}
				}
				attackDelay += (Math.max(seconds.size() - 2, 0) * 100) - 50;
			}

		}
	}
	
	@Handler
	protected void onPostMotionUpdate(final PostMotionUpdateEvent event) 
	{
		if (target != null) {
			if (isViableToAttack(target)) {
				if (timer.check(1000 / 3.5F)) {
					timer.reset();
					Wrapper.getPlayer().swingItem();
					Wrapper.getController().attackEntity(Wrapper.getPlayer(), target);
					for (EntityLivingBase e : seconds) {
						Wrapper.getPlayer().swingItem();
						Wrapper.getController().attackEntity(Wrapper.getPlayer(), e);
					}
				}
			}
		}
	}

	public boolean isValidTarget(EntityLivingBase p) {
		return !Wrapper.getPlayer().equals(p) && Wrapper.getPlayer().getDistanceSqToEntity(p) < 5.5D * 5.5D
				&& p.isEntityAlive() && (!(p instanceof EntityPlayer) || p.ticksExisted > 20 * 2) && isInTargetMode(p)
				&& !p.isPlayerSleeping();
	}

	public boolean isViableToAttack(EntityLivingBase p) {
		double adjRange = Wrapper.getPlayer().isSprinting() ? range.getFloatProperty() - 0.2 : range.getFloatProperty();

		float distYaw = Math.abs(getYawChangeToEntity(p));
		float distPitch = Math.abs(getPitchChangeToEntity(p));

		float distCombined = distYaw + distPitch;
		double maxAngle = 90;
		boolean angleCheck = distCombined < maxAngle;
		angleCheck = angleCheck || angle.check(targets.size() <= 5 ? 30 : 60);
		if (angleCheck) {
			angle.reset();
		}
		return isValidTarget(p) && Wrapper.getPlayer().getDistanceSqToEntity(p) < adjRange * adjRange
				&& (p.hurtTime <= 4 || targets.size() == 1);
		// (angleCheck || targets.size() <= 1 || !angle_Check.getValue());
	}

	public boolean isInTargetMode(EntityLivingBase el) {
		boolean mobChecks = false;
		boolean playerChecks;
		boolean animalChecks;
		playerChecks = el instanceof EntityPlayer;
		if (el instanceof EntityMob) {
			mobChecks = true;
		} else if (el instanceof EntityWolf) {
			EntityWolf wolf = (EntityWolf) el;
			mobChecks = wolf.isAngry();
		} else if (el instanceof EntitySlime) {
			mobChecks = true;
		}
		animalChecks = el instanceof EntityAnimal;
		if (playerChecks) {
			return players.getProperty();
		}
		if (mobChecks) {
			return mobs.getProperty();
		}
		if (animalChecks) {
			return animals.getProperty();
		}
		return false;
	}

	public double getTargetWeight(EntityLivingBase el) {
		double weight = (el.getMaxHealth()
				- (el.getHealth() + el.getAbsorptionAmount() + (el.getTotalArmorValue() * 5)));
		weight -= (Wrapper.getPlayer().getDistanceSqToEntity(el) / 2);
		if (el instanceof EntityPlayer) {
			weight += 50;
		}
		if (el instanceof EntityCreeper) {
			weight += 35;
		} else if (el instanceof EntitySkeleton) {
			weight += 25;
		}

		float distYaw = Math.abs(getYawChangeToEntity(el));
		float distPitch = Math.abs(getPitchChangeToEntity(el));

		float distCombined = distYaw + distPitch;

		weight -= distCombined;
		return weight;
	}

	public static float getYawChangeToEntity(Entity entity) {
		double deltaX = entity.posX - Wrapper.getPlayer().posX;
		double deltaZ = entity.posZ - Wrapper.getPlayer().posZ;
		double yawToEntity; // tangent degree to entity

		if (deltaZ < 0 && deltaX < 0) { // quadrant 3
			yawToEntity = 90D + Math.toDegrees(Math.atan(deltaZ / deltaX)); // 90
			// degrees
			// forward
		} else if (deltaZ < 0 && deltaX > 0) { // quadrant 4
			yawToEntity = -90D + Math.toDegrees(Math.atan(deltaZ / deltaX)); // 90
			// degrees
			// back
		} else { // quadrants one or two
			yawToEntity = Math.toDegrees(-Math.atan(deltaX / deltaZ));
		}

		return MathHelper.wrapAngleTo180_float(-(Wrapper.getPlayer().rotationYaw - (float) yawToEntity)); // delta
		// between
		// desired
		// yaw
		// and
		// player
		// yaw
	}
	
	private float[] getRotations(EntityLivingBase target) {
        final double var4 = target.posX - Wrapper.getPlayer().posX;
        final double var6 = target.posZ - Wrapper.getPlayer().posZ;
        final double var8 = target.posY + target.getEyeHeight() / 1.3 - (Wrapper.getPlayer().posY + Wrapper.getPlayer().getEyeHeight());
        final double var14 = MathHelper.sqrt_double(var4 * var4 + var6 * var6);
        final float yaw = (float) (Math.atan2(var6, var4) * 180.0D / Math.PI) - 90.0F;
        final float pitch = (float) -(Math.atan2(var8, var14) * 180.0D / Math.PI);
        return new float[]{ yaw, pitch };
    }

	public static float getPitchChangeToEntity(Entity entity) {
		double deltaX = entity.posX - Wrapper.getPlayer().posX;
		double deltaZ = entity.posZ - Wrapper.getPlayer().posZ;
		double deltaY = entity.posY - 1.6 + entity.getEyeHeight() - Wrapper.getPlayer().posY;
		double distanceXZ = MathHelper.sqrt_double(deltaX * deltaX + deltaZ * deltaZ); // distance
																						// away
																						// for
																						// calculating
																						// pitch
		double pitchToEntity = -Math.toDegrees(Math.atan(deltaY / distanceXZ)); // tangent
		// degree
		// to
		// entity

		return -MathHelper.wrapAngleTo180_float((Wrapper.getPlayer().rotationPitch - (float) pitchToEntity)); // topkek
																										// 90
																										// degrees
	}
	
	 public static float[] getAngles(Entity e) {
	        return new float[] {
	                getYawChangeToEntity(e) + Wrapper.getPlayer().rotationYaw,
	                getPitchChangeToEntity(e) + Wrapper.getPlayer().rotationPitch
	        };
	    }
	
	public int randomNumberFromRange(int min, int max) {
		return random.nextInt(max - min + 1) + max;
	}
	
}
