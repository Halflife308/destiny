package me.jamie.destiny.impl.module.modules.combat;

import me.jamie.destiny.impl.events.motion.UpdateEvent;
import me.jamie.destiny.impl.events.packet.PacketReceiveEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.lib.event.filter.filters.AcceptedPackets;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.network.play.server.S12PacketEntityVelocity;

public class Velocity extends Module
{
	private Property<Float> percent;
	
	public Velocity() {
		super("Velocity", new String[] { "velocity", "antivelocity", "noknockback", "antiknockback" });
		
		setCategory(Category.Combat);
		setColor(0xFF696AF0);
		setVisible(true);
	}
	
	@Override
	public void loadProperties() {
		this.properties.add(percent = new Property<Float>("Velocity Percent", "Percent",(Float)0.0F, (Float)0.0F, (Float)100.0F, (Float)10.0F, new String[] { "velocitypercent", "kbp", "vp" }));
	}
	
	@Handler
	protected void onUpdate(final UpdateEvent event) {
		if(percent.getFloatProperty() > 0) {
			setExtension(String.format("%s", "�7" + (int)percent.getFloatProperty() + "%"));
			setLabel("Velocity");
		} else {
			setLabel("Anti Velocity");
			setExtension(null);
		}
	}
	
	@AcceptedPackets(packets={ S12PacketEntityVelocity.class })
	@Handler
	protected void onPacketReceive(final PacketReceiveEvent event) {
		if(event.getPacket() instanceof S12PacketEntityVelocity) {
			final S12PacketEntityVelocity velocity = (S12PacketEntityVelocity)event.getPacket();
			event.setCancelled(percent.getFloatProperty() == 0);
			velocity.setX((int) (velocity.func_149411_d() * percent.getFloatProperty() / 100));
			velocity.setY((int) (velocity.func_149410_e() * percent.getFloatProperty() / 100));
			velocity.setZ((int) (velocity.func_149409_f() * percent.getFloatProperty() / 100));
		}
	}
	
}
