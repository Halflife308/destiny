package me.jamie.destiny.impl.module.modules.combat;

import me.jamie.destiny.impl.events.motion.UpdateEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Timer;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.init.Items;
import net.minecraft.item.ItemSoup;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C09PacketHeldItemChange;

public class AutoSoup extends Module
{
private Timer timer;
	
	private Property<Float> delay, health;
	
	public AutoSoup() {
		super("Auto Soup", new String[] { "autosoup", "asoup", "soup" });
		setCategory(Category.Combat);
		setColor(0xFFA3E8EF);
		setVisible(true);
		
		this.timer = new Timer();
	}
	
	@Override
	public void loadProperties() {
		this.properties.add(delay = new Property<Float>("AutoSoup Delay", "Delay",(Float)5.0F, (Float)0.0F, (Float)10.0F, (Float)0.1F, new String[] { "delay" }));
		this.properties.add(health = new Property<Float>("AutoSoup Health", "Health",(Float)1.0F, (Float)0.0F, (Float)20.0F, (Float)1.0F, new String[] { "health" }));
	}
	
	@Handler
	protected void onUpdate(final UpdateEvent event) 
	{
		setExtension(String.format("%s", "�7" + getSoupCount()));
		if(Wrapper.getPlayer().getHealth() > health.getFloatProperty()) return;
		if(timer.check(1000 / delay.getFloatProperty())) 
		{
			if(hotbarHasSoups()) {
				eatSoup();
			} else if(hotbarHasBowls()) { 
				dropBowls();
			} else {
				getSoupsFromInventory();
			}
			timer.reset();
		}
	}
	
	private int getSoupCount() {
		int count = 0;
		for(int i = 9; i < 45; i++) {
			final ItemStack stack = Wrapper.getPlayer().inventoryContainer.getSlot(i).getStack();
			if(stack == null) continue;
			if(hasSoups(stack))
				count++;
		}
		return count;
	}
	
	private void eatSoup() 
	{
		boolean found = false;
		int item = -1;
		int current = Wrapper.getPlayer().inventory.currentItem;
		for(int i = 36; i < 45; i++) {
			final ItemStack stack = Wrapper.getPlayer().inventoryContainer.getSlot(i).getStack();
			if(stack == null) continue;
			if(hasSoups(stack)) { 
				found = true;
				item = i;
			}
		}
		
		if(found) {
			Wrapper.getPlayer().sendQueue.addToSendQueue(new C09PacketHeldItemChange(item - 36));
			Wrapper.getPlayer().sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(Wrapper.getPlayer().inventory.getCurrentItem()));
			Wrapper.getPlayer().sendQueue.addToSendQueue(new C09PacketHeldItemChange(current));
		} 
	}
	
	private void getSoupsFromInventory() 
	{
		boolean found = false;
		int item = -1;
		for(int i = 36; i >= 9; i--) {
			final ItemStack stack = Wrapper.getPlayer().inventoryContainer.getSlot(i).getStack();
			if(stack == null) continue;
			if(hasSoups(stack)) {
				found = true;
				item = i;
			}
		}
		
		if(found) {
			Wrapper.getController().windowClick(0, item, 0, 1, Wrapper.getPlayer());
		}
	}
	
	private boolean hotbarHasBowls() {
		for(int i = 36; i < 45; i++) {
			final ItemStack stack = Wrapper.getPlayer().inventoryContainer.getSlot(i).getStack();
			if(stack == null || (!(stack.getItem() == Items.bowl))) continue;
			return true;
		}
		return false;
	}
	
	private void dropBowls() {
		boolean found = false;
		int item = -1;
		for(int i = 36; i < 45; i++) {
			final ItemStack stack = Wrapper.getPlayer().inventoryContainer.getSlot(i).getStack();
			if(stack == null) continue;
			if(hasBowls(stack)) {
				found = true;
				item = i;
			}
		}
		
		if(found) {
			Wrapper.getController().windowClick(0, item, 0, 1, Wrapper.getPlayer());
			Wrapper.getController().windowClick(0, -999, 0, 1, Wrapper.getPlayer());
		}
	}
	
	private boolean hotbarHasSoups() {
		for(int i = 36; i < 45; i++) {
			final ItemStack stack = Wrapper.getPlayer().inventoryContainer.getSlot(i).getStack();
			if(stack == null || (!(stack.getItem() instanceof ItemSoup))) continue;
			return hasSoups(stack);
		}
		return false;
	}
	
	private boolean hasSoups(ItemStack stack) 
	{
		return stack != null && stack.getItem() instanceof ItemSoup;
	}
	
	private boolean hasBowls(ItemStack stack) 
	{
		return stack != null && stack.getItem() == Items.bowl;
	}
}
