package me.jamie.destiny.impl.module.modules.combat;

import java.util.Collections;
import java.util.Comparator;

import me.jamie.destiny.impl.events.motion.UpdateEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Timer;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C09PacketHeldItemChange;
import net.minecraft.potion.PotionEffect;

/*
 * Combat: 
 * - AutoArmour
 * - AutoLog (health)
 * - Kill Aura (delay, range, autoblock, sprinting, mobs, animals, vehicles, players, teams, age, mode [switch, single, multi])
 * - Criticals (mode [packet, jump, new])
 * - Triggerbot (mode [ethylene, normal]
 */
public class AutoPotion extends Module
{
	private Timer timer;
	
	private Property<Float> delay, health;
	
	public AutoPotion() {
		super("Auto Potion", new String[] { "autopot", "autopotion", "ap" });
		setCategory(Category.Combat);
		setColor(0xFF4A4A);
		setVisible(true);
		
		this.timer = new Timer();
	}
	
	@Override
	public void loadProperties() {
		this.properties.add(delay = new Property<Float>("AutoPot Delay", "Delay",(Float)5.0F, (Float)0.0F, (Float)10.0F, (Float)0.1F, new String[] { "autopotdelay", "apd" }));
		this.properties.add(health = new Property<Float>("AutoPot Health", "Health",(Float)1.0F, (Float)0.0F, (Float)20.0F, (Float)1.0F, new String[] { "autopothealth", "aph" }));
	}
	
	@Handler
	protected void onUpdate(final UpdateEvent event) 
	{
		setExtension(String.format("%s", "�7" + getPotionCount()));
		if(Wrapper.getPlayer().getHealth() > health.getFloatProperty()) return;
		if(timer.check(1000 / delay.getFloatProperty())) 
		{
			if(hotbarHasPotions()) {
				splashPotion();
			} else {
				getPotionsFromInventory();
			}
			timer.reset();
		}
	}
	
	private int getPotionCount() {
		int count = 0;
		for(int i = 9; i < 45; i++) {
			final ItemStack stack = Wrapper.getPlayer().inventoryContainer.getSlot(i).getStack();
			if(stack == null) continue;
			if(hasPotions(stack))
				count++;
		}
		return count;
	}
	
	private void splashPotion() 
	{
		boolean found = false;
		int item = -1;
		int current = Wrapper.getPlayer().inventory.currentItem;
		for(int i = 36; i < 45; i++) {
			final ItemStack stack = Wrapper.getPlayer().inventoryContainer.getSlot(i).getStack();
			if(stack == null) continue;
			if(hasPotions(stack)) { 
				found = true;
				item = i;
			}
		}
		
		if(found) {
			Wrapper.getPlayer().sendQueue.addToSendQueue(new C03PacketPlayer.C05PacketPlayerLook(Wrapper.getPlayer().rotationYaw, 95, Wrapper.getPlayer().onGround));
			Wrapper.getPlayer().sendQueue.addToSendQueue(new C09PacketHeldItemChange(item - 36));
			Wrapper.getPlayer().sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(Wrapper.getPlayer().inventory.getCurrentItem()));
			Wrapper.getPlayer().sendQueue.addToSendQueue(new C09PacketHeldItemChange(current));
		} 
	}
	
	private void getPotionsFromInventory() 
	{
		boolean found = false;
		int item = -1;
		for(int i = 36; i >= 9; i--) {
			final ItemStack stack = Wrapper.getPlayer().inventoryContainer.getSlot(i).getStack();
			if(stack == null) continue;
			if(hasPotions(stack)) {
				found = true;
				item = i;
			}
		}
		
		if(found) {
			Wrapper.getController().windowClick(0, item, 0, 1, Wrapper.getPlayer());
		}
	}
	
	private boolean hotbarHasPotions() {
		for(int i = 36; i < 45; i++) {
			final ItemStack stack = Wrapper.getPlayer().inventoryContainer.getSlot(i).getStack();
			if(stack == null || (!(stack.getItem() instanceof ItemPotion))) continue;
			return hasPotions(stack);
		}
		return false;
	}
	
	private boolean hasPotions(ItemStack stack) 
	{
		if(stack != null && (stack.getItem() instanceof ItemPotion)) {
			final ItemPotion potion = (ItemPotion)stack.getItem();
			for(Object o : potion.getEffects(stack)) {
				final PotionEffect pe = (PotionEffect)o;
				if(pe == null) 
					return false;
				return (pe.getEffectName().contains("potion.heal"));
			}
		}
		return false;
	}
}
