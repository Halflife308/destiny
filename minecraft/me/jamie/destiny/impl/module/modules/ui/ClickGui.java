package me.jamie.destiny.impl.module.modules.ui;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.ui.click.GuiClick;
import me.jamie.destiny.util.game.Wrapper;

public class ClickGui extends Module
{

	public ClickGui() {
		super("ClickGui", new String[] { "clickgui" });
		setBind(Keyboard.KEY_RSHIFT);
		setCategory(Category.Movement);
	}
	
	@Override
	public void onEnable() {
		super.onEnable();
		Wrapper.getMinecraft().displayGuiScreen(Destiny.getInstance().getGuiClick());
		setEnabled(false);
	}
}
