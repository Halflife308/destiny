package me.jamie.destiny.impl.module.modules.ui;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.events.render.Render2DEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;

public class Test extends Module
{
	private Property<Boolean> title;

	public Test() {
		super("Test", new String[] { "test" } );
		setEnabled(true);
		setCategory(Category.Movement);
	}
	
	@Override
	public void loadProperties() {
	}
	
	@Handler
	protected void onRender2D(final Render2DEvent event) {
		
	}
}