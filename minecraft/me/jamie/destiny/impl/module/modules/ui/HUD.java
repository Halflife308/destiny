package me.jamie.destiny.impl.module.modules.ui;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.command.Command;
import me.jamie.destiny.impl.events.render.Render2DEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.destiny.util.gui.Draw;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.client.gui.Gui;
import net.minecraft.util.MathHelper;

public class HUD extends Module
{
	private Property<Boolean> title, coords, time, arraylist;
	private Property<Float> test;
	
	private Calendar calender;
    private DateFormat format, format1;

	public HUD() {
		super("HUD", new String[] { "hud" } );
		setEnabled(true);
		setBind(Keyboard.KEY_F);
		setCategory(Category.Movement);
	}
	
	@Override
	public void loadProperties() {
		this.properties.add(title = new Property<Boolean>("HUD Title", "Title", true, new String[] { "title", "logo" }));
		this.properties.add(coords = new Property<Boolean>("HUD Coordinates", "Coordinates", true, new String[] { "coords", "coordinates" }));
		this.properties.add(time = new Property<Boolean>("HUD Time", "Time", true, new String[] { "time", "clock" }));
		this.properties.add(arraylist = new Property<Boolean>("HUD Arraylist", "Arraylist", true, new String[] { "arraylist", "array" }));
		
		
		Collections.sort(this.getProperties(), new Comparator<Property>() {
	        @Override
	        public int compare(Property p1, Property p2) {
	            return p1.getLabel().compareToIgnoreCase(p2.getLabel());
	        }
	    });
		
		this.properties.add(test = new Property<Float>("HUD Test", "Test",(Float)1.0F, (Float)0.0F, (Float)10.0F, (Float)1.0F, new String[] { "teste", "tese1" }));
	}
	
	@Handler
	protected void onRender2D(final Render2DEvent event) {
		int height = Wrapper.getScaledResolution().getScaledHeight();
		if(title.getProperty()) {
			Wrapper.getFontRenderer().drawStringWithShadow("Destiny �6" + Destiny.getInstance().getVersion(), 2, 2, -1);
		}
		if(coords.getProperty()) {
			int x = MathHelper.floor_double(Wrapper.getPlayer().posX);
			int y = MathHelper.floor_double(Wrapper.getPlayer().posY);
			int z = MathHelper.floor_double(Wrapper.getPlayer().posZ);
			height -= 10;
			Wrapper.getFontRenderer().drawStringWithShadow("�f" + x + ", " + y + ", " + z + " �7XYZ", Wrapper.getScaledResolution().getScaledWidth() - Wrapper.getFontRenderer().getStringWidth(x + ", " + y + ", " + z + " XYZ") - 1, height, -1);
		}
		if(time.getProperty()) {
			height -= 10;
			
			calender = Calendar.getInstance();
			format = new SimpleDateFormat("hh:mm");
			format1 = new SimpleDateFormat("a");
			
			Wrapper.getFontRenderer().drawStringWithShadow("�f" + format.format(calender.getTime()) + " �7" + format1.format(calender.getTime()), Wrapper.getScaledResolution().getScaledWidth() - Wrapper.getFontRenderer().getStringWidth(format.format(calender.getTime()) + " �f" + format1.format(calender.getTime())) - 1, height, -1);
		}
		if(arraylist.getProperty()) {
			int y = 2;
			sortModules();
			for(Module m : Destiny.getInstance().getModuleManager().getContents()) {
				if(m.isEnabled() && m.isVisible()) {
					Wrapper.getFontRenderer().drawStringWithShadow(m.getLabel() + (m.getExtension() != null ? " " + m.getExtension() : ""), Wrapper.getScaledResolution().getScaledWidth() - Wrapper.getFontRenderer().getStringWidth(m.getLabel() + (m.getExtension() != null ? " " + m.getExtension() : "")) - 2, y, m.getColor());
					y +=  9;
				}
			}
		}
	}
	
	private void sortModules() {
		Comparator<Module> modules = new Comparator<Module>() {
			@Override
			public int compare(Module m1, Module m2) {
				int dist1 = m1.getLabel().length() + (m1.getExtension() != null ? m1.getExtension().length() : 0);
				int dist2 = m2.getLabel().length() + (m2.getExtension() != null ? m2.getExtension().length() : 0);
				return dist2 - dist1;
			}
		};
		Collections.sort(Destiny.getInstance().getModuleManager().getContents(), modules);
	}
}
