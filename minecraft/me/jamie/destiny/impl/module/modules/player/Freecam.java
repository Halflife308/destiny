package me.jamie.destiny.impl.module.modules.player;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.impl.events.block.OpaqueBlockEvent;
import me.jamie.destiny.impl.events.block.PushOutOfBlockEvent;
import me.jamie.destiny.impl.events.motion.MoveEntityEvent;
import me.jamie.destiny.impl.events.packet.PacketSendEvent;
import me.jamie.destiny.impl.events.render.RenderHandEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.filter.Filter;
import me.jamie.lib.event.filter.filters.AcceptedPackets;
import me.jamie.lib.event.filter.filters.PacketFilter;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.client.entity.EntityOtherPlayerMP;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.MathHelper;

public class Freecam extends Module
{	
	private double x, y, z;
	private float pitch, yaw, gamma;
	private boolean flying, sneaking;

	private Property<Float> speed;
	
	private EntityOtherPlayerMP camera;
		
	public Freecam() {
		super("Freecam", new String[] { "freecam", "oob" });
		
		setBind(Keyboard.KEY_V);
		setCategory(Category.Player);
	}
	
	@Override
	public void loadProperties() {
		this.properties.add(speed = new Property<Float>("Freecam Speed", "Speed", (Float)1.0F, (Float)0.0F, (Float)10.0F, (Float)0.1F, new String[] { "speed" }));
	}
	
	@Override
	public void onEnable() 
	{
		super.onEnable();
	
		if(Wrapper.getWorld() == null) return;
		
		this.x = Wrapper.getPlayer().posX;
		this.y = Wrapper.getPlayer().posY;
		this.z = Wrapper.getPlayer().posZ;
		
		this.pitch = Wrapper.getPlayer().rotationPitch;
		this.yaw = Wrapper.getPlayer().rotationYaw;	
		
		this.flying = Wrapper.getPlayer().capabilities.isFlying;
		this.sneaking = Wrapper.getPlayer().isSneaking();
		
		this.gamma = Wrapper.getGameSettings().gammaSetting;
		Wrapper.getGameSettings().gammaSetting = 100.0F;
		 
		create();
	}
	
	@Override
	public void onDisable()
	{
		super.onDisable();
		
		if(Wrapper.getWorld() == null) return;
		Wrapper.getPlayer().motionX = 0;
		Wrapper.getPlayer().motionY = 0;
		Wrapper.getPlayer().motionZ = 0;
		
		remove();
	}
	
	@Handler
	protected void onMoveEntity(final MoveEntityEvent event) 
	{
		Wrapper.getPlayer().noClip = true;
	
		float forward = Wrapper.getPlayer().moveForward;
		float strafe = Wrapper.getPlayer().moveStrafing;
		float sin = MathHelper.sin(Wrapper.getPlayer().rotationYaw * (float)Math.PI / 180.0F);
		float cos = MathHelper.cos(Wrapper.getPlayer().rotationYaw * (float)Math.PI / 180.0F);

		event.setX(event.getX() + (strafe * cos - forward * sin) * (speed.getFloatProperty() / 10));
		event.setZ(event.getZ() + (forward * cos + strafe * sin) * (speed.getFloatProperty() / 10));
		
		event.setY(0.0D);
		
		if(Wrapper.getPlayer().movementInput.jump)
			event.setY(0.3);
		else if(Wrapper.getPlayer().movementInput.sneak)
			event.setY(-0.3);
	}
	
	@AcceptedPackets(packets={ C03PacketPlayer.class, C07PacketPlayerDigging.class, C08PacketPlayerBlockPlacement.class })
	@Handler(priority = 500, filters={@Filter(type=PacketFilter.class)})
	protected void onPacketSend(final PacketSendEvent event) {
		event.setCancelled(true);
	}
	
	@Handler
	protected void onOpaqueBlock(final OpaqueBlockEvent event) {
		event.setCancelled(true);
	}
	
	@Handler
	protected void onPushOutOfBlock(final PushOutOfBlockEvent event) {
		event.setCancelled(true);
	}
	
	@Handler
	protected void onRenderHand(final RenderHandEvent event) {
		event.setCancelled(true);
	}
	
	private void create() {
		this.camera = new EntityOtherPlayerMP(Wrapper.getWorld(), Wrapper.getPlayer().getGameProfile());
		this.camera.setPositionAndRotation(this.x, this.y, this.z, this.yaw, this.pitch);
		this.camera.setCurrentItemOrArmor(0, Wrapper.getPlayer().getCurrentEquippedItem());
		this.camera.inventory.armorInventory = Wrapper.getPlayer().inventory.armorInventory;
		this.camera.setSneaking(this.sneaking);
		Wrapper.getWorld().addEntityToWorld(-1, this.camera);
	}
	
	private void remove() {
		if(this.camera == null) return;
		Wrapper.getWorld().removeEntityFromWorld(-1);
		Wrapper.getPlayer().setPositionAndRotation(this.x, this.y, this.z, this.yaw, this.pitch);
		Wrapper.getPlayer().capabilities.isFlying = this.flying;
		Wrapper.getPlayer().noClip = false;
		Wrapper.getPlayer().setSneaking(this.sneaking);
		Wrapper.getGameSettings().gammaSetting = this.gamma;
	}
}
