package me.jamie.destiny.impl.module.modules.player;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.impl.events.motion.PostMotionUpdateEvent;
import me.jamie.destiny.impl.events.motion.PreMotionUpdateEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.network.play.client.C0BPacketEntityAction;
import net.minecraft.network.play.client.C0BPacketEntityAction.Action;

public class Sneak extends Module
{
	public Sneak() {
		super("Sneak");
		
		setBind(Keyboard.KEY_Z);
		setCategory(Category.Player);
		setColor(0xFFFF9B33);
		setVisible(true);
	}
	
	@Override
	public void onDisable() {
		super.onDisable();
		Wrapper.getPlayer().sendQueue.addToSendQueue(new C0BPacketEntityAction(Wrapper.getPlayer(), Action.STOP_SNEAKING));
	}

	@Handler
	protected void onPreMotionUpdate(final PreMotionUpdateEvent event) {
		Wrapper.getPlayer().sendQueue.addToSendQueue(new C0BPacketEntityAction(Wrapper.getPlayer(), Action.STOP_SNEAKING));
	}
	
	@Handler
	protected void onPostMotionUpdate(final PostMotionUpdateEvent event) {
		Wrapper.getPlayer().sendQueue.addToSendQueue(new C0BPacketEntityAction(Wrapper.getPlayer(), Action.START_SNEAKING));
	}
}
