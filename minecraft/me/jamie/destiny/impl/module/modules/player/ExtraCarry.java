package me.jamie.destiny.impl.module.modules.player;

import me.jamie.destiny.impl.events.inventory.InventoryCloseEvent;
import me.jamie.destiny.impl.events.packet.PacketSendEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.lib.event.filter.Filter;
import me.jamie.lib.event.filter.filters.AcceptedPackets;
import me.jamie.lib.event.filter.filters.PacketFilter;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.network.play.client.C0DPacketCloseWindow;

public class ExtraCarry extends Module
{

	public ExtraCarry() {
		super("Extra Carry", new String[] { "extracarry", "xcarry" });
		
		setCategory(Category.Player);
	}
	
	@Handler
	protected void onInventoryClose(final InventoryCloseEvent event) {
		event.setCancelled(true);
	}
	
	@AcceptedPackets(packets={ C0DPacketCloseWindow.class })
	@Handler(priority = 500, filters={@Filter(type=PacketFilter.class)})
	protected void onPacketSend(final PacketSendEvent event) {
		event.setCancelled(true);
	}
}
