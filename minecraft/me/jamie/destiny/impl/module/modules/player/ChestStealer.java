package me.jamie.destiny.impl.module.modules.player;

import me.jamie.destiny.impl.events.motion.UpdateEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Timer;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.client.gui.inventory.GuiChest;
import net.minecraft.item.ItemStack;

public class ChestStealer extends Module
{
	private Timer timer;
	
	public ChestStealer() {
		super("Chest Stealer", new String[] { "cheststealer", "chest", "steal", "autoloot" });
		
		setCategory(Category.Player);
		setColor(0xFFF4883D);
		setVisible(true);
		
		this.timer = new Timer();
	}
	
	@Handler
	protected void onUpdate(final UpdateEvent event) 
	{
		if(!(Wrapper.getMinecraft().currentScreen instanceof GuiChest)) return;
		if(timer.check(180L)) {
			final GuiChest chest = (GuiChest)Wrapper.getMinecraft().currentScreen;
			if(isEmpty(chest))
				Wrapper.getPlayer().closeScreen();
			for(int i = 0; i < chest.lowerChestInventory.getSizeInventory(); i++) {
				final ItemStack stack = chest.inventorySlots.getSlot(i).getStack();
				if(stack == null)
					continue;
				Wrapper.getController().windowClick(chest.inventorySlots.windowId, i, 0, 1, Wrapper.getPlayer());
				break;
			}
			timer.reset();
		}
	}
	
	private boolean isEmpty(GuiChest chest) {
		for(int i = 0; i < chest.lowerChestInventory.getSizeInventory(); i++) {
			final ItemStack stack = chest.inventorySlots.getSlot(i).getStack();
			if(stack == null)
				continue;
			return false;
		}
		return true;
	}
}
