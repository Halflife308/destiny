package me.jamie.destiny.impl.module.modules.player;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.impl.events.inventory.InventoryCloseEvent;
import me.jamie.destiny.impl.events.motion.UpdateEvent;
import me.jamie.destiny.impl.events.packet.PacketSendEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.filter.Filter;
import me.jamie.lib.event.filter.filters.AcceptedPackets;
import me.jamie.lib.event.filter.filters.PacketFilter;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.network.play.client.C0DPacketCloseWindow;

public class InventoryWalk 	extends Module
{

	public InventoryWalk() {
		super("Inventory Walk", new String[] { "invwalk" });
		
		setCategory(Category.Player);
	}
	
	@Handler
	protected void onUpdate(final UpdateEvent event) 
	{
		if(Wrapper.getMinecraft().currentScreen instanceof GuiInventory || Wrapper.getMinecraft().currentScreen instanceof GuiContainer) {
			int[] keys = { Wrapper.getGameSettings().keyBindForward.getKeyCode(), Wrapper.getGameSettings().keyBindBack.getKeyCode(), Wrapper.getGameSettings().keyBindLeft.getKeyCode(), Wrapper.getGameSettings().keyBindRight.getKeyCode(), Wrapper.getGameSettings().keyBindJump.getKeyCode(), Wrapper.getGameSettings().keyBindSneak.getKeyCode() }; 
			
			for(int key : keys) {
				KeyBinding.setKeyBindState(key, Keyboard.isKeyDown(key));
			}
		}
	}
	
	@Handler
	protected void onInventoryClose(final InventoryCloseEvent event) {
		event.setCancelled(true);
	}
	
	@AcceptedPackets(packets={ C0DPacketCloseWindow.class })
	@Handler(priority = 500, filters={@Filter(type=PacketFilter.class)})
	protected void onPacketSend(final PacketSendEvent event) {
		event.setCancelled(true);
	}
}
