package me.jamie.destiny.impl.module.modules.player;

import me.jamie.destiny.impl.events.block.ClickBlockEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockColored;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;

public class AutoTool extends Module {

	public AutoTool() {
		super("Auto Tool");
		
		setCategory(Category.Player);
	}
	
	@Handler
	protected void onBreakBlock(final ClickBlockEvent event) 
	{
		final Block block = Wrapper.getWorld().getBlockState(event.getBlockPos()).getBlock();
		float best = 0.1F;
		int slot = -1;
		for(int i = 36; i < 45; i++) {
			final ItemStack stack = Wrapper.getPlayer().inventoryContainer.getSlot(i).getStack();
			if(stack == null || block == Blocks.air) continue;
			if(getToolStrength(stack, block) > best) {
				best = getToolStrength(stack, block);
				slot = i - 36;
			}
		}
		if(slot != -1) {
			Wrapper.getPlayer().inventory.currentItem = slot;
		}
		Wrapper.getController().updateController();
	}
	
	private float getToolStrength(final ItemStack stack, final Block block) {
		if(stack == null) return 0;
		if(block instanceof BlockColored)
			return stack.getStrVsBlock(block);
		else
			return stack.getStrVsBlock(block) + EnchantmentHelper.getEnchantmentLevel(Enchantment.efficiency.effectId, stack);
	}
}
