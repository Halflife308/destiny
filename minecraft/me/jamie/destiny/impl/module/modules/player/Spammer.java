package me.jamie.destiny.impl.module.modules.player;

import me.jamie.destiny.impl.events.motion.UpdateEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.Timer;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.listen.Handler;

public class Spammer extends Module
{
	private Property<Float> delay;
	
	private Timer timer;
	
	public Spammer() 
	{
		super("Spammer");
	
		setCategory(Category.Player);
		
		this.timer = new Timer();
	}
	
	@Override
	public void loadProperties() {
		this.properties.add(delay = new Property<Float>("Spammer Delay", "Delay", (Float)1800.0F, (Float)100.0F, (Float)2000.0F, (Float)100.0F, new String[] { "delay" }));
	}
	
	@Handler
	protected void onUpdate(final UpdateEvent event) 
	{
		int random = (int) (Math.random() * 901);
		if(timer.check(1800)) {
			if(random < 100) return;
			Wrapper.getPlayer().sendChatMessage("test " + random);
			timer.reset();
		}
	}
}
