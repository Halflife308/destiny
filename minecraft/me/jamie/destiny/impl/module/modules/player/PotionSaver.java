package me.jamie.destiny.impl.module.modules.player;

import me.jamie.destiny.impl.events.packet.PacketSendEvent;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.filter.Filter;
import me.jamie.lib.event.filter.filters.AcceptedPackets;
import me.jamie.lib.event.filter.filters.PacketFilter;
import me.jamie.lib.event.listen.Handler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.play.client.C01PacketChatMessage;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C0BPacketEntityAction;
import net.minecraft.potion.PotionEffect;

public class PotionSaver extends Module
{

	public PotionSaver() {
		super("Potion Saver", new String[] { "potionsaver", "psaver" });
		
		setCategory(Category.Player);
		setColor(0xFFA4FF1E);
		setVisible(true);
	}
	
	@AcceptedPackets(packets = { C03PacketPlayer.class })
	@Handler(priority = 500, filters={@Filter(type=PacketFilter.class)})
	protected void onPacketSend(final PacketSendEvent event) {
		boolean moving = Wrapper.getPlayer().motionX != 0 || Wrapper.getPlayer().motionZ != 0;
		if(!moving && hasPotionEffects() && (!(event.getPacket() instanceof C01PacketChatMessage))) {
			event.setCancelled(true);
			increment();
		}
	}
	
	private void increment() {
		for(Object o : Wrapper.getPlayer().getActivePotionEffects()) {
			final PotionEffect pe = (PotionEffect)o;
			if(hasPotionEffects())
				pe.setDuration(pe.getDuration() + 1);
		}
	}
	
	private boolean hasPotionEffects() {
		for(Object o : Wrapper.getPlayer().getActivePotionEffects()) {
			final PotionEffect pe = (PotionEffect)o;
			return !pe.isBadEffect(); 
		}
		return false;
	}
}
