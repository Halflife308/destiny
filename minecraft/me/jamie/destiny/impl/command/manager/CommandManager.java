package me.jamie.destiny.impl.command.manager;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.reflections.Reflections;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.command.Command;
import me.jamie.destiny.impl.events.message.MessageSendEvent;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.util.game.MessageUtils;
import me.jamie.lib.event.listen.Handler;
import me.jamie.lib.management.ListManager;

public class CommandManager extends ListManager<Command>
{
	private String prefix = ".";
	
	@Override
	public void load() {
		if(getContents() == null)
			setContents(new LinkedList<Command>());
		Destiny.getInstance().getEventManager().register(this);
		final Reflections reflect = new Reflections(Command.class);
		final Set<Class<? extends Command>> commands = reflect.getSubTypesOf(Command.class);
		for (final Class clazz : commands) {
			try {
				final Command command = (Command) clazz.newInstance();
				register(command);
			} catch (final InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		
		Collections.sort(Destiny.getInstance().getCommandManager().getContents(), new Comparator<Command>() {
	        @Override
	        public int compare(Command c1, Command c2) {
	            return c1.getLabel().compareToIgnoreCase(c2.getLabel());
	        }
	    });
	}
	
	@Handler
	protected void onMessageSend(final MessageSendEvent event) 
	{
		StringBuilder sb = new StringBuilder();
		boolean found = false;
		event.setCancelled(event.getMessage().startsWith(prefix));
		String[] args = event.getMessage().split(" ");
		
		if(event.getMessage().startsWith(prefix)) {
			for(Command c : getContents()) {
				for(String alias : c.getAliases()) {
					if(args[0].substring(1).equalsIgnoreCase(alias) || args[0].substring(1).equalsIgnoreCase(c.getLabel())) {
						found = true;
						try {
							c.execute(args);
							break;
						} catch (Exception e) {
							MessageUtils.addChatMessage("Invalid usage. Try: �e" + c.getLabel() + " �7" + c.getArguments());
							break;
						}
					}
				}
			}
			
			boolean pFound = false, print = false, prop = false;
			for(Module m : Destiny.getInstance().getModuleManager().getContents()) {
				if(m.getAliases() == null) {
					if(!args[0].substring(1).equalsIgnoreCase(m.getLabel())) continue;
					prop = true;
					found = true;
				} else {
					for(String alias : m.getAliases()) {
						if(!args[0].substring(1).equalsIgnoreCase(alias)) continue;
						prop = true;
						found = true;
					}
				}
				if(prop) {
					try {
						for(Property p : m.getProperties()) {
							for(String alias : p.getAliases()) {
								if(args[1].equalsIgnoreCase(alias) || args[1].equalsIgnoreCase(p.getDisplay())) {
									try {
										pFound = true;
										break;
									} catch (ArrayIndexOutOfBoundsException e) {
										MessageUtils.addChatMessage("Property '�6" + p.getDisplay() + "�f' " + "must not be empty.");
									}
								} else {
									//sb.append("�6" + p.getDisplay() + "[" + alias + "]" + "�f").append(p.getAliases().length > 1 ? ", " : ".");
								}
							}
							
							sb.append("�6" + p.getDisplay() + "�f").append(p.getAliases().length > 1 ? " " : ".");
							
							List<String> aliases = new LinkedList<>();
							int count = 0;
							for(String alias : p.getAliases()) {
								if(!aliases.contains(alias))
									aliases.add(alias);
							}
							
							sb.append("�7" + aliases.toString() + "�f").append(p.getAliases().length > 1 ? ", " : ".");
							
							
							if(pFound) {
								
								if(p.getProperty() instanceof Boolean) {
									System.out.println(p.getDisplay() + " " + p.getProperty());
									try {
										p.setProperty(Boolean.valueOf(args[2]));
										print = true;
									} catch (ArrayIndexOutOfBoundsException e) {
										MessageUtils.addChatMessage("Property '�6" + p.getDisplay() + "�f' " + "must not be empty.");
										return;
									} catch (Exception e) {
										MessageUtils.addChatMessage("Invalid usage.");
									}
								} else {
									try {
										p.setProperty(Float.valueOf(args[2]));
										print = true;
									} catch (ArrayIndexOutOfBoundsException e) {
										MessageUtils.addChatMessage("Property '�6" + p.getDisplay() + "�f' " + "must not be empty.");
										return;
									} catch (Exception e) {
										MessageUtils.addChatMessage("Invalid usage.");
									}
								}
							}
						if(print) {
							MessageUtils.addChatMessage("Property '�6" + p.getDisplay() + "�f' " + "has been set to �e" + p.getProperty() + "�f.");
							print = false;
							return;
						}
					}
				} catch (ArrayIndexOutOfBoundsException e) {
					System.out.println("");
					MessageUtils.addChatMessage("Property must not be empty.");
					return;
				}
				}
			}
			
			if(!found) {
				MessageUtils.addChatMessage("Unknown command. Do '�ehelp�f' for a list of commands.");
			}
			
			if(!pFound && sb.length() != 0) {
				MessageUtils.addChatMessage("Invalid property. Properties: " + sb.toString().substring(0, sb.toString().length() - 2) + ".");
				return;
			}
		}
	}
	
	@Override
	public void register(Command c) {
		if(getContents().contains(c)) return;
		getContents().add(c);
	}
	
	public String getPrefix() {
		return prefix;
	}
	
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
}
