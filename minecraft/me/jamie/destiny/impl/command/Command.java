package me.jamie.destiny.impl.command;

import me.jamie.lib.interfaces.Labelled;

public abstract class Command implements Labelled
{
	private String label, arguments, description;
	private String[] aliases;
	
	public Command(String label, String arguments, String description, String... aliases) {
		this.label = label;
		this.arguments = arguments;
		this.description = description;
		this.aliases = aliases;
	}
	
	@Override
	public String getLabel() {
		return label;
	}
	
	public String getArguments() {
		return arguments;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String[] getAliases() {
		return aliases;
	}
	
	public abstract void execute(String[] args);
}
