package me.jamie.destiny.impl.command.commands.client;

import java.util.LinkedList;
import java.util.List;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.command.Command;
import me.jamie.destiny.util.game.MessageUtils;

public class Test extends Command
{

	public Test() {
		super("Help", "<none>", "Lists all commands.", new String[] { "help" });
	}

	@Override
	public void execute(String[] args)
	{
		StringBuilder sb = new StringBuilder("Commands: ");
		List<String> aliases = new LinkedList<>();
		for(Command c : Destiny.getInstance().getCommandManager().getContents()) {
			sb.append("�6" + c.getLabel() + "�f").append(c.getAliases().length > 1 ? " " : " ");
			for(String alias : c.getAliases()) {
				if(!aliases.contains(alias))
					aliases.add(alias);
			}
			sb.append("�7" + aliases.toString() + "�f").append(c.getAliases().length > 1 ? ", " : ", ");
			aliases.clear();
		}
		MessageUtils.addChatMessage(sb.toString().substring(0, sb.toString().length() - 2) + ".");
	}
	
}