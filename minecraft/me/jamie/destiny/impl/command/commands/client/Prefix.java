package me.jamie.destiny.impl.command.commands.client;

import java.util.LinkedList;
import java.util.List;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.command.Command;
import me.jamie.destiny.util.game.MessageUtils;

public class Prefix extends Command
{

	public Prefix() {
		super("Prefix", "<prefix>", "Sets the command prefix.", new String[] { "cmdprefix", "pr", "prefix" });
	}

	@Override
	public void execute(String[] args)
	{
		Destiny.getInstance().getCommandManager().setPrefix(args[1]);
		MessageUtils.addChatMessage("Prefix has been set to: '" + args[1] + "'.");
		Destiny.getInstance().getConfigManager().saveAll();
	}
	
}