package me.jamie.destiny.impl.command.commands.game;

import me.jamie.destiny.impl.command.Command;
import me.jamie.destiny.util.game.MessageUtils;
import me.jamie.destiny.util.game.Wrapper;

public class VClip extends Command
{

	public VClip() {
		super("VClip", "<blocks>", "Teleports you up/down.", new String[] { "vc", "vclip" });
	}

	@Override
	public void execute(String[] args) {
		double pos = Double.parseDouble(args[1]);
		if(pos != 0) {
			Wrapper.getPlayer().setPosition(Wrapper.getPlayer().posX, Wrapper.getPlayer().posY + pos, Wrapper.getPlayer().posZ);
			if(pos == Math.ceil(pos)) {
				MessageUtils.addChatMessage("Teleported �e" + (int)pos + "�f block" + (pos >= -1 && pos <= 1 ? "" : "s") + ".");
			} else {
				MessageUtils.addChatMessage("Teleported �e" + pos + "�f blocks.");
			}
		} else {
			MessageUtils.addChatMessage("Amount must not be zero.");
		}
	}
	
}
