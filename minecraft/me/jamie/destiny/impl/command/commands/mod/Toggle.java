package me.jamie.destiny.impl.command.commands.mod;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.command.Command;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.util.game.MessageUtils;

public class Toggle extends Command
{

	public Toggle() {
		super("Toggle", "<mod>", "Toggles a module.", new String[] { "t", "toggle" });
	}

	@Override
	public void execute(String[] args) 
	{
		for(Module m : Destiny.getInstance().getModuleManager().getContents()) {
			for(String alias : m.getAliases()) {
				if(args[1].equalsIgnoreCase(m.getLabel()) || args[1].equalsIgnoreCase(alias)) {
					m.toggle();
					MessageUtils.addChatMessage("Module '�6" + m.getLabel() + "�f' has been toggled " + (m.isEnabled() ? "�aon" : "�coff") + "�f.");
					break;
				}
			}
		}
	}
	
}
