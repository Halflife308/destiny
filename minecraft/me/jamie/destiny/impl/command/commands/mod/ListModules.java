package me.jamie.destiny.impl.command.commands.mod;

import java.util.LinkedList;
import java.util.List;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.command.Command;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.util.game.MessageUtils;

public class ListModules extends Command
{

	public ListModules() {
		super("ListModules", "<none>", "Lists all the modules.", new String[] { "lm", "loadmodules", "modules" });
	}

	@Override
	public void execute(String[] args) 
	{
		StringBuilder sb = new StringBuilder("Modules: ");
		List<String> aliases = new LinkedList<>();
		for(Module m : Destiny.getInstance().getModuleManager().getContents()) {
			for(String alias : m.getAliases()) {
				if(!aliases.contains(alias))
					aliases.add(alias);
			}
			sb.append("" + (m.isEnabled() ? "�a" : "�c") + m.getLabel() + "�f").append(m.getAliases().length > 1 ? " " : " ");
			sb.append("�7" + aliases.toString() + "�f").append(m.getAliases().length > 1 ? ", " : ", ");
			aliases.clear();
		}
		MessageUtils.addChatMessage(sb.toString().substring(0, sb.toString().length() - 2) + ".");
	}
	
}