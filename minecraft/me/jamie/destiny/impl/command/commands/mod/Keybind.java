package me.jamie.destiny.impl.command.commands.mod;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.command.Command;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.util.game.MessageUtils;

public class Keybind extends Command
{

	public Keybind() {
		super("Bind", "<mod> <key>", "Binds a module to a key.", new String[] { "bind", "kb", "key", "keybind" });
	}

	@Override
	public void execute(String[] args) {		
		boolean found = false;
		for(Module m : Destiny.getInstance().getModuleManager().getContents()) {
			for(String alias : m.getAliases()) {
				if(args[1].equalsIgnoreCase(m.getLabel()) || args[1].equalsIgnoreCase(alias)) {
					found = true;
					System.out.println(m.getAliases().length);
					m.setBind(Keyboard.getKeyIndex(args[2].toUpperCase()));
					System.out.println("???");
					MessageUtils.addChatMessage("Module '�6" + m.getLabel() + "�f' has been bound to: �e" + args[2].toUpperCase() + "�f.");
					break;
				}
			}
		}
		if(!found) {
			MessageUtils.addChatMessage("Module '�6" + args[1] + "�f' could not be found.");
			return;
		}
	}
	
}
