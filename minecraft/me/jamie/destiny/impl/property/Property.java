package me.jamie.destiny.impl.property;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import me.jamie.lib.interfaces.Labelled;

public class Property<T> implements Labelled
{
	private String label, display, current;
	private String[] aliases;
	
	private T property, max, min, inc;
	private List<String> properties;
	
	public Property(final String label, final String display, final T property, final String... aliases) {
		this.properties = new ArrayList<>();
		this.label = label;
		this.property = property;
		this.display = display;
		this.aliases = aliases;
		addProperties();
	}
	
	public Property(final String label, final String display, final T property, final T min, final T max, final T inc, final String... aliases) {
		this.properties = new ArrayList<>();
		this.label = label;
		this.display = display;
		this.property = property;
		this.min = min;
		this.max = max;
		this.inc = inc;
		this.aliases = aliases;
		addProperties();
	}
	
	public String getLabel() {
		return label;
	}
	
	public String getDisplay() {
		return display;
	}
	
	public String[] getAliases() {
		return aliases;
	}
	
	public T getProperty() {
		return property;
	}
	
	public void setProperty(T property) {
		this.property = property;
	}
	
	public String getCurrentProperty() {
		if(current == null)
			return (String)property;
		return current;
	}
	
	public void setCurrentProperty(String current) {
		this.current = current;
	}
	
	public List<String> getProperties() {
		return properties;
	}
	
	public void addProperties() {}
	
	public void increment() {
		Iterator itr = getProperties().iterator();
		if(getProperties() == null) return;
		while(itr.hasNext()) {
			String s = (String)itr.next();
			if(s.equals(getCurrentProperty())) {
				if(itr.hasNext()) {
					String s1 = (String) itr.next();
					setCurrentProperty(s1);
					break;
				} else {
					String s2 = (String)getProperties().get(0);
					setCurrentProperty(s2);
					break;
				}
			}
		}
	}
	
	public void decrement() {
		ListIterator li = properties.listIterator(properties.size());
		if(getProperties() == null) return;
		while(li.hasPrevious()) {
			String s = (String)li.previous();
			if(s.equals(getCurrentProperty())) {
				if(li.hasPrevious()) {
					String s1 = (String) li.previous();
					setCurrentProperty(s1);
					break;
				} else {
					String s2 = (String)getProperties().get(properties.size() - 1);
					setCurrentProperty(s2);
					break;
				}
			}
		}
	}
	
	public T getMinimum() {
		return min;
	}
	
	public T getMaximum() {
		return max;
	}
	
	public T getIncrement() {
		return inc;
	}
	
	public float getFloatProperty() {
		return Float.parseFloat(property.toString());
	}
	
	public int getIntProperty() {
		return Integer.parseInt(property.toString());
	}
	
	public double getDoubleProperty() {
		return Double.parseDouble(property.toString());
	}
}
