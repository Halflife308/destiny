package me.jamie.destiny.impl.events.block;

import me.jamie.lib.event.Event;
import net.minecraft.util.BlockPos;

public class ClickBlockEvent extends Event
{
	private BlockPos pos;
	
	public ClickBlockEvent(final BlockPos pos) {
		this.pos = pos;

	}
	
	public BlockPos getBlockPos() {
		return pos;
	}
}
