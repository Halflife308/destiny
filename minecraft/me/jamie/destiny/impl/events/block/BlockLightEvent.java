package me.jamie.destiny.impl.events.block;

import me.jamie.lib.event.Event;

public class BlockLightEvent extends Event
{
	private float light;
	
	public BlockLightEvent(float light) {
		this.light = light;
	}
	
	public float getLight() {
		return light;
	}
	
	public void setLight(float light) {
		this.light = light;
	}
}
