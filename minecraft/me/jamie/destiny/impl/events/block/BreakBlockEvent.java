package me.jamie.destiny.impl.events.block;

import me.jamie.lib.event.Event;
import net.minecraft.util.BlockPos;

public class BreakBlockEvent extends Event
{
	private BlockPos pos;
	private int delay;
	private double multiplier;
	private float damage;
	
	public BreakBlockEvent(final BlockPos pos, final int delay, final double multiplier, final float damage) {
		this.pos = pos;
		this.delay = delay;
		this.multiplier = multiplier;
		this.damage = damage;
	}
	
	public BlockPos getBlockPos() {
		return pos;
	}
	
	public int getDelay() {
		return delay;
	}
	
	public double getMultiplier() {
		return multiplier;
	}
	
	public float getBlockDamage() {
		return damage;
	}

	public void setPos(BlockPos pos) {
		this.pos = pos;
	}

	public void setDelay(int delay) {
		this.delay = delay;
	}

	public void setMultiplier(double multiplier) {
		this.multiplier = multiplier;
	}
	
	public void setBlockDamage(float damage) {
		this.damage = damage;
	}
}
