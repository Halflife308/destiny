package me.jamie.destiny.impl.events.block;

import me.jamie.lib.event.Event;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;

public class BoundingBoxEvent extends Event 
{
	private AxisAlignedBB aabb;
	private final int x, y, z;
	
	public BoundingBoxEvent(AxisAlignedBB aabb, int x, int y, int z)
	{
		this.aabb = aabb;
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public final AxisAlignedBB getAABB() {
		return aabb;
	}
	
	public final void setAABB(AxisAlignedBB aabb) {
		this.aabb = aabb;
	}
	
	public final int getX() {
		return x;
	}
	
	public final int getY() {
		return y;
	}
	
	public final int getZ() {
		return z;
	}
}