package me.jamie.destiny.impl.events.key;

import me.jamie.lib.event.Event;

public class KeyToggleEvent extends Event
{
	private int key;
	
	public KeyToggleEvent(final int key) {
		this.key = key;
	}
	
	public int getKey() {
		return key;
	}
	
	public void setKey(int key) {
		this.key = key;
	}
}
