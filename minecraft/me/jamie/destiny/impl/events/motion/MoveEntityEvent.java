package me.jamie.destiny.impl.events.motion;

import me.jamie.lib.event.Event;

public class MoveEntityEvent extends Event	
{
	private double x, y, z;
	
	public MoveEntityEvent(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}
}
