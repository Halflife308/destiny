package me.jamie.destiny.impl.events.motion;

import me.jamie.lib.event.Event;

public class PreMotionUpdateEvent extends Event {
	private float yaw, pitch;
	
	public PreMotionUpdateEvent(float yaw, float pitch) {
		this.yaw = yaw;
		this.pitch = pitch;
	}

	public float getYaw() {
		return yaw;
	}
	
	public float getPitch() {
		return pitch;
	}
	
	public void setYaw(float yaw) {
		this.yaw = yaw;
	}
	
	public void setPitch(float pitch) {
		this.pitch = pitch;
	}
}
