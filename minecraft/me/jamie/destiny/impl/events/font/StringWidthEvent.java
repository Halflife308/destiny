package me.jamie.destiny.impl.events.font;

import me.jamie.lib.event.Event;

public class StringWidthEvent extends Event
{
	private String text;
	
	public StringWidthEvent(String text) {
		this.text = text;
	}
	
	public String getText() {
		return text;
	}
}
