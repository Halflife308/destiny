package me.jamie.destiny.impl.events.packet;

import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.Event;
import net.minecraft.network.Packet;

public class PacketReceiveEvent extends Event 
{
	private Packet packet;

	public PacketReceiveEvent(Packet packet) {
		this.packet = packet;
	}

	public Packet getPacket() {
		return this.packet;
	}
}
