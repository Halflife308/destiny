package me.jamie.destiny.impl.events.packet;

import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.event.Event;
import net.minecraft.network.Packet;

public class PacketSendEvent extends Event
{
  private Packet packet;
  
  public PacketSendEvent(Packet packet)
  {
    this.packet = packet;
  }
  
  public void sendPacket(Packet packet)
  {
    Wrapper.getMinecraft().getNetHandler().getNetworkManager().sendPacket(packet);
  }
  
  public Packet getPacket()
  {
    return this.packet;
  }
}
