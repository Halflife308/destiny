package me.jamie.destiny.impl.events.render;

import me.jamie.lib.event.Event;

public class Render3DEvent extends Event 
{
	private float partialTicks;
	
	public Render3DEvent(float partialTicks) {
		this.partialTicks = partialTicks;
	}
	
	public float getPartialTicks() {
		return partialTicks;
	}
}
