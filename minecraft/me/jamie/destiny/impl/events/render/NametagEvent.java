package me.jamie.destiny.impl.events.render;

import me.jamie.lib.event.Event;
import net.minecraft.entity.EntityLivingBase;

public class NametagEvent extends Event
{
	private EntityLivingBase entity;
	private double x, y, z;
	
	public NametagEvent(EntityLivingBase entity, double x, double y, double z) 
	{
		this.entity = entity;
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public EntityLivingBase getEntity() {
		return entity;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}
}
