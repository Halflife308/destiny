package me.jamie.destiny.impl.events.message;

import me.jamie.lib.event.Event;

public class MessageSendEvent extends Event	
{
	private String message;
	
	public MessageSendEvent(final String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
}
