package me.jamie.destiny.impl.hooks.key;

import dk.n3xuz.hookregistry.context.Context;

public class KeyToggleContext extends Context
{
	private int key;
	
	public KeyToggleContext(final int key) {
		this.key = key;
	}
	
	public int getKey() {
		return key;
	}
	
	public void setKey(int key) {
		this.key = key;
	}
}
