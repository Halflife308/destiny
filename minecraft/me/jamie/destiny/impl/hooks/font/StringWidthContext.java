package me.jamie.destiny.impl.hooks.font;

import dk.n3xuz.hookregistry.context.CancellableContext;

public class StringWidthContext extends CancellableContext
{
	private String text;
	
	public StringWidthContext(String text) {
		this.text = text;
	}
	
	public String getText() {
		return text;
	}
}
