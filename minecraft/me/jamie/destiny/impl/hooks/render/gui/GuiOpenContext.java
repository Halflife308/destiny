package me.jamie.destiny.impl.hooks.render.gui;

import java.lang.reflect.Field;

import dk.n3xuz.hookregistry.context.CancellableContext;
import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.destiny.util.mcleaks.PacketHandler;
import net.minecraft.client.gui.GuiDisconnected;
import net.minecraft.client.gui.GuiMultiplayer;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.multiplayer.GuiConnecting;
import net.minecraft.network.NetworkManager;

public class GuiOpenContext extends CancellableContext {
	private GuiScreen gui;
	
	public GuiOpenContext(GuiScreen gui) {
		this.gui = gui;
	}
	
	public GuiScreen getGui() {
		return gui;
	}
	
	public void connect() {
		//TODO: Destiny
    	if((Wrapper.getMinecraft().currentScreen instanceof GuiMultiplayer || Wrapper.getMinecraft().currentScreen instanceof GuiDisconnected) && Destiny.getInstance().getSessionManager().getMCLeaksSession() != null) {
    		Destiny.getInstance().getSessionManager().setSession(Destiny.getInstance().getSessionManager().getName());
    	}
    	
    	if(this.getGui() instanceof GuiConnecting && Destiny.getInstance().getSessionManager().getMCLeaksSession() != null) {
    		final GuiConnecting gui = (GuiConnecting)this.getGui();
    		new Thread(new NetworkThread(this, gui)).start();		        
    	}
 
	}

}

class NetworkThread implements Runnable 
{
	private GuiOpenContext parent;
	private GuiScreen gui;
	
	public NetworkThread(GuiOpenContext parent, GuiScreen gui) {
		this.parent = parent;
		this.gui = gui;
	}
	
	@Override
    public void run() {
		while(true) {
	         NetworkManager networkManager = (NetworkManager)getFieldByClass(this.gui, NetworkManager.class);
	         if(networkManager != null) {
	            networkManager.channel().pipeline().addBefore("packet_handler", "mcleaks_handler", new PacketHandler());
	            Destiny.getInstance().getSessionManager().setCurrentNetworkManager(networkManager);
	            return;
	         }

	         try {
	            Thread.sleep(2L);
	         } catch (InterruptedException var3) {
	            var3.printStackTrace();
	         }
	      }
	}
	
	public static Object getFieldByClass(Object o, Class searchingClazz) {
	      return getFieldByClass(o.getClass(), o, searchingClazz);
	   }

	   public static Object getFieldByClass(Class parentClass, Object o, Class searchingClazz) {
	      Field field = null;
	      Field[] e = parentClass.getDeclaredFields();
	      int toReturn = e.length;

	      for(int var6 = 0; var6 < toReturn; ++var6) {
	         Field f = e[var6];
	         if(f.getType().equals(searchingClazz)) {
	            field = f;
	            break;
	         }
	      }

	      if(field == null) {
	         return null;
	      } else {
	         try {
	            boolean var9 = field.isAccessible();
	            field.setAccessible(true);
	            Object var10 = field.get(o);
	            field.setAccessible(var9);
	            return var10;
	         } catch (IllegalAccessException var8) {
	            var8.printStackTrace();
	            return null;
	         }
	      }
	   }
}
