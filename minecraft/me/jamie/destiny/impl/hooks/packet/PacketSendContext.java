package me.jamie.destiny.impl.hooks.packet;

import dk.n3xuz.hookregistry.context.Context;
import me.jamie.destiny.util.game.Wrapper;
import net.minecraft.network.Packet;

public class PacketSendContext extends Context 
{
	private Packet packet;

	public PacketSendContext(Packet packet)
	  {
	    this.packet = packet;
	  }

	public void sendPacket(Packet packet) {
		Wrapper.getMinecraft().getNetHandler().getNetworkManager().sendPacket(packet);
	}

	public Packet getPacket() {
		return this.packet;
	}
}
