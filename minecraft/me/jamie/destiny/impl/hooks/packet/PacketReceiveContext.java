package me.jamie.destiny.impl.hooks.packet;

import dk.n3xuz.hookregistry.context.Context;
import net.minecraft.network.Packet;

public class PacketReceiveContext extends Context
{
	private Packet packet;

	public PacketReceiveContext(Packet packet) {
		this.packet = packet;
	}

	public Packet getPacket() {
		return this.packet;
	}
}
