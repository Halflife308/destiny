package me.jamie.destiny.impl.hooks.message;

import dk.n3xuz.hookregistry.context.CancellableContext;

public class MessageSendContext extends CancellableContext
{
	private String message;
	
	public MessageSendContext(final String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
}
