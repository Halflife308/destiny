package me.jamie.destiny.util.game;

import net.minecraft.util.ChatComponentText;

public class MessageUtils 
{
	public static void addChatMessage(String s) {
		if(s == null || Wrapper.getWorld() == null) return;
		Wrapper.getPlayer().addChatMessage(new ChatComponentText("�6[Destiny]: �f" + s));
	}

	public static void addConsoleMessage(String s) {
		if(s == null) return;
		System.out.println("[Destiny]: " + s);
	}
}
