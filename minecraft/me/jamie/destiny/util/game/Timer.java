package me.jamie.destiny.util.game;

public class Timer {
	private long previousMS;

	public Timer() {
		reset();
	}

	public boolean check(float milliseconds) {
		return getCurrentMS() - previousMS >= milliseconds;
	}

	public void reset() {
		previousMS = getCurrentMS();
	}

	public long getPreviousMS() {
		return previousMS;
	}

	public long getCurrentMS() {
		return System.nanoTime() / 1000000;
	}

}
