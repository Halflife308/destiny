package me.jamie.destiny.util.game;

import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.BlockLiquid;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;

public class PlayerUtils 
{
	public static void damage(int amount) {
		final double[] d = { 0.2, 0.26 };
		for (int a = 0; a < 60; ++a) {
			for (int i = 0; i < d.length; ++i) {
				Wrapper.getPlayer().sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(
						Wrapper.getPlayer().posX, Wrapper.getPlayer().posY + d[i], Wrapper.getPlayer().posZ, false));
			}
		}
	}

	public static boolean isOnLiquid() {
		if (Wrapper.getPlayer() == null)
			return false;
		boolean onLiquid = false;
		final int y = (int) Wrapper.getPlayer().getEntityBoundingBox().offset(0.0D, -0.01D, 0.0D).minY;
		for (int x = MathHelper.floor_double(Wrapper.getPlayer().getEntityBoundingBox().minX); x < MathHelper
				.floor_double(Wrapper.getPlayer().getEntityBoundingBox().maxX) + 1; x++) {
			for (int z = MathHelper.floor_double(Wrapper.getPlayer().getEntityBoundingBox().minZ); z < MathHelper
					.floor_double(Wrapper.getPlayer().getEntityBoundingBox().maxZ) + 1; z++) {
				final Block block = Wrapper.getWorld().getBlockState(new BlockPos(x, y, z)).getBlock();
				if (block != null && !(block instanceof BlockAir)) {
					if (!(block instanceof BlockLiquid))
						return false;
					onLiquid = true;
				}
			}
		}
		return onLiquid;
	}
	
	public static boolean isInLiquid() {
        boolean inLiquid = false;
        final int y = (int) Wrapper.getPlayer().getEntityBoundingBox().minY;
        for (int x = MathHelper.floor_double(Wrapper.getPlayer().getEntityBoundingBox().minX); x < MathHelper.floor_double(Wrapper.getPlayer().getEntityBoundingBox().maxX) + 1; x++) {
            for (int z = MathHelper.floor_double(Wrapper.getPlayer().getEntityBoundingBox().minZ); z < MathHelper.floor_double(Wrapper.getPlayer().getEntityBoundingBox().maxZ) + 1; z++) {
                final Block block = Wrapper.getWorld().getBlockState(new BlockPos(x, y, z)).getBlock();
                if (block != null && !(block instanceof BlockAir)) {
                    if (!(block instanceof BlockLiquid))
                        return false;
                    inLiquid = true;
                }
            }
        }
        return inLiquid || Wrapper.getPlayer().isInWater();
    }
}
