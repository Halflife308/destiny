package me.jamie.destiny.util.mcleaks;

import me.jamie.destiny.util.game.Wrapper;
import net.minecraft.network.NetworkManager;
import net.minecraft.util.Session;

public class SessionManager
{
	private String leaksSession, name;
	private Session session, saved;
	private NetworkManager currentNetworkManager;
	
	public Session getSelectedSession() {
		return session;
	}
	
	public Session getSavedSession() {
		return saved;
	}
	
	public void setSavedSession(Session saved) {
		this.saved = saved;
	}
	
	public String getMCLeaksSession() {
		return leaksSession;
	}
	
	public void setMCLeaksSession(String leaksSession) {
		this.leaksSession = leaksSession;
	}
	
	public NetworkManager getCurrentNetworkManager() {
        return this.currentNetworkManager;
    }
    
    public void setCurrentNetworkManager(final NetworkManager currentNetworkManager) {
        this.currentNetworkManager = currentNetworkManager;
    }
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public static void setSession(final String mcName) {
        setSession(new Session(mcName, "", "", "mojang"));
    }
    
    public static void setSession(final Session session) {
        Wrapper.getMinecraft().setSession(session);
    }
}
