package me.jamie.destiny.util.mcleaks;

import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.security.PublicKey;
import java.util.List;

import javax.crypto.SecretKey;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.ui.mcleaks.Callback;
import me.jamie.destiny.util.game.Wrapper;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.login.client.C01PacketEncryptionResponse;
import net.minecraft.network.login.server.S01PacketEncryptionRequest;
import net.minecraft.util.CryptManager;

public class PacketHandler extends MessageToMessageDecoder<Packet> {

	@Override
	protected void decode(ChannelHandlerContext ctx, Packet packet, List<Object> out) throws Exception {
		if (Destiny.getInstance().getSessionManager().getCurrentNetworkManager() != null && packet instanceof S01PacketEncryptionRequest) {
			handle(Destiny.getInstance().getSessionManager().getCurrentNetworkManager(), (S01PacketEncryptionRequest) packet);
			ctx.channel().pipeline().remove((ChannelHandler) this);
		} else {
			out.add(packet);
		}
	}
	

    private void handle(final NetworkManager networkManager, final S01PacketEncryptionRequest packetIn) {
        final SecretKey secretkey = CryptManager.createNewSharedKey();
        final String s = packetIn.func_149609_c();
        final PublicKey publickey = packetIn.func_149608_d();
        final String s2 = new BigInteger(CryptManager.getServerIdHash(s, publickey, secretkey)).toString(16);
        final InetSocketAddress remoteAddress = (InetSocketAddress)networkManager.getRemoteAddress();
        Connector.joinServer(Destiny.getInstance().getSessionManager().getMCLeaksSession(), Wrapper.getMinecraft().getSession().getUsername(), s2, remoteAddress.getHostName() + ":" + remoteAddress.getPort(), new Callback() {
        	public void done(final Object o) {
                if (o == null) {
                    return;
                }
                if (o instanceof String) {
                    networkManager.channel().close();
                    return;
                }
                networkManager.sendPacket(new C01PacketEncryptionResponse(secretkey, publickey, packetIn.func_149607_e()), new GenericFutureListener<Future<? super Void>>() {

					@Override
					public void operationComplete(final Future<? super Void> p_operationComplete_1_) throws Exception {
						 networkManager.enableEncryption(secretkey);
					}
                });
            }
        });
    }
}
