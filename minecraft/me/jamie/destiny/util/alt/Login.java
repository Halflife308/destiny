package me.jamie.destiny.util.alt;

import java.net.Proxy;

import com.mojang.authlib.Agent;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.util.game.Wrapper;
import net.minecraft.util.Session;

public class Login 
{
	private String progress;
	private boolean running = false;
	
	public void login(String name, String password)
	{
		try {
			YggdrasilUserAuthentication auth = new YggdrasilUserAuthentication(new YggdrasilAuthenticationService(Proxy.NO_PROXY, ""), new Agent("minecraft", 1));
			auth.setUsername(name);
			auth.setPassword(password);
			try {
				auth.logIn();
				Wrapper.getMinecraft().setSession(new Session(auth.getSelectedProfile().getName(), auth.getSelectedProfile().getId().toString(), auth.getAuthenticatedToken(), "legacy"));
				Destiny.getInstance().getSessionManager().setSession(Wrapper.getMinecraft().getSession());
				Destiny.getInstance().getSessionManager().setMCLeaksSession(null);
				setProgress("�aLogin successful");
			} catch (AuthenticationException ae) {
				setProgress("�cInvalid username/password");
			}
		} catch (Exception e) {
			setProgress("�cError logging in");
		}
	}
	
	public String getProgress() {
		return progress;
	}
	
	public void setProgress(String progress) {
		this.progress = progress;
	}
}
