package me.jamie.destiny.util.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.*;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;

import java.awt.Color;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import static org.lwjgl.opengl.GL11.*;

/**
 * @author DoubleParallax
 */
public class Draw {

	public static String hsvToRgb(final float hue, final float saturation, final float value) {
        final int h = (int)(hue * 6.0f);
        final float f = hue * 6.0f - h;
        final float p = value * (1.0f - saturation);
        final float q = value * (1.0f - f * saturation);
        final float t = value * (1.0f - (1.0f - f) * saturation);
        switch (h) {
            case 0: {
                return rgbToString(value, t, p);
            }
            case 1: {
                return rgbToString(q, value, p);
            }
            case 2: {
                return rgbToString(p, value, t);
            }
            case 3: {
                return rgbToString(p, q, value);
            }
            case 4: {
                return rgbToString(t, p, value);
            }
            case 5: {
                return rgbToString(value, p, q);
            }
            default: {
                throw new RuntimeException("Something went wrong when converting from HSV to RGB. Input was " + hue + ", " + saturation + ", " + value);
            }
        }
    }
    
    public static String rgbToString(final float r, final float g, final float b) {
        final String rs = Integer.toHexString((int)(r * 256.0f));
        final String gs = Integer.toHexString((int)(g * 256.0f));
        final String bs = Integer.toHexString((int)(b * 256.0f));
        return String.valueOf(rs) + gs + bs;
    }
    
    public static Color getRainbow(final float fade) {
        final float hue = System.nanoTime() / 5.0E9f % 1.0f;
        final long color = Long.parseLong(Integer.toHexString(Integer.valueOf(Color.HSBtoRGB(hue, 1.0f, 1.0f))), 16);
        final Color c = new Color((int)color);
        return new Color(c.getRed() / 255.0f * fade, c.getGreen() / 255.0f * fade, c.getBlue() / 255.0f * fade, c.getAlpha() / 255.0f);
    }
    
    public static void startClip(final double x1, double y1, final double x2, double y2) {
        if (y1 > y2) {
            final double temp = y2;
            y2 = y1;
            y1 = temp;
        }
        GL11.glEnable(3089);
        GL11.glScissor((int)x1, (int)(Display.getHeight() - y2), (int)(x2 - x1), (int)(y2 - y1));
    }
    
    public static void endClip() {
        GL11.glDisable(3089);
    }
    
    public static void rect(final double x1, final double y1, final double x2, final double y2, final int color) {
        final float r = (color >> 16 & 0xFF) / 255.0f;
        final float g = (color >> 8 & 0xFF) / 255.0f;
        final float b = (color & 0xFF) / 255.0f;
        final float a = (color >> 24 & 0xFF) / 255.0f;
        final Tessellator t = Tessellator.getInstance();
        final WorldRenderer wr = t.getWorldRenderer();
        GlStateManager.enableBlend();
        GlStateManager.disableTexture();
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
        GlStateManager.color(r, g, b, a);
        wr.startDrawingQuads();
        wr.addVertex(x1, y2, 0.0);
        wr.addVertex(x2, y2, 0.0);
        wr.addVertex(x2, y1, 0.0);
        wr.addVertex(x1, y1, 0.0);
        t.draw();
        GlStateManager.enableTexture();
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GlStateManager.disableBlend();
    }
    
    public static void rectOutline(final double x1, final double y1, final double x2, final double y2, final int color, final double width) {
        rect(x1, y1, x2, y1 + width, color);
        rect(x1, y2 - width, x2, y2, color);
        rect(x1, y1 + width, x1 + width, y2 - width, color);
        rect(x2 - width, y1 + width, x2, y2 - width, color);
    }
    
    public static void rectBordered(final double x1, final double y1, final double x2, final double y2, final int fill, final int outline, final double width) {
        rectOutline(x1, y1, x2, y2, outline, width);
        rect(x1 + width, y1 + width, x2 - width, y2 - width, fill);
    }
    
    public static void drawBorderedRect(double left, double top, double right, double bottom, float borderWidth, int borderColor, int color) {
        float alpha = (borderColor >> 24 & 0xFF) / 255.0f;
        float red = (borderColor >> 16 & 0xFF) / 255.0f;
        float green = (borderColor >> 8 & 0xFF) / 255.0f;
        float blue = (borderColor & 0xFF) / 255.0f;
        GlStateManager.pushMatrix();
        rect(left, top, right, bottom, color);
        GlStateManager.enableBlend();
        GlStateManager.disableTexture();
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
        GlStateManager.color(red, green, blue, alpha);

        if (borderWidth == 1.0F) {
            GL11.glEnable(GL11.GL_LINE_SMOOTH);
        }

        GL11.glLineWidth(borderWidth);
        Tessellator tessellator = Tessellator.getInstance();
        WorldRenderer worldRenderer = tessellator.getWorldRenderer();
        worldRenderer.startDrawing(1);
        worldRenderer.addVertex(left, top, 0.0F);
        worldRenderer.addVertex(left, bottom, 0.0F);
        worldRenderer.addVertex(right, bottom, 0.0F);
        worldRenderer.addVertex(right, top, 0.0F);
        worldRenderer.addVertex(left, top, 0.0F);
        worldRenderer.addVertex(right, top, 0.0F);
        worldRenderer.addVertex(left, bottom, 0.0F);
        worldRenderer.addVertex(right, bottom, 0.0F);
        tessellator.draw();
        GL11.glLineWidth(2.0F);

        if (borderWidth == 1.0F) {
            GL11.glDisable(GL11.GL_LINE_SMOOTH);
        }

        GlStateManager.enableTexture();
        GlStateManager.disableBlend();
        GlStateManager.popMatrix();
    }
    
    public static void rectGradient(final double x1, final double y1, final double x2, final double y2, final int[] color) {
        final float[] r = new float[color.length];
        final float[] g = new float[color.length];
        final float[] b = new float[color.length];
        final float[] a = new float[color.length];
        for (int t = 0; t < color.length; ++t) {
            r[t] = (color[t] >> 16 & 0xFF) / 255.0f;
            g[t] = (color[t] >> 8 & 0xFF) / 255.0f;
            b[t] = (color[t] & 0xFF) / 255.0f;
            a[t] = (color[t] >> 24 & 0xFF) / 255.0f;
        }
        GlStateManager.disableTexture();
        GlStateManager.enableBlend();
        GlStateManager.disableAlpha();
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
        GlStateManager.shadeModel(7425);
        final Tessellator var15 = Tessellator.getInstance();
        final WorldRenderer var16 = var15.getWorldRenderer();
        var16.startDrawingQuads();
        var16.func_178960_a(r[0], g[0], b[0], a[0]);
        var16.addVertex(x2, y1, 0.0);
        var16.func_178960_a(r[1], g[1], b[1], a[1]);
        var16.addVertex(x1, y1, 0.0);
        var16.func_178960_a(r[2], g[2], b[2], a[2]);
        var16.addVertex(x1, y2, 0.0);
        var16.func_178960_a(r[3], g[3], b[3], a[3]);
        var16.addVertex(x2, y2, 0.0);
        var15.draw();
        GlStateManager.shadeModel(7424);
        GlStateManager.disableBlend();
        GlStateManager.enableAlpha();
        GlStateManager.enableTexture();
    }
    
    public static void drawHollowRect(double left, double top, double right, double bottom, float borderWidth, int borderColor) {
        float alpha = (borderColor >> 24 & 0xFF) / 255.0f;
        float red = (borderColor >> 16 & 0xFF) / 255.0f;
        float green = (borderColor >> 8 & 0xFF) / 255.0f;
        float blue = (borderColor & 0xFF) / 255.0f;
        GlStateManager.pushMatrix();
        GlStateManager.enableBlend();
        GlStateManager.disableTexture();
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
        GlStateManager.color(red, green, blue, alpha);

        GL11.glEnable(GL11.GL_LINE_SMOOTH);

        GL11.glLineWidth(borderWidth);
        Tessellator tessellator = Tessellator.getInstance();
        WorldRenderer worldRenderer = tessellator.getWorldRenderer();
        worldRenderer.startDrawing(1);
        worldRenderer.addVertex(left, top, 0.0F);
        worldRenderer.addVertex(left, bottom, 0.0F);
        worldRenderer.addVertex(right, bottom, 0.0F);
        worldRenderer.addVertex(right, top, 0.0F);
        worldRenderer.addVertex(left, top, 0.0F);
        worldRenderer.addVertex(right, top, 0.0F);
        worldRenderer.addVertex(left, bottom, 0.0F);
        worldRenderer.addVertex(right, bottom, 0.0F);
        tessellator.draw();
        GL11.glLineWidth(2.0F);

        GL11.glDisable(GL11.GL_LINE_SMOOTH);

        GlStateManager.enableTexture();
        GlStateManager.disableBlend();
        GlStateManager.popMatrix();
    }
    
    public static void rectGradientOutline(final double x1, final double y1, final double x2, final double y2, final int[] color, final double width) {
        rectGradient(x1, y1, x2, y1 + width, new int[] { color[0], color[1], color[0], color[1] });
        rectGradient(x1, y2 - width, x2, y2, new int[] { color[2], color[3], color[2], color[3] });
        rectGradient(x1, y1 + width, x1 + width, y2 - width, color);
        rectGradient(x2 - width, y1 + width, x2, y2 - width, color);
    }
    
    public static void rectGradientBordered(final double x1, final double y1, final double x2, final double y2, final int[] fill, final int[] outline, final double width) {
        rectGradientOutline(x1, y1, x2, y2, outline, width);
        rectGradient(x1 + width, y1 + width, x2 - width, y2 - width, fill);
    }
    
    public static void line(final double x1, final double y1, final double x2, final double y2, final int color, final float width) {
        final float r = (color >> 16 & 0xFF) / 255.0f;
        final float g = (color >> 8 & 0xFF) / 255.0f;
        final float b = (color & 0xFF) / 255.0f;
        final float a = (color >> 24 & 0xFF) / 255.0f;
        final Tessellator t = Tessellator.getInstance();
        final WorldRenderer wr = t.getWorldRenderer();
        GlStateManager.enableBlend();
        GlStateManager.disableTexture();
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
        GlStateManager.color(r, g, b, a);
        GL11.glLineWidth(width);
        GL11.glEnable(2848);
        wr.startDrawing(2);
        wr.addVertex(x1, y1, 0.0);
        wr.addVertex(x2, y2, 0.0);
        t.draw();
        GL11.glDisable(2848);
        GlStateManager.enableTexture();
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GlStateManager.disableBlend();
    }
    
    public static void drawOutlinedBox(AxisAlignedBB box) {
        if (box == null) {
            return;
        }

        glBegin(GL_LINE_STRIP);
        glVertex3d(box.minX, box.minY, box.minZ);
        glVertex3d(box.maxX, box.minY, box.minZ);
        glVertex3d(box.maxX, box.minY, box.maxZ);
        glVertex3d(box.minX, box.minY, box.maxZ);
        glVertex3d(box.minX, box.minY, box.minZ);
        glEnd();
        glBegin(GL_LINE_STRIP);
        glVertex3d(box.minX, box.maxY, box.minZ);
        glVertex3d(box.maxX, box.maxY, box.minZ);
        glVertex3d(box.maxX, box.maxY, box.maxZ);
        glVertex3d(box.minX, box.maxY, box.maxZ);
        glVertex3d(box.minX, box.maxY, box.minZ);
        glEnd();
        glBegin(GL_LINES);
        glVertex3d(box.minX, box.minY, box.minZ);
        glVertex3d(box.minX, box.maxY, box.minZ);
        glVertex3d(box.maxX, box.minY, box.minZ);
        glVertex3d(box.maxX, box.maxY, box.minZ);
        glVertex3d(box.maxX, box.minY, box.maxZ);
        glVertex3d(box.maxX, box.maxY, box.maxZ);
        glVertex3d(box.minX, box.minY, box.maxZ);
        glVertex3d(box.minX, box.maxY, box.maxZ);
        glEnd();
    }

    public static void renderCrosses(AxisAlignedBB box) {
        glBegin(GL_LINES);
        glVertex3d(box.maxX, box.maxY, box.maxZ);
        glVertex3d(box.maxX, box.minY, box.minZ);
        glVertex3d(box.maxX, box.maxY, box.minZ);

        glVertex3d(box.minX, box.maxY, box.maxZ);
        glVertex3d(box.minX, box.maxY, box.minZ);
        glVertex3d(box.maxX, box.minY, box.minZ);

        glVertex3d(box.minX, box.minY, box.maxZ);
        glVertex3d(box.maxX, box.maxY, box.maxZ);
        glVertex3d(box.minX, box.minY, box.maxZ);

        glVertex3d(box.minX, box.maxY, box.minZ);
        glVertex3d(box.minX, box.minY, box.minZ);
        glVertex3d(box.maxX, box.minY, box.maxZ);
        glEnd();
    }

    public static void drawBox(AxisAlignedBB box) {
        if (box == null) {
            return;
        }
        // back
        glBegin(GL_QUADS);
        glVertex3d(box.minX, box.minY, box.maxZ);
        glVertex3d(box.maxX, box.minY, box.maxZ);
        glVertex3d(box.maxX, box.maxY, box.maxZ);
        glVertex3d(box.minX, box.maxY, box.maxZ);
        glEnd();
        glBegin(GL_QUADS);
        glVertex3d(box.maxX, box.minY, box.maxZ);
        glVertex3d(box.minX, box.minY, box.maxZ);
        glVertex3d(box.minX, box.maxY, box.maxZ);
        glVertex3d(box.maxX, box.maxY, box.maxZ);
        glEnd();
        // left
        glBegin(GL_QUADS);
        glVertex3d(box.minX, box.minY, box.minZ);
        glVertex3d(box.minX, box.minY, box.maxZ);
        glVertex3d(box.minX, box.maxY, box.maxZ);
        glVertex3d(box.minX, box.maxY, box.minZ);
        glEnd();
        glBegin(GL_QUADS);
        glVertex3d(box.minX, box.minY, box.maxZ);
        glVertex3d(box.minX, box.minY, box.minZ);
        glVertex3d(box.minX, box.maxY, box.minZ);
        glVertex3d(box.minX, box.maxY, box.maxZ);
        glEnd();
        // right
        glBegin(GL_QUADS);
        glVertex3d(box.maxX, box.minY, box.maxZ);
        glVertex3d(box.maxX, box.minY, box.minZ);
        glVertex3d(box.maxX, box.maxY, box.minZ);
        glVertex3d(box.maxX, box.maxY, box.maxZ);
        glEnd();
        glBegin(GL_QUADS);
        glVertex3d(box.maxX, box.minY, box.minZ);
        glVertex3d(box.maxX, box.minY, box.maxZ);
        glVertex3d(box.maxX, box.maxY, box.maxZ);
        glVertex3d(box.maxX, box.maxY, box.minZ);
        glEnd();
        // front
        glBegin(GL_QUADS);
        glVertex3d(box.minX, box.minY, box.minZ);
        glVertex3d(box.maxX, box.minY, box.minZ);
        glVertex3d(box.maxX, box.maxY, box.minZ);
        glVertex3d(box.minX, box.maxY, box.minZ);
        glEnd();
        glBegin(GL_QUADS);
        glVertex3d(box.maxX, box.minY, box.minZ);
        glVertex3d(box.minX, box.minY, box.minZ);
        glVertex3d(box.minX, box.maxY, box.minZ);
        glVertex3d(box.maxX, box.maxY, box.minZ);
        glEnd();
        // top
        glBegin(GL_QUADS);
        glVertex3d(box.minX, box.maxY, box.minZ);
        glVertex3d(box.maxX, box.maxY, box.minZ);
        glVertex3d(box.maxX, box.maxY, box.maxZ);
        glVertex3d(box.minX, box.maxY, box.maxZ);
        glEnd();

        glBegin(GL_QUADS);
        glVertex3d(box.maxX, box.maxY, box.minZ);
        glVertex3d(box.minX, box.maxY, box.minZ);
        glVertex3d(box.minX, box.maxY, box.maxZ);
        glVertex3d(box.maxX, box.maxY, box.maxZ);
        glEnd();

        // bottom
        glBegin(GL_QUADS);
        glVertex3d(box.minX, box.minY, box.minZ);
        glVertex3d(box.maxX, box.minY, box.minZ);
        glVertex3d(box.maxX, box.minY, box.maxZ);
        glVertex3d(box.minX, box.minY, box.maxZ);
        glEnd();

        glBegin(GL_QUADS);
        glVertex3d(box.maxX, box.minY, box.minZ);
        glVertex3d(box.minX, box.minY, box.minZ);
        glVertex3d(box.minX, box.minY, box.maxZ);
        glVertex3d(box.maxX, box.minY, box.maxZ);
        glEnd();
    }
}
