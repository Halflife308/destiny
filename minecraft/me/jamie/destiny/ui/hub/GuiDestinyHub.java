package me.jamie.destiny.ui.hub;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.URL;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.ui.alt.GuiAltManager;
import me.jamie.destiny.ui.font.GuiFontManager;
import me.jamie.destiny.ui.menu.GuiDestinyMainMenu;
import me.jamie.destiny.ui.mod.GuiModuleManager;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.destiny.util.gui.Draw;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public class GuiDestinyHub extends GuiScreen {
	private boolean loaded = false;
	
	public void initGui() {
		Keyboard.enableRepeatEvents(true);
		
		this.buttonList.clear();
		int i = 36;
		this.buttonList.add(new GuiButton(1, 5, 36, 90, 20, "Accounts"));
		this.buttonList.add(new GuiButton(2, 5, i += 24, 90, 20, "Fonts"));
		this.buttonList.add(new GuiButton(3, 5, i += 24, 90, 20, "Modules"));
		this.buttonList.add(new GuiButton(4, 5, this.height - 36, 90, 20, "Back"));
	}
	
	@Override
	public void onGuiClosed() {
		loaded = false;
	}

	protected void actionPerformed(GuiButton button) throws IOException {
		if (!button.enabled)
			return;
		switch (button.id) {
		case 1: {
			Wrapper.getMinecraft().displayGuiScreen(new GuiAltManager());
			break;
		}
		case 2: {
			Wrapper.getMinecraft().displayGuiScreen(new GuiFontManager());
			break;
		}
		case 3: {
			Wrapper.getMinecraft().displayGuiScreen(new GuiModuleManager());
			break;
		}
		case 4: {
			Wrapper.getMinecraft().displayGuiScreen(new GuiDestinyMainMenu());
			break;
		}
		}
	}

	public void handleMouseInput() throws IOException {
		super.handleMouseInput();
	}

	protected void keyTyped(char par1, int par2) throws IOException {
		if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
			Wrapper.getMinecraft().displayGuiScreen(new GuiDestinyMainMenu());
		}
	}

	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		this.drawDefaultBackground();
		this.drawCenteredString(Wrapper.getFontRenderer(), "Destiny Hub", this.width / 2, 16, 16777215);
		Draw.drawBorderedRect(105, 33, this.width - 10, this.height - 16, 1, 16777215, 0x80000000);
		
		//if(!loaded) {
			try {
				loadChangelog();
			} catch (ConnectException e) {
				e.printStackTrace();
			}
		//}
		
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
	public void loadChangelog() throws ConnectException {
		try {
			URL url = new URL("http://snoxcraft.com/destiny/changelog.txt");
			BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
			StringBuilder sb = new StringBuilder();
			int y = 38;
			String temp;
			while ((temp = br.readLine()) != null) {
				if (temp.contains("+")) {
					temp = temp.replaceAll("\\+", "\\�a+�7");
				}
				String[] args = temp.split("Version");
				Wrapper.getFontRenderer().drawStringWithShadow(temp, 110, y, -1);
				y += 12;
			}
			loaded = true;
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
