package me.jamie.destiny.ui.alt;

import java.io.IOException;
import java.util.Random;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.alt.Alt;
import me.jamie.destiny.ui.hub.GuiDestinyHub;
import me.jamie.destiny.util.game.Wrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSlot;

public class GuiAltManager extends GuiScreen
{
	private Alt alt;
	private GuiAltManager.GuiAltSlot slots;
	
	public void initGui()
    {
        Keyboard.enableRepeatEvents(true);
       
        this.buttonList.add(new GuiButton(1, this.width / 2 - 154, this.height - 28, 70, 20, "Edit"));
        this.buttonList.add(new GuiButton(2, this.width / 2 - 74, this.height - 28, 70, 20, "Random"));
        this.buttonList.add(new GuiButton(3, this.width / 2 - 154, this.height - 52, 100, 20, "Add"));
        this.buttonList.add(new GuiButton(4, this.width / 2 - 50, this.height - 52, 100, 20, "Direct Login"));
        this.buttonList.add(new GuiButton(5, this.width / 2 + 4 + 50, this.height - 52, 100, 20, "Delete"));
        this.buttonList.add(new GuiButton(6, this.width / 2 + 4, this.height - 28, 70, 20, "�cImport"));
        this.buttonList.add(new GuiButton(7, this.width / 2 + 4 + 76, this.height - 28, 75, 20, "Cancel"));
        
        this.slots = new GuiAltManager.GuiAltSlot(Wrapper.getMinecraft());
        this.slots.registerScrollButtons(7, 8);
    }
	
	public void handleMouseInput() throws IOException {
		super.handleMouseInput();
		this.slots.func_178039_p();
	}
	
	protected void actionPerformed(GuiButton button) throws IOException
	{
		if(!button.enabled) return;
		switch(button.id) {
			case 3: {
				Destiny.getInstance().getLogin().setProgress("Add Account");
				Wrapper.getMinecraft().displayGuiScreen(new GuiAltAdd());
				break;
			}
			case 2: {
				Random random = new Random();
				int rndAlt = random.nextInt(Destiny.getInstance().getAltManager().getContents().size());
				setSelectedAlt(Destiny.getInstance().getAltManager().getContents().get(rndAlt));
				Destiny.getInstance().getLogin().login(alt.getLabel(), alt.getPassword());
				break;
			}
			case 4: {
				Destiny.getInstance().getLogin().setProgress("Direct Login");
				Wrapper.getMinecraft().displayGuiScreen(new GuiDirectLogin());
				break;
			}
			case 5: {
				if(alt == null)
					return;
				Destiny.getInstance().getAltManager().getContents().remove(alt);
				break;
			}
			case 7: {
				Wrapper.getMinecraft().displayGuiScreen(new GuiDestinyHub());
				break;
			}
			case 1: {
				Wrapper.getMinecraft().displayGuiScreen(new GuiAltEdit(this));
				break;
			}
			default: {
				this.slots.actionPerformed(button);
			}
		}
	}
	
	protected void keyTyped(char par1, int par2) throws IOException {
        if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
        	Wrapper.getMinecraft().displayGuiScreen(new GuiDestinyHub());
        }
    }
	
	public void drawScreen(int mouseX, int mouseY, float partialTicks) 
	{
		this.slots.drawScreen(mouseX, mouseY, partialTicks);
		this.drawCenteredString(Wrapper.getFontRenderer(), "Account Manager (�7" + Wrapper.getMinecraft().getSession().getUsername() + "�f)", this.width / 2, 16, 16777215);
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
	public Alt getSelectedAlt() {
		return alt;
	}
	
	public void setSelectedAlt(Alt alt) {
		this.alt = alt;
	}

	class GuiAltSlot extends GuiSlot
	{
		private Alt alt;
		
		public GuiAltSlot(Minecraft mcIn) {
			super(mcIn, GuiAltManager.this.width, GuiAltManager.this.height, 32, GuiAltManager.this.height - 65 + 4, 24);
		}

	    protected int getSize()
	    {
	        return Destiny.getInstance().getAltManager().getContents().size();
	    }

	    protected void elementClicked(int slotIndex, boolean isDoubleClick, int mouseX, int mouseY)
	    {
	        alt = (Alt)Destiny.getInstance().getAltManager().getContents().get((slotIndex));
	        GuiAltManager.this.setSelectedAlt(alt);
	        String username = alt.getLabel();
	        String password = alt.getPassword();
	        if(isDoubleClick)
	        	Destiny.getInstance().getLogin().login(username, password);
	    }

	    protected boolean isSelected(int slotIndex)
	    {
	        return Destiny.getInstance().getAltManager().getContents().get(slotIndex).equals(GuiAltManager.this.getSelectedAlt());
	    }

	    protected int getContentHeight()
	    {
	        return this.getSize() * 18;
	    }

	    protected void drawBackground()
	    {
	        Wrapper.getMinecraft().currentScreen.drawDefaultBackground();
	    }

	    protected void drawSlot(int p_180791_1_, int p_180791_2_, int p_180791_3_, int p_180791_4_, int p_180791_5_, int p_180791_6_)
	    {
	        GuiAltManager.this.drawCenteredString(Wrapper.getFontRenderer(), ((Alt)Destiny.getInstance().getAltManager().getContents().get(p_180791_1_)).getLabel().toString(), this.width / 2, p_180791_3_ + 2, 16777215);
	        GuiAltManager.this.drawCenteredString(Wrapper.getFontRenderer(), "�7" + ((Alt)Destiny.getInstance().getAltManager().getContents().get(p_180791_1_)).getPassword().replaceAll(".", "*"), this.width / 2, p_180791_3_ + 12, 16777215);
	    }
	}

}
