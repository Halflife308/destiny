package me.jamie.destiny.ui.alt;

import java.io.IOException;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.ui.menu.GuiDestinyMainMenu;
import me.jamie.destiny.util.game.Wrapper;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;

public class GuiDirectLogin extends GuiScreen
{
	private GuiTextField usernameField;
	private GuiPasswordField passwordField;

	public void initGui() 
	{
		Keyboard.enableRepeatEvents(true);
        this.buttonList.clear();
        this.buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height / 4 + 96 + 12, "Login"));
        this.buttonList.add(new GuiButton(1, this.width / 2 - 100, this.height / 4 + 120 + 12, "Back"));
        this.usernameField = new GuiTextField(2, this.fontRendererObj, this.width / 2 - 100, 76, 200, 20);
        this.usernameField.setFocused(true);
        this.passwordField = new GuiPasswordField(2, this.fontRendererObj, this.width / 2 - 100, 116, 200, 20);
	}
	
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	 {
	        this.drawDefaultBackground();
	        this.drawCenteredString(this.fontRendererObj, Destiny.getInstance().getLogin().getProgress(), this.width / 2, 20, 16777215);
	        this.drawString(this.fontRendererObj, "Username:", this.width / 2 - 100, 63, 10526880);
	        this.drawString(this.fontRendererObj, "Password:", this.width / 2 - 100, 104, 10526880);
	        this.usernameField.drawTextBox();
	        this.passwordField.drawTextBox();
	        super.drawScreen(mouseX, mouseY, partialTicks);
	 }
	
	protected void actionPerformed(GuiButton button) throws IOException
	{
		if(button.id == 1)
		{
			Wrapper.getMinecraft().displayGuiScreen(new GuiAltManager());
			Destiny.getInstance().getLogin().setProgress("Login");
		} else if(button.id == 0)
		{
			if((!usernameField.getText().isEmpty() && (!passwordField.getText().isEmpty())))
			{
				String name = usernameField.getText();
				String password = passwordField.getText();
				Destiny.getInstance().getLogin().login(name, password);
			} else {
				Destiny.getInstance().getLogin().setProgress("�cEnter valid credidentials");
			}
		}
	}
	
    protected void keyTyped(char par1, int par2) throws IOException {
        usernameField.textboxKeyTyped(par1, par2);
        passwordField.textboxKeyTyped(par1, par2);
        if (par1 == '\t' && (this.usernameField.isFocused() || this.passwordField.isFocused())) {
            this.usernameField.setFocused(!this.usernameField.isFocused());
            this.passwordField.setFocused(!this.passwordField.isFocused());
        }

        if (par1 == '\r') {
            actionPerformed((GuiButton)buttonList.get(0));
        }
        
        if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
        	Wrapper.getMinecraft().displayGuiScreen(new GuiAltManager());
        }
    }
	
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
    {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.usernameField.mouseClicked(mouseX, mouseY, mouseButton);
        this.passwordField.mouseClicked(mouseX, mouseY, mouseButton);
    }
}
