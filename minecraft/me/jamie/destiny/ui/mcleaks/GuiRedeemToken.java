package me.jamie.destiny.ui.mcleaks;

import java.io.IOException;
import java.net.URI;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.events.render.gui.GuiOpenEvent;
import me.jamie.destiny.ui.menu.GuiDestinyMainMenu;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.destiny.util.mcleaks.Connector;
import me.jamie.destiny.util.mcleaks.RedeemResponse;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;

public class GuiRedeemToken extends GuiScreen
{
	private String name;
    private final boolean sessionRestored;
    private final String message;
    private GuiTextField tokenField;
    private GuiButton restoreButton;
    
    public GuiRedeemToken(final boolean sessionRestored) {
        this(sessionRestored, null);
    }
    
    public GuiRedeemToken(final boolean sessionRestored, final String message) {
        this.sessionRestored = sessionRestored;
        this.message = message;
    }
    
    public void updateScreen() {
        this.tokenField.updateCursorCounter();
    }
    
    public void initGui() {
        Keyboard.enableRepeatEvents(true);
        this.restoreButton = new GuiButton(0, this.width / 2 - 150, this.height / 4 + 96 + 18, 128, 20, this.sessionRestored ? "Session restored!" : "Restore Session");
        this.restoreButton.enabled = (Destiny.getInstance().getSessionManager().getSavedSession() != null);
        this.buttonList.add(this.restoreButton);
        this.buttonList.add(new GuiButton(1, this.width / 2 - 18, this.height / 4 + 96 + 18, 168, 20, "Redeem Token"));
        this.buttonList.add(new GuiButton(2, this.width / 2 - 150, this.height / 4 + 120 + 18, 158, 20, "Get Token"));
        this.buttonList.add(new GuiButton(3, this.width / 2 + 12, this.height / 4 + 120 + 18, 138, 20, "Cancel"));
        (this.tokenField = new GuiTextField(0, Wrapper.getFontRenderer(), this.width / 2 - 100, 128, 200, 20)).setFocused(true);
    }
    
    public void onGuiClosed() {
        Keyboard.enableRepeatEvents(false);
    }
    
    protected void actionPerformed(final GuiButton button) throws IOException {
        if (button.enabled) {
            if (button.id == 0) {
            	final GuiOpenEvent event = new GuiOpenEvent(this);
            	event.setCancelled(true);
            	Destiny.getInstance().getSessionManager().setMCLeaksSession(null);
            	Destiny.getInstance().getSessionManager().setName(null);
                Destiny.getInstance().getSessionManager().setSession(Destiny.getInstance().getSessionManager().getSavedSession());
                Destiny.getInstance().getSessionManager().setSavedSession(null);
                Wrapper.getMinecraft().displayGuiScreen((GuiScreen)new GuiRedeemToken(true));
            }
            else if (button.id == 1) {
                if (this.tokenField.getText().length() != 16) {
                    Wrapper.getMinecraft().displayGuiScreen((GuiScreen)new GuiRedeemToken(false, "�cThe token has to be 16 characters long!"));
                    return;
                }
                button.enabled = false;
                button.displayString = "Please wait ...";
                Connector.redeem(this.tokenField.getText(), new Callback()
                {
					@Override
					public void done(Object o) {
						if (o instanceof String) {
							Wrapper.getMinecraft().displayGuiScreen((GuiScreen)new GuiRedeemToken(false, "�c" + o));
				            return;
				        }
				        if (Destiny.getInstance().getSessionManager().getSavedSession() == null) {
				        	Destiny.getInstance().getSessionManager().setSavedSession(Wrapper.getMinecraft().getSession());
				        }
				        final RedeemResponse response = (RedeemResponse)o;
				        Destiny.getInstance().getSessionManager().setName(response.getMcName());
				        Destiny.getInstance().getSessionManager().setMCLeaksSession(response.getSession());
				        Wrapper.getMinecraft().displayGuiScreen(new GuiRedeemToken(false, "�aYour token was redeemed successfully!"));
					}
                });
            }
            else if (button.id == 2) {
                try {
                    final Class<?> oclass = Class.forName("java.awt.Desktop");
                    final Object object = oclass.getMethod("getDesktop", (Class<?>[])new Class[0]).invoke(null, new Object[0]);
                    oclass.getMethod("browse", URI.class).invoke(object, new URI("https://mcleaks.net/"));
                }
                catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
            else if (button.id == 3) {
                this.mc.displayGuiScreen((GuiScreen)new GuiDestinyMainMenu());
            }
        }
    }
    
    protected void keyTyped(final char typedChar, final int keyCode) throws IOException {
        this.tokenField.textboxKeyTyped(typedChar, keyCode);
        if (keyCode == 15) {
            this.tokenField.setFocused(!this.tokenField.isFocused());
        }
        if (keyCode == 28 || keyCode == 156) {
            this.actionPerformed((GuiButton)this.buttonList.get(1));
        }
    }
    
    protected void mouseClicked(final int mouseX, final int mouseY, final int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.tokenField.mouseClicked(mouseX, mouseY, mouseButton);
    }
    
    public void drawScreen(final int mouseX, final int mouseY, final float partialTicks) {
        this.drawDefaultBackground();
        this.drawCenteredString(Wrapper.getFontRenderer(), "�f- �bMCLeaks�f.�bnet�f -", this.width / 2, 17, 16777215);
        this.drawCenteredString(Wrapper.getFontRenderer(), "Free minecraft accounts", this.width / 2, 32, 16777215);
        this.drawCenteredString(Wrapper.getFontRenderer(), "Status:", this.width / 2, 68, 16777215);
        this.drawCenteredString(Wrapper.getFontRenderer(), getCurrentStatus(), this.width / 2, 78, 16777215);
        this.drawString(Wrapper.getFontRenderer(), "Token", this.width / 2 - 100, 115, 10526880);
        if (this.message != null) {
            this.drawCenteredString(Wrapper.getFontRenderer(), this.message, this.width / 2, 158, 16777215);
        }
        this.tokenField.drawTextBox();
        super.drawScreen(mouseX, mouseY, partialTicks);
    }
    
    public String getCurrentStatus() {
        String status = "�6No Token redeemed. Using �e" + (Wrapper.getMinecraft().getSession().getUsername() != null ? Wrapper.getMinecraft().getSession().getUsername() : " ") + " �6to login!";
        if (Destiny.getInstance().getSessionManager().getMCLeaksSession() != null) {
            status = "�aToken active. Using �b" + Destiny.getInstance().getSessionManager().getName() + " �2to login!";
        }
        return status;
    }
}
