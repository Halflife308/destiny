package me.jamie.destiny.ui.mcleaks;

public interface Callback<T>
{
    void done(final T p0);
}
