package me.jamie.destiny.ui.font;

import java.awt.Font;
import java.io.IOException;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.ui.hub.GuiDestinyHub;
import me.jamie.destiny.ui.menu.GuiDestinyIngameMenu;
import me.jamie.destiny.util.font.MinecraftFontRenderer;
import me.jamie.destiny.util.game.Wrapper;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;

public class GuiFontManager extends GuiScreen
{
	private GuiTextField name, size;
	private boolean global;
	
	public void initGui() 
	{
		Keyboard.enableRepeatEvents(true);
        this.buttonList.clear();
        this.buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height / 4 + 96 + 12, 98, 20, "Set Font"));
        this.buttonList.add(new GuiButton(1, this.width / 2 - 100, this.height / 4 + 120 + 12, "Back"));
        this.buttonList.add(new GuiButton(2, this.width / 2 + 2, this.height / 4 + 96 + 12, 98, 20, "Global TTF"));
        
        this.name = new GuiTextField(2, this.fontRendererObj, this.width / 2 - 100, 76, 100, 20);
        this.name.setFocused(true);
        this.size = new GuiTextField(3, this.fontRendererObj, this.width / 2 - 100, 116, 100, 20);
	
        if(Destiny.getInstance().getFontManager().getTTF() == null) return;
        this.name.setText(Wrapper.getFontRenderer().getSelectedFont().getFont().getFontName());
        this.size.setText(String.valueOf(Wrapper.getFontRenderer().getSelectedFont().getFont().getSize()));
	}
	
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	 {
	        this.drawDefaultBackground();
	        this.drawCenteredString(this.fontRendererObj, Destiny.getInstance().getLogin().getProgress(), this.width / 2, 20, 16777215);
	        this.drawString(this.fontRendererObj, "Name:", this.width / 2 - 100, 63, 10526880);
	        this.drawString(this.fontRendererObj, "Size:", this.width / 2 - 100, 104, 10526880);
	        this.name.drawTextBox();
	        this.size.drawTextBox();
	        super.drawScreen(mouseX, mouseY, partialTicks);
	 }
	
	protected void actionPerformed(GuiButton button) throws IOException
	{
		switch(button.id) {
			case 0: 
			{
				if((!name.getText().isEmpty() && (!size.getText().isEmpty())))
				{
					Wrapper.getFontRenderer().setSelectedFont(new MinecraftFontRenderer(new Font(name.getText(), 0, Integer.valueOf(size.getText())), true, false));
				}
				break;
			}
			case 1: {
				if(Wrapper.getWorld() == null) {
					Wrapper.getMinecraft().displayGuiScreen(new GuiDestinyHub());
				} else {
					Wrapper.getMinecraft().displayGuiScreen(new GuiDestinyIngameMenu(this));
				}
				break;
			}
			case 2: {
				Destiny.getInstance().getFontManager().setGlobalTTF(!Destiny.getInstance().getFontManager().isGlobalTTF());
				break;
			}
		}
	}
	
   protected void keyTyped(char par1, int par2) throws IOException {
       name.textboxKeyTyped(par1, par2);
       size.textboxKeyTyped(par1, par2);
       if (par1 == '\t' && (this.name.isFocused() || this.size.isFocused())) {
           this.name.setFocused(!this.name.isFocused());
           this.size.setFocused(!this.size.isFocused());
       }

       if (par1 == '\r') {
           actionPerformed((GuiButton)buttonList.get(0));
       }
       
       if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
    	   if(Wrapper.getWorld() == null) {
				Wrapper.getMinecraft().displayGuiScreen(new GuiDestinyHub());
			} else {
				Wrapper.getMinecraft().displayGuiScreen(new GuiDestinyIngameMenu(this));
			}
       }
   }
	
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
   {
       super.mouseClicked(mouseX, mouseY, mouseButton);
       this.name.mouseClicked(mouseX, mouseY, mouseButton);
       this.size.mouseClicked(mouseX, mouseY, mouseButton);
   }
}
