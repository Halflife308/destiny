package me.jamie.destiny.ui.menu;

import java.io.IOException;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.ui.font.GuiFontManager;
import me.jamie.destiny.ui.hub.GuiDestinyHub;
import me.jamie.destiny.ui.mod.GuiModuleManager;
import me.jamie.destiny.util.game.Wrapper;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraft.client.gui.GuiOptionButton;
import net.minecraft.client.gui.GuiScreen;

public class GuiDestinyIngameMenu extends GuiScreen
{
	private GuiScreen screen;
	
	public GuiDestinyIngameMenu(GuiScreen screen) {
		this.screen = screen;
	}
	
	public void initGui()
    {
		this.buttonList.clear();
        this.buttonList.add(new GuiOptionButton(1, this.width / 2 - 50, this.height / 4 + 8, 98, 20, "Fonts"));
        this.buttonList.add(new GuiOptionButton(2, this.width / 2 - 50, this.height / 4 + 35, 98, 20, "Modules"));
        this.buttonList.add(new GuiOptionButton(3, this.width / 2 - 50, this.height / 2 + 50, 98, 20, "Back"));
    }

	public void drawScreen(int mouseX, int mouseY, float partialTicks)
    {
		this.drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTicks);
    }

	protected void actionPerformed(GuiButton button) throws IOException
	{
		if(!button.enabled) return;
		switch(button.id) {
			case 1: {
				Wrapper.getMinecraft().displayGuiScreen(new GuiFontManager());
				break;
			}
			case 2: {
				Wrapper.getMinecraft().displayGuiScreen(new GuiModuleManager());
				break;
			}
			case 3: {
				 Wrapper.getMinecraft().displayGuiScreen(new GuiIngameMenu());
				 break;
			}
		}
	}

	protected void keyTyped(char par1, int par2) throws IOException {
        if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
        	Wrapper.getMinecraft().displayGuiScreen(new GuiIngameMenu());
        }
    }
}
