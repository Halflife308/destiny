package me.jamie.destiny.ui.mod;

import java.io.IOException;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.ui.alt.GuiAltManager;
import me.jamie.destiny.ui.alt.GuiPasswordField;
import me.jamie.destiny.util.game.Wrapper;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;

public class GuiModuleEdit extends GuiScreen 
{
	private GuiModuleManager gui;
	
	private GuiTextField keybind, color, visible;
	
	public GuiModuleEdit(GuiModuleManager gui) {
		this.gui = gui;
	}
	
	public void initGui() 
	{
		Keyboard.enableRepeatEvents(true);
        this.buttonList.clear();
        this.buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height / 4 + 96 + 12, 98, 20, "Edit"));
        this.buttonList.add(new GuiButton(1, this.width / 2 + 2, this.height / 4 + 96 + 12, 98, 20, "Clear Bind"));
        this.buttonList.add(new GuiButton(2, this.width / 2 - 100, this.height / 4 + 120 + 12, "Back"));
        
        this.keybind = new GuiTextField(2, this.fontRendererObj, this.width / 2 - 100, 76, 200, 20);
        this.keybind.setFocused(true);
        
        this.visible = new GuiTextField(2, this.fontRendererObj, this.width / 2 - 100, 116, 200, 20);
        
        if(gui.getSelectedModule() == null) return;
        this.keybind.setText(Keyboard.getKeyName(gui.getSelectedModule().getBind()).toUpperCase());
        this.visible.setText(gui.getSelectedModule().isVisible() ? "True" : "False");
	}
	
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	 {
	        this.drawDefaultBackground();
	        this.drawCenteredString(Wrapper.getFontRenderer(), "Edit �c" + gui.getSelectedModule().getLabel(), this.width / 2, 16, 16777215);
	        this.drawString(this.fontRendererObj, "Keybind:", this.width / 2 - 100, 63, 10526880);
	        this.drawString(this.fontRendererObj, "Visible:", this.width / 2 - 100, 104, 10526880);
	        this.keybind.drawTextBox();
	        this.visible.drawTextBox();
	        super.drawScreen(mouseX, mouseY, partialTicks);
	 }
	
	protected void actionPerformed(GuiButton button) throws IOException
	{
		if(button.id == 2)
		{
			Wrapper.getMinecraft().displayGuiScreen(new GuiModuleManager());
		} else if(button.id == 0)
		{
			if(!keybind.getText().isEmpty()) {
				gui.getSelectedModule().setBind(Keyboard.getKeyIndex(keybind.getText().toUpperCase()));
			}
			if(!visible.getText().isEmpty()) {
				gui.getSelectedModule().setVisible(Boolean.valueOf(visible.getText()));
			}
		} else if(button.id == 1) {
			gui.getSelectedModule().setBind(0);
			this.keybind.setText(Keyboard.getKeyName(gui.getSelectedModule().getBind()).toUpperCase());
		}
	}
	
    protected void keyTyped(char par1, int par2) throws IOException {
        keybind.textboxKeyTyped(par1, par2);
        visible.textboxKeyTyped(par1, par2);
        
        if (par1 == '\t' && (this.keybind.isFocused())) {
            this.keybind.setFocused(!this.keybind.isFocused());
        }
        
        if (par1 == '\t' && (this.visible.isFocused())) {
            this.visible.setFocused(!this.visible.isFocused());
        }

        if (par1 == '\r') {
            actionPerformed((GuiButton)buttonList.get(1));
        }
        
        if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
        	Wrapper.getMinecraft().displayGuiScreen(new GuiModuleManager());
        }
    }
	
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
    {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.keybind.mouseClicked(mouseX, mouseY, mouseButton);
        this.visible.mouseClicked(mouseX, mouseY, mouseButton);
    }
}
