package me.jamie.destiny.ui.mod;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.ui.hub.GuiDestinyHub;
import me.jamie.destiny.ui.menu.GuiDestinyIngameMenu;
import me.jamie.destiny.util.game.Wrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSlot;

public class GuiModuleManager extends GuiScreen
{
	private Module module;
	private GuiModuleManager.GuiModuleSlot slots;
	
	public void initGui()
    {
        Keyboard.enableRepeatEvents(true);
       
        this.buttonList.add(new GuiButton(1, this.width / 2 - 100, this.height - 58, "Edit"));
        this.buttonList.add(new GuiButton(7, this.width / 2 - 100, this.height - 28, "Cancel"));
        
        this.slots = new GuiModuleManager.GuiModuleSlot(Wrapper.getMinecraft());
        this.slots.registerScrollButtons(7, 8);
   
        Collections.sort(Destiny.getInstance().getModuleManager().getContents(), new Comparator<Module>() {
	        @Override
	        public int compare(Module m1, Module m2) {
	            return m1.getLabel().compareToIgnoreCase(m2.getLabel());
	        }
	    });
    }
	
	public void handleMouseInput() throws IOException {
		super.handleMouseInput();
		this.slots.func_178039_p();
	}
	
	protected void actionPerformed(GuiButton button) throws IOException
	{
		if(!button.enabled) return;
		switch(button.id) {
			case 1: {
				if(getSelectedModule() == null) return;
				Wrapper.getMinecraft().displayGuiScreen(new GuiModuleEdit(this));
				break;
			}
			case 7: {
				if(Wrapper.getWorld() == null) {
					Wrapper.getMinecraft().displayGuiScreen(new GuiDestinyHub());
				} else {
					Wrapper.getMinecraft().displayGuiScreen(new GuiDestinyIngameMenu(this));
				}
				break;
			}
			default: {
				this.slots.actionPerformed(button);
			}
		}
	}
	
	protected void keyTyped(char par1, int par2) throws IOException {
        if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
        	if(Wrapper.getWorld() == null) {
				Wrapper.getMinecraft().displayGuiScreen(new GuiDestinyHub());
			} else {
				Wrapper.getMinecraft().displayGuiScreen(new GuiDestinyIngameMenu(this));
			}
        }
    }
	
	public void drawScreen(int mouseX, int mouseY, float partialTicks) 
	{
		this.slots.drawScreen(mouseX, mouseY, partialTicks);
		this.drawCenteredString(Wrapper.getFontRenderer(), "Module Manager", this.width / 2, 16, 16777215);
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
	public Module getSelectedModule() {
		return module;
	}
	
	public void setSelectedModule(Module module) {
		this.module = module;
	}
	
	class GuiModuleSlot extends GuiSlot
	{
		private Module module;
		
		public GuiModuleSlot(Minecraft mcIn) {
			super(mcIn, GuiModuleManager.this.width, GuiModuleManager.this.height, 32, GuiModuleManager.this.height - 65 + 4, 18);
		}

	    protected int getSize()
	    {
	        return Destiny.getInstance().getModuleManager().getContents().size();
	    }

	    protected void elementClicked(int slotIndex, boolean isDoubleClick, int mouseX, int mouseY)
	    {
	        module = (Module)Destiny.getInstance().getModuleManager().getContents().get((slotIndex));
	        GuiModuleManager.this.setSelectedModule(module);
	        if(isDoubleClick)
	        	Wrapper.getMinecraft().displayGuiScreen(new GuiModuleEdit(GuiModuleManager.this));
	    }

	    protected boolean isSelected(int slotIndex)
	    {
	        return Destiny.getInstance().getModuleManager().getContents().get(slotIndex).equals(GuiModuleManager.this.getSelectedModule());
	    }

	    protected int getContentHeight()
	    {
	        return this.getSize() * 18;
	    }

	    protected void drawBackground()
	    {
	        Wrapper.getMinecraft().currentScreen.drawDefaultBackground();
	    }

	    protected void drawSlot(int p_180791_1_, int p_180791_2_, int p_180791_3_, int p_180791_4_, int p_180791_5_, int p_180791_6_)
	    {
	    	Module m = (Module)Destiny.getInstance().getModuleManager().getContents().get(p_180791_1_);
	    	GuiModuleManager.this.drawCenteredString(Wrapper.getFontRenderer(), m.getLabel().toString() + " �7(" + (m.getBind() != 0 ? "�a" + Keyboard.getKeyName(m.getBind()) : "�eNONE") + "�7)", this.width / 2, p_180791_3_ + 2, m.getColor() != 0 ? m.getColor() : 16777215);
	    }
	}
}
