package me.jamie.destiny.ui.click.panel;

import java.util.LinkedList;
import java.util.List;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.ui.click.element.Element;
import me.jamie.destiny.util.font.NahrFont.FontType;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.destiny.util.gui.Draw;
import me.jamie.lib.interfaces.Labelled;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.util.ResourceLocation;

public class Panel implements Labelled	
{	private String label;
	private int x, y, xx, yy, width, height;
	private boolean open, enabled, drag;
	
	private List<Element> elements;
	
	public Panel(String label, int x, int y, boolean open) {
		this.elements = new LinkedList<>();
		this.label = label;
		this.x = x;
		this.y = y;
		this.width = 90;
		this.height = 18;
		this.open = open;
		loadElements();
	}
	
	public void drawScreen(int mX, int mY, float ticks) 
	{
		if(drag) {
			this.x = xx + mX;
			this.y = yy + mY;
		}
		
		Draw.drawBorderedRect(this.x, this.y, this.x + this.width, this.y + this.height - 4, 1.0F, 0xFF242424, 0xFF242424);
		Destiny.getInstance().getGuiClick().getTitleFont().drawString(this.label, this.x + 3, this.y - 2, FontType.SHADOW_THIN, -1);
		
		if(open) {
			Draw.drawBorderedRect(this.x, this.y + 12.5, this.x + this.width, this.open ? this.y + this.height + (getElementHeight() - 2) : this.y + this.height - 1, 1.0F, 0xFF242424, 0xFF242424);
			
			int y1 = this.y + this.height - 2;
			for(Element e : this.elements) {
				e.setX(this.x + 2);
				e.setY(y1);
				e.setWidth(this.width - 4);
				e.drawScreen(mX, mY, ticks);
				y1 += e.getHeight() + 2;
			}
		}
	}
	
	public void loadElements() {}
	
	public void mouseClicked(int mX, int mY, int button) {
		/*
		switch(button) {
			case 0: {
				if(isSelected(mX, mY)) {
					this.xx = this.x - mX;
					this.yy = this.y - mY;
					this.drag = true;
				}
				if(this.open) {
					this.elements.forEach(e -> e.mouseClicked(mX, mY, button));
				}
				break;
			}
			case 1: {
				if(!isSelected(mX, mY)) return;
				System.out.println("Skeen");
				this.open = !this.open;
				if(this.open) {
					this.elements.forEach(e -> e.mouseClicked(mX, mY, button));
				}
				Wrapper.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.createPositionedSoundRecord(new ResourceLocation("gui.button.press"), 1.0F));
				break;
			}
		} */
		
		if(button == 0 && isSelected(mX, mY)) {
			this.xx = this.x - mX;
			this.yy = this.y - mY;
			this.drag = true;
		}
		if(button == 1 && isSelected(mX, mY)) {
			this.open = !this.open;
			Wrapper.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.createPositionedSoundRecord(new ResourceLocation("gui.button.press"), 1.0F));
		}
		
		if(this.open) {
			this.elements.forEach(e -> e.mouseClicked(mX, mY, button));
		}
	}  
	
	public void mouseReleased(int mX, int mY, int button) {
		switch(button) {
			case 0: {
				this.drag = false;
				break;
			}
		}
		
		if(this.open) {
			this.elements.forEach(e -> e.mouseReleased(mX, mY, button));
		}
	}
	
	private boolean isSelected(int mX, int mY) {
		return mX >= this.x && mX <= this.x + this.width && mY >= this.y && mY <= this.y + this.height;
	}
	
	private float getElementHeight() {
		float h = 0;
		for(Element e : this.elements) {
			h += e.getHeight() + 2;
		}
		return h;
	}
	
	@Override
	public String getLabel() {
		return this.label;
	}

	public List<Element> getElements() {
		return elements;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}
	
	public boolean isDragging() {
		return drag;
	}
}
