package me.jamie.destiny.ui.click.element;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.ui.click.panel.Panel;
import me.jamie.lib.interfaces.Labelled;

public abstract class Element implements Labelled
{
	private String label;
	private float x, y;
	private int width, height;
	
	public Element(String label) {
		this.label = label;
	}

	public abstract void drawScreen(int mX, int mY, float ticks);
	
	public abstract void mouseClicked(int mX, int mY, int button);
	
	public abstract void mouseReleased(int mX, int mY, int button);
	
	@Override
	public String getLabel() {
		return this.label;
	}
	
	public float getX() {
		return x;
	}
	
	public void setX(float x) {
		this.x = x;
	}
	
	public float getY() {
		return y;
	}
	
	public void setY(float y) {
		this.y = y;
	}
	
	public int getWidth() {
		return width;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
}
