package me.jamie.destiny.ui.click.element.elements;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.ui.click.element.Element;
import me.jamie.destiny.ui.click.element.option.Option;
import me.jamie.destiny.ui.click.element.option.options.OptionBoolean;
import me.jamie.destiny.ui.click.element.option.options.OptionFloat;
import me.jamie.destiny.ui.click.element.option.options.OptionInt;
import me.jamie.destiny.ui.click.element.option.options.OptionMulti;
import me.jamie.destiny.ui.click.panel.Panel;
import me.jamie.destiny.util.font.NahrFont.FontType;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.destiny.util.gui.Draw;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.util.ResourceLocation;

public class ElementToggle extends Element 
{
	private List<Option> options;
	private Module module;
	private boolean open;

	public ElementToggle(String label, Module module) {
		super(module.getLabel());
		this.options = new LinkedList<>();
		this.module = module;
		setHeight(15);
		loadOptions();
	}

	public void drawScreen(int mX, int mY, float ticks) 
	{		
		Draw.rect(this.getX(), this.getY() - 1, this.getX() + this.getWidth(), this.getY() + 14, this.module.isEnabled() ? (isSelected(mX, mY) ? 0xFF1B88E3 : 0xFF2F87CF) : (isSelected(mX, mY) ? 0xFF383737 : 0xFF2D2D2D));
		Destiny.getInstance().getGuiClick().getButtonFont().drawString(this.module.getLabel(), this.getX() + 2,
				this.getY() - 2.8F, FontType.NORMAL, -1);
		
		if(!this.module.getProperties().isEmpty()) 
			Destiny.getInstance().getGuiClick().getButtonFont().drawString("...", this.getX() + this.getWidth() - 8, this.getY() - 2.5F, FontType.NORMAL, -1);
		
		if(this.open) {	
			float height = 3.0F;
			for(Option option : this.options) {
				height += 15.0F;
				option.setX(getX() - 1);
				option.setY(getY() + height);
				option.setHeight(15);
				option.setWidth(getWidth() + 1);
				option.drawScreen(mX, mY, ticks);
			}
		}
	}
	
	public void loadOptions() {
		if(!module.getProperties().isEmpty()) {
			for(Property property : module.getProperties()) {
				if(property.getProperty() instanceof Boolean) {
					options.add(new OptionBoolean(property));
				}  else if(property.getProperty() instanceof Float) {
					options.add(new OptionFloat(property));
				} else if(property.getProperty() instanceof Integer) {
					options.add(new OptionInt(property));
				} else if(property.getProperty() instanceof String) {
					options.add(new OptionMulti(property));
				}
			}
		}
	}

	public void mouseClicked(int mX, int mY, int button) {
			switch(button) {
				case 0: {
					if(isSelected(mX, mY)) {
						this.module.toggle();
						Wrapper.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.createPositionedSoundRecord(new ResourceLocation("gui.button.press"), 1.0F));
					}
					break;
				}
				case 1: {
					if(isSelected(mX, mY) && !module.getProperties().isEmpty()) {
						this.open = !this.open;
					}
					break;
				}
			}
		if(this.open) {
			this.options.forEach(o -> o.mouseClicked(mX, mY, button));
		}
	}

	public void mouseReleased(int mX, int mY, int button) {
	}

	public int getHeight() {
		int h = 14;
		if(this.open) {
			for(Option option : this.options) {
				h += option.getHeight() + 1;
			}
			return h + 1;
		}
		return 14;
	}

	private boolean isSelected(int mX, int mY) {
		for (Panel panel : Destiny.getInstance().getGuiClick().getPanels())
			if (panel.isDragging())
				return false;
		return mX >= this.getX() && mX <= this.getX() + this.getWidth() && mY >= this.getY() && mY <= this.getY() + 14;
	}
}
