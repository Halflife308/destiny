package me.jamie.destiny.ui.click.element.option.options;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.Comparator;

import org.lwjgl.input.Mouse;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.ui.click.element.option.Option;
import me.jamie.destiny.ui.click.panel.Panel;
import me.jamie.destiny.util.font.NahrFont.FontType;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.destiny.util.gui.Draw;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.util.ResourceLocation;

public class OptionFloat extends Option
{
	private Property property;
	
	private boolean dragging;
	
	public OptionFloat(Property property) {
		super(property.getDisplay());
		this.property = property;
		setWidth(15);
		setHeight(14);
	}
	
	@Override
	public void drawScreen(int mX, int mY, float ticks) {
		final NumberFormat nf = new DecimalFormat("##.###");
		final float[] limit = new float[] { (Float)this.property.getMinimum(), (Float)this.property.getMaximum() };
	    final float valRel = property.getFloatProperty() - limit[0];
	    final float perc = valRel / (limit[1] - limit[0]);
	    final double valAbs = (this.getWidth() - 1.0) * perc;
	    
	    if(isSelected(mX, mY) && Mouse.isButtonDown(0)) 
	    	drag(mX, mY);
		Draw.rect(this.getX() + 2, this.getY() - 1, this.getX() + this.getWidth() - 1, this.getY() + this.getHeight() - 2.5F, (isSelected(mX, mY) ? 0xFF707070: 0xFF636363) );
	    Draw.rect(this.getX() + 2, this.getY() - 1, this.getX() + valAbs, this.getY() + this.getHeight() - 2.5F, (isSelected(mX, mY) ? 0xFF4393D4 : 0xFF589FD9) );
		Destiny.getInstance().getGuiClick().getButtonFont().drawString(this.property.getDisplay() + ": " + nf.format(this.property.getFloatProperty()), this.getX() + 4,
				this.getY() - 3.5F, FontType.NORMAL, -1);
	}

	@Override
	public void mouseClicked(int mX, int mY, int button) {
		switch(button) {
			case 0: {
				if(!isSelected(mX, mY)) return;
				this.dragging = true;
				Wrapper.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.createPositionedSoundRecord(new ResourceLocation("gui.button.press"), 1.0F));
				break;
			}
		}
	}

	@Override
	public void mouseReleased(int mX, int mY, int button) {
		this.dragging = false;

	}
	
	//Credits: DoubleParallax
	private void drag(int mX, int mY) {
		if (this.dragging) {
            final float[] limit = new float[] { (Float)this.property.getMinimum() + 1, (Float)this.property.getMaximum() };
            final float inc = (Float)this.property.getIncrement();
            final double valAbs = mX - (this.getX() + 1.0);
            double perc = valAbs / (this.getWidth() - 2.0);
            perc = Math.min(Math.max(0.0, perc), 1.0);
            final double valRel = (limit[1] - limit[0]) * perc;
            double val = limit[0] + valRel;
            val = Math.round(val * (1.0f / inc)) / (1.0f / inc);
            this.property.setProperty(val);
        }
	}
	
	private boolean isSelected(int mX, int mY) {
		for (Panel panel : Destiny.getInstance().getGuiClick().getPanels())
			if (panel.isDragging())
				return false;
		return mX >= this.getX() && mX <= this.getX() + this.getWidth() && mY >= this.getY() && mY <= this.getY() + this.getHeight();
	}
}
