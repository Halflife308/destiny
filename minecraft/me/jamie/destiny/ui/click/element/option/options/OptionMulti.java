package me.jamie.destiny.ui.click.element.option.options;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.ui.click.element.option.Option;
import me.jamie.destiny.ui.click.panel.Panel;
import me.jamie.destiny.util.font.NahrFont.FontType;
import me.jamie.destiny.util.game.Wrapper;
import me.jamie.destiny.util.gui.Draw;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.util.ResourceLocation;

public class OptionMulti extends Option
{
	private Property property;
	
	public OptionMulti(Property property) {
		super(property.getDisplay());
		this.property = property;
		setWidth(15);
		setHeight(14);
	}
	
	@Override
	public void drawScreen(int mX, int mY, float ticks) {
		Draw.rect(this.getX() + 2, this.getY() - 1, this.getX() + this.getWidth() - 1, this.getY() + this.getHeight() - 2.5F, (isSelected(mX, mY) ? 0xFF707070: 0xFF636363));
		Destiny.getInstance().getGuiClick().getButtonFont().drawString(this.property.getDisplay() + ": " + this.property.getCurrentProperty(), this.getX() + 4,
				this.getY() - 3.5F, FontType.NORMAL, -1);
	}

	@Override
	public void mouseClicked(int mX, int mY, int button) {
		switch(button) {
			case 0: {
				if(!isSelected(mX, mY)) return;
				property.increment();
				Wrapper.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.createPositionedSoundRecord(new ResourceLocation("gui.button.press"), 1.0F));
				break;
			}
			case 1: {
				if(!isSelected(mX, mY)) return;
				property.decrement();
			}
		}
	}

	@Override
	public void mouseReleased(int mX, int mY, int button) {
	}
	
	private boolean isSelected(int mX, int mY) {
		for (Panel panel : Destiny.getInstance().getGuiClick().getPanels())
			if (panel.isDragging())
				return false;
		return mX >= this.getX() && mX <= this.getX() + this.getWidth() && mY >= this.getY() && mY <= this.getY() + this.getHeight();
	}
}