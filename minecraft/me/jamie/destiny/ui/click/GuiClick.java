package me.jamie.destiny.ui.click;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.module.Category;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.ui.click.element.elements.ElementToggle;
import me.jamie.destiny.ui.click.panel.Panel;
import me.jamie.destiny.util.font.NahrFont;
import net.minecraft.client.gui.GuiScreen;

public class GuiClick extends GuiScreen
{
	private List<Panel> panels = new ArrayList<>();
	
	private NahrFont titleFont, buttonFont;
	
	public GuiClick() {
		this.titleFont = new NahrFont("Tahoma", 20);
		this.buttonFont = new NahrFont("Segoe UI", 18);
	}
	
	@Override
	public void initGui() {
		if(getPanels().isEmpty()) {
			loadPanels();
		}
		//Destiny.getInstance().getConfigManager().loadAll();
	}
	
	public void loadPanels() {
		int x = 2;
		for(Category c : Category.values()) {
			panels.add(new Panel(c.name(), x += 50, 4, false) {
				public void loadElements() {
					for(Module m : Destiny.getInstance().getModuleManager().getContents()) {
						if(m.getCategory() == null) continue;
						if(!m.getCategory().equals(c)) continue;
						getElements().add(new ElementToggle(m.getLabel(), m));
					} 
				}
			});
		}
		this.panels.forEach(panel -> panel.getElements().sort((p1, p2) -> p1.getLabel().compareTo(p2.getLabel())));
	}
	
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		super.drawScreen(mouseX, mouseY, partialTicks);
		this.drawDefaultBackground();
		this.panels.forEach(p -> p.drawScreen(mouseX, mouseY, partialTicks));
	}
	
	public void mouseClicked(int mouseX, int mouseY, int clickedButton) {
		this.panels.forEach(p -> p.mouseClicked(mouseX, mouseY, clickedButton));
	}

	public void mouseReleased(int mouseX, int mouseY, int releaseButton) {
		this.panels.forEach(p -> p.mouseReleased(mouseX, mouseY, releaseButton));
	}
	
	@Override
    public void onGuiClosed() {
		//Destiny.getInstance().getConfigManager().saveAll();
	}
	
	public List<Panel> getPanels() {
		return this.panels;
	}
	
	public NahrFont getButtonFont() {
		return buttonFont;
	}
	
	public NahrFont getTitleFont() {
		return titleFont;
	}
}
