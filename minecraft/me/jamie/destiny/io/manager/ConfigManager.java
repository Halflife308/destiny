package me.jamie.destiny.io.manager;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Set;

import org.reflections.Reflections;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.io.Config;
import me.jamie.lib.management.ListManager;

public class ConfigManager extends ListManager<Config>
{

	@Override
	public void load() {
		if(getContents() == null)
			setContents(new LinkedList<Config>());
		Destiny.getInstance().getEventManager().register(this);
		final Reflections reflect = new Reflections(Config.class);
		final Set<Class<? extends Config>> configs = reflect.getSubTypesOf(Config.class);
		for (final Class clazz : configs) {
			try {
				final Config config = (Config) clazz.newInstance();
				register(config);
			} catch (final InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		loadAll();
	}

	@Override
	public void register(Config c) {
		if(getContents().contains(c)) return;
		getContents().add(c);
	}
	
	public void loadAll() { 
		getContents().stream().forEach(c -> {
			try {
				c.load();
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}
	
	public void saveAll() {
		getContents().stream().forEach(c -> {
			try {
				c.save();
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}
}
