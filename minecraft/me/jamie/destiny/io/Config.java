package me.jamie.destiny.io;

import java.io.File;
import java.io.IOException;

import me.jamie.destiny.util.game.Wrapper;
import me.jamie.lib.interfaces.Loadable;
import me.jamie.lib.interfaces.Saveable;

public abstract class Config implements Loadable, Saveable
{
	protected String name;
	protected File dir, config;
	private boolean created;
	
	public Config(String name)
	{
		this.name = name;
		dir = new File(Wrapper.getMinecraft().mcDataDir, "Destiny");
		if(!dir.exists()) {
			dir.mkdir();
		}
		try {
			config = new File(dir, getName());
			if(!config.exists()) {
				created = config.createNewFile();
				if(created);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getName() {
		return name;
	}
}
