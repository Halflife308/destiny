package me.jamie.destiny.io.files;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.impl.property.Property;
import me.jamie.destiny.io.Config;
import me.jamie.destiny.ui.click.panel.Panel;

public class Properties extends Config
{

	public Properties() {
		super("properties.txt");
	}

	@Override
	public void load() throws IOException 
	{
		BufferedReader br = new BufferedReader(new FileReader(config));
		String s;	
	
		while((s = br.readLine()) != null)
		{
			String[] args = s.split(":");
			for(Module m : Destiny.getInstance().getModuleManager().getContents()) {
				for(Property p : m.getProperties()) {
					if(!p.getLabel().equalsIgnoreCase(args[0])) continue;
					if(p.getProperty() instanceof Boolean) {
						p.setProperty(Boolean.valueOf(args[1]));
					} else if(p.getProperty() instanceof Float) {
						p.setProperty(Float.parseFloat(args[1]));
					} else if(p.getProperty() instanceof String) {
						p.setCurrentProperty(String.valueOf(args[1]));
					}
				}
			}
		}
		br.close();
	}

	@Override
	public void save() throws IOException {
		
		PrintWriter pw = new PrintWriter(config);
		for(Module m : Destiny.getInstance().getModuleManager().getContents()) {
			for(Property p : m.getProperties()) {
				if(p.getProperty() instanceof Boolean) {
					pw.println(p.getLabel() + ":" + p.getProperty());
				} else if(p.getProperty() instanceof Float) {
					pw.println(p.getLabel() + ":" + p.getFloatProperty());
				} else if(p.getProperty() instanceof String) {
					pw.println(p.getLabel() + ":" + p.getCurrentProperty());
				} else {
					pw.println(p.getLabel() + ":" + p.getFloatProperty());
				}
			}
		}
		pw.close();
	}	
	
}
