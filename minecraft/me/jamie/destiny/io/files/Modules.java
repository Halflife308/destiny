package me.jamie.destiny.io.files;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.module.Module;
import me.jamie.destiny.io.Config;

public class Modules extends Config
{

	public Modules() {
		super("modules.txt");
	}

	@Override
	public void load() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(config));
		String s;	
	
		while((s = br.readLine()) != null)
		{
			String[] args = s.split(":");
			for(Module m : Destiny.getInstance().getModuleManager().getContents()) {
				if(!m.getLabel().equalsIgnoreCase(args[0])) continue;
				m.setBind(Keyboard.getKeyIndex(args[1].toUpperCase()));
				m.setEnabled(Boolean.valueOf(args[2]));
			}
		}
		br.close();
	}

	@Override
	public void save() throws IOException 
	{	
		PrintWriter pw = new PrintWriter(config);
		Destiny.getInstance().getModuleManager().getContents().stream().forEach(m -> pw.println(m.getLabel() + ":" + Keyboard.getKeyName(m.getBind()) + ":" + m.isEnabled()));
		pw.close();
	}
	
}
