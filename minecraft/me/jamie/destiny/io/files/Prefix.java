package me.jamie.destiny.io.files;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.io.Config;

public class Prefix extends Config
{

	public Prefix() {
		super("prefix.txt");
	}

	@Override
	public void load() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(config));
		String s;	
	
		while((s = br.readLine()) != null)
		{
			Destiny.getInstance().getCommandManager().setPrefix(s);
		}
		br.close();
	}

	@Override
	public void save() throws IOException {
		PrintWriter pw = new PrintWriter(config);
		pw.print(Destiny.getInstance().getCommandManager().getPrefix());
		pw.close();
	}
	
}
