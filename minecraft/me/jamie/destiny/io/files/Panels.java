package me.jamie.destiny.io.files;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.lwjgl.input.Keyboard;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.io.Config;
import me.jamie.destiny.ui.click.panel.Panel;

public class Panels extends Config
{

	public Panels() {
		super("panels.txt");
	}

	@Override
	public void load() throws IOException 
	{
		BufferedReader br = new BufferedReader(new FileReader(config));
		String s;	
	
		while((s = br.readLine()) != null)
		{
			String[] args = s.split(":");
			for(Panel p : Destiny.getInstance().getGuiClick().getPanels()) {
				if(!p.getLabel().equalsIgnoreCase(args[0])) continue;
				p.setX(Integer.valueOf(args[1]));
				p.setY(Integer.valueOf(args[2]));
				p.setOpen(Boolean.valueOf(args[3]));
			}
		}
		br.close();
	}

	@Override
	public void save() throws IOException {
		PrintWriter pw = new PrintWriter(config);
		Destiny.getInstance().getGuiClick().getPanels().stream().forEach(p -> pw.println(p.getLabel() + ":" + p.getX() + ":" + p.getY() + ":" + p.isOpen()));
		pw.close();
	}	
	
}
