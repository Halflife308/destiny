package me.jamie.destiny.io.files;

import java.awt.Font;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.io.Config;
import me.jamie.destiny.util.font.MinecraftFontRenderer;
import me.jamie.destiny.util.game.Wrapper;

public class Fonts extends Config
{

	public Fonts() {
		super("fonts.txt");
	}

	@Override
	public void load() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(config));
		String s;	
	
		while((s = br.readLine()) != null)
		{
			String[] args = s.split(":");
			Wrapper.getFontRenderer().setSelectedFont(new MinecraftFontRenderer(new Font(args[0], 0, Integer.valueOf(args[1])), true, false));
			Destiny.getInstance().getFontManager().setGlobalTTF(Boolean.valueOf(args[2]));
		}
		br.close();
	}

	@Override
	public void save() throws IOException 
	{	
		PrintWriter pw = new PrintWriter(config);
		pw.println(Wrapper.getFontRenderer().getSelectedFont().getFont().getName() + ":" + Wrapper.getFontRenderer().getSelectedFont().getFont().getSize() + ":" + Destiny.getInstance().getFontManager().isGlobalTTF());
		pw.close();
	}
	
}
