package me.jamie.destiny.core;

import java.awt.Font;

import org.lwjgl.opengl.Display;

import dk.n3xuz.hookregistry.Hook;
import dk.n3xuz.hookregistry.HookRegistry;
import me.jamie.destiny.impl.alt.manager.AltManager;
import me.jamie.destiny.impl.command.manager.CommandManager;
import me.jamie.destiny.impl.events.system.InitializeEvent;
import me.jamie.destiny.impl.events.system.ShutdownEvent;
import me.jamie.destiny.impl.font.manager.FontManager;
import me.jamie.destiny.impl.hooks.runtime.InitializeContext;
import me.jamie.destiny.impl.module.manager.ModuleManager;
import me.jamie.destiny.io.manager.ConfigManager;
import me.jamie.destiny.ui.click.GuiClick;
import me.jamie.destiny.util.alt.Login;
import me.jamie.destiny.util.font.MinecraftFontRenderer;
import me.jamie.destiny.util.mcleaks.SessionManager;
import me.jamie.lib.event.EventManager;
import me.jamie.lib.event.listen.Handler;

public final class Destiny 
{
	private static final Destiny INSTANCE = new Destiny();
	
	private static final double VERSION = 1.1D;
	
	private Hook<InitializeContext> initialize;
	
	private AltManager altManager;
	private CommandManager commandManager;
	private ConfigManager configManager;
	private EventManager eventManager;
	private FontManager fontManager;
	private HookRegistry hookRegistry;
	private Login login;
	private GuiClick gui;
	private ModuleManager moduleManager;
	private SessionManager sessionManager;
	
	/*
	 * TODO: 
	 * - Console
	 * - Tab GUI
	 * - Modules (AutoAccept, AutoLog)
	 * - Friends
	 * - Auto Updater
	 * - Website
	 * - Donation Option (eg. Capes, Particles, etc...)
	 * - In-game Themes
	 * - Alt Loading/Saving
	 * - MCLeaks Alt Generator (pending release)
	 * - 1.9/1.10 Versions
	 */
	private Destiny() {
		Display.setTitle("Destiny " + VERSION);
		
		eventManager = new EventManager();
		eventManager.register(this);
		
		initialize = new Hook<InitializeContext>("initialize_hook") {
			@Override
			public void call(InitializeContext context) {
				login = new Login();
				
				sessionManager = new SessionManager();
				
				altManager = new AltManager();
				altManager.load();
				
				commandManager = new CommandManager();
				commandManager.load();
				
				moduleManager = new ModuleManager();
				moduleManager.load();
				
				fontManager = new FontManager();
				if(fontManager.getTTF() == null)
					fontManager.setTTF(new MinecraftFontRenderer(new Font("Arial", 0, 20), true, false));
				fontManager.load();
				
				gui = new GuiClick();
				
				configManager = new ConfigManager();
				configManager.load();
			}
		};
	}

	@Handler
	public void onInitialize(final InitializeEvent event) {
		login = new Login();
		
		sessionManager = new SessionManager();
		
		altManager = new AltManager();
		altManager.load();
		
		commandManager = new CommandManager();
		commandManager.load();
		
		moduleManager = new ModuleManager();
		moduleManager.load();
		
		fontManager = new FontManager();
		if(fontManager.getTTF() == null)
			fontManager.setTTF(new MinecraftFontRenderer(new Font("Arial", 0, 20), true, false));
		fontManager.load();
		
		gui = new GuiClick();
		
		configManager = new ConfigManager();
		configManager.load();
	}
	
	@Handler
	protected void onShutdown(final ShutdownEvent event) {
		configManager.saveAll();
	}
	
	public static Destiny getInstance() {
		return INSTANCE;
	}
	
	public AltManager getAltManager() {
		return altManager;
	}
	
	public CommandManager getCommandManager() {
		return commandManager;
	}
	
	public ConfigManager getConfigManager() {
		return configManager;
	}
	
	public EventManager getEventManager() {
		return eventManager;
	}
	
	public FontManager getFontManager() {
		return fontManager;
	}
	
	public Login getLogin() {
		return login;
	}
	
	public GuiClick getGuiClick() {
		return gui;
	}
	
	public ModuleManager getModuleManager() {
		return moduleManager;
	}
	
	public SessionManager getSessionManager() {
		return sessionManager;
	}
	
	public double getVersion() {
		return VERSION;
	}
}
