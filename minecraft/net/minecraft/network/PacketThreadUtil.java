package net.minecraft.network;

import me.jamie.destiny.core.Destiny;
import me.jamie.destiny.impl.events.packet.PacketReceiveEvent;
import net.minecraft.util.IThreadListener;

public class PacketThreadUtil
{
    private static final String __OBFID = "CL_00002306";

    public static void func_180031_a(final Packet p_180031_0_, final INetHandler p_180031_1_, IThreadListener p_180031_2_)
    {
        if (!p_180031_2_.isCallingFromMinecraftThread())
        {
            p_180031_2_.addScheduledTask(new Runnable()
            {
                private static final String __OBFID = "CL_00002305";
                public void run()
                {
                	//TODO: Destiny
                	final PacketReceiveEvent event = new PacketReceiveEvent(p_180031_0_);
                	Destiny.getInstance().getEventManager().fire(event);
                	
                	if(!event.isCancelled())
                		event.getPacket().processPacket(p_180031_1_);
                }
            });
            throw ThreadQuickExitException.field_179886_a;
        }
    }
}
